/**
 * Created by DELL on 6/21/2017.
 */
!(function() {
    var $this = $(".load_more_news li");
    $this.on("click", function(t) {
        t.preventDefault();

        // Remove all class current
        $this.removeAttr('class');
        $('body').addClass('loading');

        var result		= '',
            getThis 	= $(this),
            getOffset 	= parseInt(getThis.attr('data-page')),
            getLimit 	= parseInt($('.pagination').attr('data-limit')),
            getFix 		= parseInt($('.pagination').attr('data-fix')),
            getType		= $('.pagination').attr('data-type'),
            getCat		= $('.pagination').attr('data-cat'),
            getSize		= $('.pagination').attr('data-size');


        if(getOffset > 0){
            getOffset 	= (getFix + ( getOffset * getLimit ) - getLimit);
        }else{
            // This condition for list all post
            getOffset 	= getFix;
            getLimit  	= 999999999999;
        }


        $.ajax({
            type: "post",
            context: this,
            dataType: "json",
            url: headJS.ajaxurl,
            data: {
                action: "load_more_news",
                offset: getOffset,
                limit: getLimit,
                type: getType,
                cat: getCat,
                fix: getFix,
                size: getSize,
            },
            beforeSend: function(t) {

                // Add current class
                getThis.addClass('current');

                // Set focus on screen
                $('html, body').animate({
                    scrollTop: ($("#jumpto").offset().top - 5)
                }, 500);

            },
            success: function(response) {

                result = $(response.html.replace(/ ( \r\n|\n|\r )/gm, ""));
                $("#resultList").html(result);

                setTimeout(function(){
                    $('body').removeAttr('class');
                }, 800);


            },
            error: function(ts) { alert(ts.responseText) }
        })
    })
})();