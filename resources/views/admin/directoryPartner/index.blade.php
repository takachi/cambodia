
@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                {{ lang($directory, 'title') }} | Partner
            </h1>
            <ol class="breadcrumb">
                <li class=""><a href="{{url('dashboard')}}"><i class="fa fa fa-th"></i>Dashboard</a></li>
                <li class="active"><i class="fa fa fa-folder"></i> Directory</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    @include('partials.flushMessage')
                    <div class="box">
                        <div class="box-header">
                            <?php $service = isset($_GET['directory']) ? $_GET['directory'] : null ?>
                            <a href="{{url('dashboard/directory-partner/create')}}{{ isset($service) ? '?directory='.$directory->id : ''   }}" class="btn btn-sm btn-primary">
                                <span class="glyphicon glyphicon-plus"></span> Add New
                            </a>
                        </div>
                        <div class="box-body">
                            <table class="default-data-table table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>@lang('label.logo')</th>
                                    <th>@lang('label.created_at')</th>
                                    <th>@lang('label.updated_at')</th>
                                    <th>@lang('label.action')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($directoryPartner as $value)
                                    <tr role="row" class="odd center_content ">
                                        <td>
                                            @include(
                                                'partials.imageModal',
                                                [
                                                    'id' => $value->id,
                                                    'title' => 'Partner logo',
                                                    'src' => $value->logo
                                                ]
                                            )
                                        </td>
                                        <td>{{ dateFormat($value->created_at) }}</td>
                                        <td>{{ dateFormat($value->updated_at) }}</td>
                                        <td>
                                            <form action="{{ action('admin\DirectoryPartnerController@destroy', $value->id) }}" method="POST">
                                                <button title="Delete" class="btn btn-xs btn-danger">
                                                    <i class="glyphicon glyphicon-trash"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop










