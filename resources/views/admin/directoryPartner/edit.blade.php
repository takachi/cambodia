
@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Directory Partner
            </h1>
            <ol class="breadcrumb">
                <li class=""><a href="{{url('dashboard')}}"><i class="fa fa fa-th"></i>Dashboard</a></li>
                <li class=""><a href="{{url('dashboard/directory-partner')}}"><i class="fa fa fa-folder"></i>Directory Partner</a></li>
                <li class="active"><i class="fa fa fa-list"></i> Edit</li>
            </ol>
        </section>

        <section class="content clearfix">
            <div class="col-xs-12">
                {!! Form::model($directoryPartner, ['method' => 'PUT', 'route' => ['dashboard.directory-partner.update', $directoryPartner ], 'files' => 'true']) !!}
                    @include('admin.advertisement._form')
                    @if(isset($directoryPartner->logo))
                        <div class="form-group">
                            <img src="{{ asset($directoryPartner->logo) }}" class="logo-preview">
                        </div>
                    @endif
                    <div class="form-group">
                        <button class="btn btn-primary">Update</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </section>
    </div>
@stop









