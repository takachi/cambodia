@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Service Lading
            </h1>
            <ol class="breadcrumb">
                <li class=""><a href="{{url('dashboard')}}"><i class="fa fa fa-th"></i>Dashboard</a></li>
                <li class=""><a href="{{url('dashboard/service')}}"><i class="fa fa fa-folder"></i>Service Lading</a></li>
                <li class="active"><i class="fa fa fa-list"></i> Edit</li>
            </ol>
        </section>
        <section class="content clearfix">
            {!! Form::model($service, ['method' => 'PUT', 'action' => ['admin\ServiceController@update', $service->id ],'files' => 'true']) !!}
                @include('admin.service._form')
                <div class="form-group">
                    <button class="btn btn-primary">Update</button>
                </div>
            {!! Form::close() !!}
        </section>
    </div>
@stop









