<a href="{{ action('admin\ServiceController@create') }}" class="btn btn-sm btn-primary margin-tb-15">
    <span class="glyphicon glyphicon-plus"></span> Add New
</a>

<table class="default-data-table table table-bordered table-striped">
    <thead>
    <tr>
        <th class="center_content">@lang('label.thumbnail')
        <th class="center_content">@lang('label.title')</th>
        <th class="center_content">@lang('label.order')</th>
        <th class="center_content">@lang('label.created_at')</th>
        <th class="center_content">@lang('label.updated_at')</th>
        <th class="center_content">@lang('label.directory')</th>
        <th class="center_content">@lang('label.carousel')</th>
        <th class="center_content">Advertisement</th>
        <th class="center_content">@lang('label.action')</th>
    </tr>
    </thead>
    <tbody>
    @foreach($services as $service)

        <tr role="row" class="odd center_content ">
            <td>
                <img class="media-object"
                     src="{{ $service->thumb  ? asset($service->thumb) : '' }}"
                     alt="User Avatar"
                     rel="popover"
                     style="height: 40px;"
                     data-img="{{ $service->thumb  ? asset($service->thumb) : '' }}"
                     title="{{ $service->title_en }}">
            </td>
            <td>{{ lang($service, 'title') }}</td>

            <td>{{ $service->ordering }}</td>
            <td>{{ dateFormat($service->created_at) }}</td>
            <td>{{ dateFormat($service->updated_at) }}</td>
            <td>
                <a title="Manage Carousel" href="{{ action('admin\ServiceController@directories', $service->id) }}" class="btn btn-xs btn-success">
                    {{ count($service->directories) }}
                    | <i class="glyphicon glyphicon-plus"></i>
                </a>
            </td>
            <td>
                <a title="Manage Carousel" href="{{ action('admin\ServiceController@carousel', $service->id) }}" class="btn btn-xs btn-success">
                    {{ count($service->carousels) }}
                    | <i class="glyphicon glyphicon-plus"></i>
                </a>
            </td>
            <td>
                <a title="Manage Advertisement" href="{{ action('admin\AdvertisementController@index', ['service' => $service->name]) }}" class="btn btn-xs btn-success">
                    {{ count($service->advertisements()) }}
                    | <i class="glyphicon glyphicon-plus"></i>
                </a>
            </td>
            <td>
                @include('partials._action', ['entityName' => 'service', 'entity' => $service])
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
