@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Service Ladding
            </h1>
            <ol class="breadcrumb">
                <li class=""><a href="{{url('dashboard')}}"><i class="fa fa fa-th"></i>Dashboard</a></li>
                <li class="active"><i class="fa fa fa-folder"></i> Service Ladding</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#service_item" data-toggle="tab">Service Items</a></li>
                        <li><a href="#slide_show" data-toggle="tab">SlideShow</a></li>
                        <li><a href="#search_box" data-toggle="tab">Search Box</a></li>
                        <li><a href="#page_title" data-toggle="tab">Page Title</a></li>
                    </ul>
                    <div class="tab-content">
                        @include('partials.flushMessage')
                        <div class="active tab-pane clearfix" id="service_item">
                            @include('admin.service._serviceItem')
                        </div>
                        <div class="tab-pane clearfix" id="slide_show">
                            @include(
                                'admin.carousel._form',
                                [
                                    'carousels' => isset($carousels) ? $carousels : null,
                                    'carouselName' => \App\Models\Carousel::PAGES['service']
                                ]
                            )
                            @include('admin.carousel._table')
                        </div>
                        <div class="tab-pane clearfix" id="search_box">
                            @include(
                                'admin.contentElement._searchBox',
                                [
                                    'contentElementId'=> isset($ceSearchBox) ? $ceSearchBox->id : null,
                                    'contentElementName'=> \App\Models\ContentElement::SERVICE
                                ]
                            )
                        </div>
                        <div class="tab-pane clearfix" id="page_title">
                            @include(
                                'admin.contentElement._pageTitle',
                                [
                                    'contentElementId' =>  isset($cePageTitle) ? $cePageTitle->id : null,
                                    'contentElementName' => \App\Models\ContentElement::SERVICE
                                ]
                            )
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop




