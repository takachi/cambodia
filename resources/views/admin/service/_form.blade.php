<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#service_en" data-toggle="tab">EN</a></li>
        <li class=""><a href="#service_kh" data-toggle="tab">KH</a></li>
    </ul>
    <div class="tab-content">
        <div class="active tab-pane clearfix" id="service_en">
            <label for="title_en">Title</label><i class="text-red">*</i>
            <input name="title_en" id="title_en" class="form-control" value="{{ isset($service) ? $service->title_en : '' }}" placeholder="Title" required>
        </div>
        <div class="tab-pane clearfix" id="service_kh">
            <label for="title_kh">Title</label>
            <input name="title_kh" id="title_kh" class="form-control" value="{{ isset($service) ? $service->title_kh : '' }}" placeholder="Title">
        </div>
    </div>
</div>
<label for="ordering">Ordering</label>
<input type="number" name="ordering" id="ordering" class="form-control" value="{{ isset($service) ? $service->ordering : '' }}" placeholder="Number">

<label for="thumb">Thumbnail (275x X 295px)</label>
@include(
    'partials.fileUpload',
    [
        'id' => 'thumbnail',
        'name' => 'thumb',
        'url' => isset($service->thumb) ? $service->thumb : ''
    ]
)
