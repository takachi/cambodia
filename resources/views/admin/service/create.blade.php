
@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Service Ladding</h1>
            <ol class="breadcrumb">
                <li class=""><a href="{{url('dashboard')}}"><i class="fa fa fa-th"></i>Dashboard</a></li>
                <li class=""><a href="{{url('dashboard/service')}}"><i class="fa fa fa-folder"></i>Service Ladding</a></li>
                <li class="active"><i class="fa fa fa-list"></i> Add New</li>
            </ol>
        </section>

        <section class="content clearfix">
            <form class="form-horizontal"
                  action="{{ action('admin\ServiceController@store') }} "
                  method="POST"
                  enctype="multipart/form-data">

                @include('admin.service._form')
                <button class="btn btn-primary margin-tb-15">Submit</button>
            </form>
        </section>
    </div>
@stop

