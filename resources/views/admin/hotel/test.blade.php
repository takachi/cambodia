@extends('layouts.admin')
@section('content')
    <div class="panel panel-info">
        <h3 class="panel-title">Create Article</h3>
    </div>
    <div class="panel-body">
        {!! Form::open(['url' => 'test']) !!}

            <div class="form-group">
                {{ Form::label('title', 'Title') }}
                {{ Form::text('title', null, ['class'=>'form-control']) }}
            </div>

            <div class="form-group">
                {{ Form::label('body', 'Body') }}
                {{ Form::textarea('body', null, ['class'=>'form-control']) }}
            </div>

            <div class="form-group">
                {{ Form::label('active', 'Active') }}
                {{ Form::checkbox('active', 1) }}
            </div>

            <div class="form-group">
                {{ Form::submit('save', ['class'=>'btn btn-primary form-control']) }}
                {{Form::select('size', ['L' => 'Large', 'S' => 'Small'])}}
            </div>

        {!! Form::close() !!}
    </div>

@stop
