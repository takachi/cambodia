<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th class="center_content">@lang('label.image')</th>
        <th class="center_content">@lang('label.created')</th>
        <th class="center_content">@lang('label.updated')</th>
        <th class="center_content">@lang('label.action')</th>
    </tr>
    </thead>
    <tbody>
    @if(isset($carousels))
        @foreach($carousels as $carousel)
            <tr>
                <td>
                    @include(
                        'partials.imageModal',
                        [
                            'id' => $carousel->id,
                            'title' =>  lang($carousel, 'title'),
                            'src' => $carousel->image
                        ]
                    )
                </td>
                <td> {{ dateFormat($carousel->created_at) }}</td>
                <td> {{ dateFormat($carousel->updated_at) }}</td>
                <td>
                    @include('admin.carousel._actionEditModal')
                    <form action="{{ action('admin\CarouselController@destroy', ['id' => $carousel->id]) }}"
                          method="POST" enctype="multipart/form-data"
                          style="display: inline-block"
                          onsubmit="return confirm('Are you sure you want to delete?')">

                        <button title="Delete" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i></button>
                    </form>
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
