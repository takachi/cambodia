<a title="Edit" href="#slide_show" class="btn btn-xs btn-success" data-toggle="modal" data-target="#edit-{{$carousel->id}}">
    <i class="glyphicon glyphicon-pencil"></i>
</a>
<div class="modal fade" id="edit-{{ $carousel->id }}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>
                <h4 class="modal-title">{{ lang($carousel, 'title') }}</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal"
                      action="{{ action('admin\CarouselController@update', ['id' => $carousel->id]) }} "
                      method="POST"
                      enctype="multipart/form-data">

                    <label for="image">Image (1920px X 450px )</label> <i class="text-red">*</i>
                    <input type="file" name="image" id="image" class="form-control" required>

                    <input type="hidden" name="name" value="{{ isset($carouselName) ? $carouselName : null }}">
                    <input type="hidden" name="service_id" value="{{ isset($service) ? $service->id : null }}">
                    <button class="btn btn-primary margin-tb-15">Update</button>
                </form>
            </div>
        </div>
    </div>
</div>





