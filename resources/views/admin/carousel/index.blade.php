@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                {{ isset($service) ? lang($service, 'title').' | ' : '' }} Carousel
            </h1>
            <ol class="breadcrumb">
                <li class=""><a href="{{url('dashboard')}}"><i class="fa fa fa-th"></i>Dashboard</a></li>
                <li class="active"><i class="fa fa fa-folder"></i> Service</li>
            </ol>
        </section>
        <section class="content">
            <div class="row nav-tabs-custom">
                <div class="tab-content">
                    @include(
                       'admin.carousel._form',
                       [
                           'carousels' => isset($carousels) ? $carousels : null,
                           'carouselName' => $service->name,
                           'serviceId' => $service->id
                       ]
                    )

                    @include('admin.carousel._table')
                </div>
            </div>
        </section>
    </div>
@stop




