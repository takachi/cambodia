
<form class="form-horizontal"
      action="{{ action('admin\CarouselController@store') }}"
      method="POST"
      enctype="multipart/form-data">

    <label for="image">Image (1920px X 450px )</label> <i class="text-red">*</i>
    <input type="file" name="image" id="image" class="form-control" required>

    <input type="hidden" name="name" value="{{ isset($carouselName) ? $carouselName : null }}">
    <input type="hidden" name="service_id" value="{{ isset($serviceId) ? $serviceId : null }}">
    <button class="btn btn-primary margin-tb-15">{{ isset($carousel) ? 'Update' : 'Create' }}</button>
</form>

<!-- Sample
{{--@include(--}}
   {{--'admin.carousel._form',--}}
   {{--[--}}
       {{--'carousels' => isset($carousels) ? $carousels : null,--}}
       {{--'carouselName' => \App\Models\Carousel::PAGES['service']--}}
   {{--]--}}
{{--)--}}
-->



