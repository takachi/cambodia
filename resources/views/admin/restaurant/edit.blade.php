
@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Restaurant
            </h1>
            <ol class="breadcrumb">
                <li class=""><a href="{{url('dashboard')}}"><i class="fa fa fa-th"></i>Dashboard</a></li>
                <li class=""><a href="{{url('dashboard/hotel')}}"><i class="fa fa fa-folder"></i>Restaurant</a></li>
                <li class="active"><i class="fa fa fa-list"></i> Edit</li>
            </ol>
        </section>

        <section class="content clearfix">
            <div class="col-xs-12">
                 {!! Form::model($items, ['method' => 'PUT', 'action' => ['admin\RestaurantController@update', $items->id ],'files' => 'true']) !!}
                    @include('admin.restaurant._form')
                    <div class="form-group">
                        <button class="btn btn-primary">Update</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </section>
    </div>
@stop










