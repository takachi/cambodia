
@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Restaurant
            </h1>
            <ol class="breadcrumb">
                <li class=""><a href="{{url('dashboard')}}"><i class="fa fa fa-th"></i>Dashboard</a></li>
                <li class=""><a href="{{url('dashboard/restaurant')}}"><i class="fa fa fa-folder"></i>Restaurant</a></li>
                <li class="active"><i class="fa fa fa-list"></i> Add new</li>
            </ol>
        </section>

        <section class="content clearfix">
            <div class="col-xs-12">
                <form class="form-horizontal" action="{{ action('admin\RestaurantController@store') }} " method="POST" enctype="multipart/form-data">
                    @include('admin.restaurant._form')
                    <div class="form-group">
                        <button class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </section>
    </div>
@stop












