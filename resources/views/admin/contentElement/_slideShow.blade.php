
@if(isset($ceSlideShow))
    <form class="form-horizontal"
          action="{{ action('admin\ContentElement\SlideShowController@update', ['id' => $contentElementId]) }} "
          method="POST"
          enctype="multipart/form-data">
@else
    <form class="form-horizontal"
          action="{{ action('admin\ContentElement\SlideShowController@store') }}"
          method="POST"
          enctype="multipart/form-data">
@endif

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#service_en" data-toggle="tab">EN</a></li>
                <li class=""><a href="#service_kh" data-toggle="tab">KH</a></li>
            </ul>
            <div class="tab-content">
                <div class="active tab-pane clearfix" id="service_en">
                    <label for="title">Title</label>
                    <input name="content[title_en]" id="title" value="{{ isset($slideShow) ? $slideShow->title_en : '' }}" class="form-control" placeholder="Title">

                    <label for="description_en">Description</label>
                    <input name="content[description_en]" id="description_en" value="{{ isset($slideShow) ? $slideShow->description_en : '' }}" class="form-control" placeholder="Description">

                </div>
                <div class="tab-pane clearfix" id="service_kh">
                    <label for="title_kh">Title</label>
                    <input name="content[title_kh]" id="title_kh" value="{{ isset($slideShow) ? $slideShow->title_en : '' }}" class="form-control" placeholder="Title">

                    <label for="description_kh">Description</label>
                    <input name="content[description_kh]" id="description_kh" value="{{ isset($slideShow) ? $slideShow->description_en : '' }}" class="form-control" placeholder="Description">

                </div>
            </div>
        </div>

        <label for="slide_show">Image (1920px X 450px )</label> <i class="text-red">*</i>
        <input type="file" name="content[slide_show]" id="banner" class="form-control" required>

        <input type="hidden" name="name" value="{{ $contentElementName }}">
        <button class="btn btn-primary margin-tb-15">{{ isset($slideShow) ? 'Update' : 'Create' }}</button>
    </form>

<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th class="center_content">@lang('label.image')</th>
            <th class="center_content">@lang('label.title')</th>
            <th class="center_content">@lang('label.description')</th>
            <th class="center_content">@lang('label.created')</th>
            <th class="center_content">@lang('label.updated')</th>
            <th class="center_content">@lang('label.action')</th>
        </tr>
    </thead>
    <tbody>
        @if(isset($ceSlideShows))
            @foreach($ceSlideShows as $ceSlideShow)
                <?php $slideShow =  json_decode($ceSlideShow->content)?>
                <tr>
                    <td>
                        @include(
                            'partials.imageModal',
                            [
                                'id' => $ceSlideShow->id,
                                'title' =>  $slideShow->title_en,
                                'src' => $slideShow->slide_show
                            ]
                        )
                    </td>
                    <td> {{ $slideShow->title_en }}</td>
                    <td> {{ $slideShow->description_en }}</td>
                    <td> {{ dateFormat($ceSlideShow->created_at) }}</td>
                    <td> {{ dateFormat($ceSlideShow->updated_at) }}</td>
                    <td>
                        <a title="Edit" href="#slide_show" class="btn btn-xs btn-success" data-toggle="modal" data-target="#edit-{{$ceSlideShow->id}}">
                            <i class="glyphicon glyphicon-pencil"></i>
                        </a>
                        @include('admin.contentElement._actionEditModal', ['item' => $ceSlideShow, 'id' => $ceSlideShow->id])
                        <form action="{{ action('admin\ContentElement\SlideShowController@destroy', ['id' => $ceSlideShow->id]) }}"
                              method="POST" enctype="multipart/form-data"
                              style="display: inline-block"
                              onsubmit="return confirm('Are you sure you want to delete?')">
                            <button title="Delete" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i></button>
                        </form>
                    </td>
                </tr>
            @endforeach
        @endif
    </tbody>
</table>






