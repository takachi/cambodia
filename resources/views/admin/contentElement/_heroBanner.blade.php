<?php
if (isset($ceHeroBanner)) {
    $heroBanner = json_decode($ceHeroBanner->content)->hero_banner;
}
?>
@if(isset($heroBanner))

    <form class="form-horizontal" action="{{ action('admin\ContentElementController@heroBannerUpdate', ['id' => $contentElementId]) }} " method="POST" enctype="multipart/form-data">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Hero Banner</h3>
            </div>
            <label for="title">Title</label>
            <input name="content[hero_banner][title_en]" id="title" class="form-control" placeholder="Title">

            <label for="banner">Image (1920px X 450px )</label> <i class="text-red">*</i>
            {{--<input type="file" name="content[hero_banner][banner]" id="banner" class="form-control" required>--}}
            @include(
                'partials.fileUpload',
                [
                    'id' => 'banner',
                    'name' => 'content[hero_banner][banner]',
                    'url' => isset($heroBanner->banner) ? $heroBanner->banner : ''
                ]
            )

            <input type="hidden" name="name" value="{{ $contentElementName }}">
            <button class="btn btn-primary margin-tb-15">Update</button>
        </div>
    </form>
@else
    <form class="form-horizontal" action="{{ action('admin\ContentElementController@heroBannerStore') }} " method="POST" enctype="multipart/form-data">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"></h3>
            </div>
            <label for="title">Title</label>
            <input name="content[hero_banner][title_en]" id="title" class="form-control" placeholder="Title">

            <label for="banner">Image (1920px x 250px )</label> <i class="text-red">*</i>
            {{--<input type="file" name="content[hero_banner][banner]" id="banner" class="form-control" required>--}}
            @include(
                'partials.fileUpload',
                [
                    'id' => 'banner',
                    'name' => 'content[hero_banner][banner]',
                ]
            )
            <input type="hidden" name="name" value="{{ $contentElementName }}">
            <button class="btn btn-primary margin-tb-15">Create</button>
        </div>
    </form>
    @endif

    @if(isset($heroBanner))
    </br></br>
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Display Hero Banner</h3>
        </div>
        <label for="title">Title:</label> ​{{ $heroBanner->title_en }}

        <img
                src="{{ $heroBanner->banner  ? asset($heroBanner->banner) : asset('assets/images/sample_logo.png') }}"
                class="img-responsive"
                title="{{ $heroBanner->title_en }}">
    </div>
@endif





