<div class="modal fade" id="edit-{{ $item->id }}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>
                <h4 class="modal-title">Service</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal"
                      action="{{ action('admin\ContentElement\SlideShowController@update', ['id' => $id]) }} "
                      method="POST"
                      enctype="multipart/form-data">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#slide_en" data-toggle="tab">EN</a></li>
                            <li class=""><a href="#slide_kh" data-toggle="tab">KH</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="active tab-pane clearfix" id="slide_en">
                                <label for="title">Title</label>
                                <input name="content[title_en]" id="title" value="{{ isset($slideShow) ? $slideShow->title_en : '' }}" class="form-control" placeholder="Title">

                                <label for="description_en">Description</label>
                                <input name="content[description_en]" id="description_en" value="{{ isset($slideShow) ? $slideShow->description_en : '' }}" class="form-control" placeholder="Description">

                            </div>
                            <div class="tab-pane clearfix" id="slide_kh">
                                <label for="title_kh">Title</label>
                                <input name="content[title_kh]" id="title_kh" value="{{ isset($slideShow) ? $slideShow->title_en : '' }}" class="form-control" placeholder="Title">

                                <label for="description_kh">Description</label>
                                <input name="content[description_kh]" id="description_kh" value="{{ isset($slideShow) ? $slideShow->description_en : '' }}" class="form-control" placeholder="Description">

                            </div>
                        </div>
                    </div>

                    <label for="slide_show">Image (1920px X 450px )</label>
                    <input type="file" name="content[slide_show]" id="banner" class="form-control">

                    <input type="hidden" name="name" value="{{ $contentElementName }}">
                    <button class="btn btn-primary margin-tb-15">{{ isset($slideShow) ? 'Update' : 'Create' }}</button>
                </form>
            </div>
        </div>
    </div>
</div>





