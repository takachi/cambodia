<?php
if (isset($cePageTitle)) {
    $pageTitle = json_decode($cePageTitle->content)->page_title;
}
?>
@if(isset($pageTitle))
    <!-- todo: should use only one form with create and update -->
    <form class="" action="{{ action('admin\ContentElementController@pageTitleUpdate', ['id' => $contentElementId]) }} " method="POST" enctype="multipart/form-data">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#page_title_en" data-toggle="tab" aria-expanded="true">EN</a></li>
                <li class=""><a href="#page_title_kh" data-toggle="tab" aria-expanded="false">KH</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="page_title_en">
                    <input type="hidden" name="name" value="{{ $contentElementName }}">
                    <div class="input-group">
                        <div class="input-group-btn">
                            <button class="btn btn-primary">Update</button>
                        </div>
                        <input name="content[page_title][title_en]" value="{{ isset($pageTitle->title_en) ? $pageTitle->title_en : '' }}" id="title" class="form-control" placeholder="Page title">
                    </div>
                </div>
                <div class="tab-pane" id="page_title_kh">
                    <input type="hidden" name="name" value="{{ $contentElementName }}">
                    <div class="input-group">
                        <div class="input-group-btn">
                            <button class="btn btn-primary">Update</button>
                        </div>
                        <input name="content[page_title][title_kh]" value="{{ isset($pageTitle->title_kh) ? $pageTitle->title_kh : '' }}" id="title" class="form-control" placeholder="Page title">
                    </div>
                </div>
            </div>
        </div>
    </form>
@else
    <form class="" action="{{ action('admin\ContentElementController@pageTitleStore') }} " method="POST" enctype="multipart/form-data">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#page_title_en" data-toggle="tab" aria-expanded="true">EN</a></li>
                <li class=""><a href="#page_title_kh" data-toggle="tab" aria-expanded="false">KH</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="page_title_en">
                    <input type="hidden" name="name" value="{{ $contentElementName }}">
                    <div class="input-group">
                        <div class="input-group-btn">
                            <button class="btn btn-primary">Create</button>
                        </div>
                        <input name="content[page_title][title_en]" id="title" class="form-control" placeholder="Page title">
                    </div>
                </div>
                <div class="tab-pane" id="page_title_kh">
                    <input type="hidden" name="name" value="{{ $contentElementName }}">
                    <div class="input-group">
                        <div class="input-group-btn">
                            <button class="btn btn-primary">Create</button>
                        </div>
                        <input name="content[page_title][title_kh]" id="title" class="form-control" placeholder="Page title">
                    </div>
                </div>
            </div>
        </div>
    </form>
@endif














