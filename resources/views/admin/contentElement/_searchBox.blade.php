<?php
if (isset($ceSearchBox)) {
    $searchBox = json_decode($ceSearchBox->content)->search_box;
}
?>
@if(isset($searchBox))
    <!-- todo: should use only one form with create and update -->
    <form class="form-horizontal" action="{{ action('admin\ContentElementController@searchBoxUpdate', ['id' => $contentElementId]) }} " method="POST" enctype="multipart/form-data">
        <input type="hidden" name="name" value="{{ $contentElementName }}">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#en" data-toggle="tab" aria-expanded="true">EN</a></li>
                <li class=""><a href="#kh" data-toggle="tab" aria-expanded="false">KH</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="en">
                    <label for="title">Title</label>
                    <input name="content[search_box][title_en]" value="{{ isset($searchBox->title_en) ?  $searchBox->title_en : '' }}" id="title" class="form-control" placeholder="Title">

                    <label for="placeholder_en">Placeholder</label>
                    <input name="content[search_box][placeholder_en]" value="{{ isset($searchBox->placeholder_en) ? $searchBox->placeholder_en : '' }}"  id="placeholder_en" class="form-control" placeholder="Placeholder">

                    <label for="description_en">Description</label>
                    <input name="content[search_box][description_en]" value="{{ isset($searchBox->description_en) ? $searchBox->description_en : '' }}" id="description_en" class="form-control" placeholder="Description">

                    <button class="btn btn-primary margin-tb-15">Update</button>
                </div>
                <div class="tab-pane" id="kh">
                    <label for="title">Title</label>
                    <input name="content[search_box][title_kh]" value="{{ isset($searchBox->title_kh) ?  $searchBox->title_kh : '' }}" id="title" class="form-control" placeholder="Title">

                    <label for="placeholder_en">Placeholder</label>
                    <input name="content[search_box][placeholder_kh]" value="{{ isset($searchBox->placeholder_kh) ? $searchBox->placeholder_kh : '' }}" id="placeholder_en" class="form-control" placeholder="Placeholder">

                    <label for="description_en">Description</label>
                    <input name="content[search_box][description_kh]" value="{{ isset($searchBox->description_kh) ? $searchBox->description_kh : '' }}" id="description_en" class="form-control" placeholder="Description">

                    <button class="btn btn-primary margin-tb-15">Update</button>
                </div>
            </div>
        </div>
    </form>
@else
    <form class="form-horizontal" action="{{ action('admin\ContentElementController@searchBoxStore') }} " method="POST" enctype="multipart/form-data">
        <input type="hidden" name="name" value="{{ $contentElementName }}">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#en" data-toggle="tab" aria-expanded="true">EN</a></li>
                <li class=""><a href="#kh" data-toggle="tab" aria-expanded="false">KH</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="en">
                    <label for="title">Title</label>
                    <input name="content[search_box][title_en]" id="title" class="form-control" placeholder="Title">

                    <label for="placeholder_en">Placeholder</label>
                    <input name="content[search_box][placeholder_en]" id="placeholder_en" class="form-control" placeholder="Placeholder">

                    <label for="description_en">Description</label>
                    <input name="content[search_box][description_en]" id="description_en" class="form-control" placeholder="Description">

                    <button class="btn btn-primary margin-tb-15">Create</button>
                </div>
                <div class="tab-pane" id="kh">
                    <label for="title">Title</label>
                    <input name="content[search_box][title_kh]" id="title" class="form-control" placeholder="Title">

                    <label for="placeholder_en">Placeholder</label>
                    <input name="content[search_box][placeholder_kh]" id="placeholder_en" class="form-control" placeholder="Placeholder">

                    <label for="description_en">Description</label>
                    <input name="content[search_box][description_kh]" id="description_en" class="form-control" placeholder="Description">

                    <button class="btn btn-primary margin-tb-15">Create</button>
                </div>
            </div>
        </div>
    </form>
@endif
