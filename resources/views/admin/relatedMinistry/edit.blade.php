
@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Related Ministry
            </h1>
            <ol class="breadcrumb">
                <li class=""><a href="{{url('dashboard')}}"><i class="fa fa fa-th"></i>Dashboard</a></li>
                <li class=""><a href="{{url('dashboard/relatedministry')}}"><i class="fa fa fa-folder"></i>Related Ministries</a></li>
                <li class="active"><i class="fa fa fa-list"></i> Edit</li>
            </ol>
        </section>

        <section class="content clearfix">
            <div class="col-xs-12">
                {!! Form::model($relatedMinistry, ['method' => 'PUT', 'route' => ['dashboard.relatedministry.update', $relatedMinistry ],'files' => 'true']) !!}
                    @include('admin.relatedMinistry._form')
                    <div class="form-group">
                        <button class="btn btn-primary">Update</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </section>
    </div>
@stop










