<div class="form-group">
    <label for="title">Title</label><i class="text-red">*</i>
    <input name="title_en" id="title" class="form-control" value="{{ isset($relatedMinistry) ? $relatedMinistry->title_en : '' }}" placeholder="Title" required>
</div>
<div class="form-group">
    <label for="website">URL</label>
    <input type="url" name="website" id="website" class="form-control" value="{{ isset($relatedMinistry) ? $relatedMinistry->website : '' }}" placeholder="URL" required>
</div>
<div class="form-group">
    <label for="logo">Logo</label>
    <input type="file" id="logo" class="form-control" name="logo">
</div>
