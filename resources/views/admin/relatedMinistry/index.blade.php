
@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Related Ministry
            </h1>
            <ol class="breadcrumb">
                <li class=""><a href="{{url('dashboard')}}"><i class="fa fa fa-th"></i>Dashboard</a></li>
                <li class="active"><i class="fa fa fa-folder"></i> Related Ministries</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#related_ministry_item" data-toggle="tab">Related Ministry Items</a></li>
                        <li><a href="#hero_banner" data-toggle="tab">Hero Banner</a></li>
                        <li><a href="#page_title" data-toggle="tab">Page Title</a></li>
                    </ul>
                    <div class="tab-content">
                        @include('partials.flushMessage')
                        <div class="active tab-pane clearfix" id="related_ministry_item">
                            <div class="box">
                                <div class="box-header">
                                    <a href="{{url('dashboard/relatedministry/create')}}" class="btn btn-sm btn-primary">
                                        <span class="glyphicon glyphicon-plus"></span> Add New
                                    </a>
                                </div>
                                <div class="box box-primary">
                                    <div class="box-body">
                                        <table class="default-data-table table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th class="center_content">@lang('label.logo')</th>
                                                <th class="center_content">@lang('label.title')</th>
                                                <th class="center_content">@lang('label.url')</th>
                                                <th class="center_content">@lang('label.date')</th>
                                                <th class="center_content">@lang('label.modifyDate')</th>
                                                <th class="center_content">@lang('label.action')</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($relatedMinistries as $relatedMinistry)

                                                <tr role="row" class="odd center_content ">
                                                    <td>
                                                        <img class="media-object"
                                                             src="{{ $relatedMinistry->logo  ? asset($relatedMinistry->logo) : asset('assets/images/sample_logo.png') }}"
                                                             alt="User Avatar"
                                                             rel="popover"
                                                             style="height: 25px;"
                                                             data-img="{{ $relatedMinistry->logo  ? asset($relatedMinistry->logo) : asset('assets/images/sample_logo.png') }}"
                                                             title="{{ $relatedMinistry->title }}">
                                                    </td>
                                                    <td>{{ $relatedMinistry->title_en }}</td>
                                                    <td>{{ $relatedMinistry->url}}</td>
                                                    <td>{{ dateFormat($relatedMinistry->createe_at) }}</td>
                                                    <td>{{ dateFormat($relatedMinistry->updated_at) }}</td>
                                                    <td>
                                                        @include('partials._action', ['entityName' => 'relatedministry', 'entity' => $relatedMinistry])
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane clearfix" id="hero_banner">
                            @include('admin.contentElement._heroBanner', [
                            'contentElementId' =>  isset($ceHeroBanner) ? $ceHeroBanner->id : null,
                            'contentElementName' => \App\Models\ContentElement::RELATED_MINISTRY
                            ])
                        </div>
                        <div class="tab-pane clearfix" id="page_title">
                            @include('admin.contentElement._pageTitle', [
                            'contentElementId' =>  isset($cePageTitle) ? $cePageTitle->id : null,
                            'contentElementName' => \App\Models\ContentElement::RELATED_MINISTRY
                            ])
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop








