
@extends('layouts.admin')
@section('content')
    <style>
        .center_content{
            text-align: center;
        }
        .box .padding-bottom {
            padding-bottom: 10px;
        }
    </style>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Show Information
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('dashboard')}}"><i class="fa fa fa-th"></i>Dashboard</a></li>
                <li><a href="{{url('dashboard/user')}}"><i class="fa fa fa-th"></i>Administrator</a></li>
                <li class="active">User Information</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <table>
                            {{--<tr >--}}
                                {{--<td class="padding-bottom" ><label>FIRST NAME : </label> {{ $user->first_name }} </td>--}}
                            {{--</tr>--}}
                            {{--<tr >--}}
                                {{--<td class="padding-bottom" ><label>LAST NAME : </label> {{ $user->last_name }} </td>--}}
                            {{--</tr>--}}

                            <tr >
                                <td class="padding-bottom" ><label>USER NAME : </label> {{ $user->name }} </td>
                            </tr>
                            <tr>
                                <td class="padding-bottom" ><label>COMPANY NAME : </label> {{ $user->company_name }}</td>
                            </tr>
                            <tr>
                                <td class="padding-bottom" ><label>ADDRESS : </label> {{ $user->address }}</td>
                            </tr>
                            <tr>
                                <td class="padding-bottom" ><label>EMAIL : </label> {{ $user->email }}</td>
                            </tr>
                            <tr>
                                <td class="padding-bottom" ><label>PHONE NUMBER : </label> {{ $user->phone_number }}</td>
                            </tr>
                            <tr>
                                <td class="padding-bottom" ><label>ROLE : </label> {{ $user->role }}</td>
                            </tr>
                            <tr>
                                <td class="padding-bottom" ><label>CREATE DATE : </label> {{ $user->date_add_data }}</td>
                            </tr>
                            {{--<tr>--}}
                                {{--<td class="padding-bottom" ><label>UPDATE DATE : </label>{{ $user->updated_at }}</td>--}}
                            {{--</tr>--}}
                        </table>
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@stop














