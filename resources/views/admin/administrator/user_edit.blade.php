@extends('layouts.admin')
@section('content')
    <div class="content-wrapper" style="min-height: 1143px;">
        <section class="content-header">
            <h1>
                Edit
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('dashboard')}}"><i class="fa fa fa-th"></i>Dashboard</a></li>
                <li><a href="{{url('dashboard/user')}}"><i class="fa fa fa-th"></i>Administrator</a></li>
                <li class="active">Edit User</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">

                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        {{--<li class="active"><a href="#activity" data-toggle="tab">Activity</a></li>--}}
                        <li class="active"><a href="#english" data-toggle="tab">User</a></li>
                        {{--<li class=""><a href="#khmer" data-toggle="tab">ខ្មែរ</a></li>--}}
                    </ul>
                    <div class="tab-content">

                        <!-- /.tab-pane -->
                        <div class="active tab-pane" id="english">
                            <form class="form-horizontal" method="POST" action="{{ route('dashboard.user-update',$user->id) }}" enctype="multipart/form-data">
                             {{ csrf_field() }}
                                @include('admin.administrator.user_fields')

                                {{--status--}}
                                <!--<div class="form-group">
                                    <label for="regulation_status_en" class="col-sm-3 control-label">Status</label>
                                    <div class="col-sm-3">
                                        <select id="regulation_status_en" name="regulation_status_en"  class="form-control">
                                            <option value="1">Enable</option>
                                            <option value="0">disable</option>
                                        </select>
                                    </div>
                                </div> -->
                                {{--uplord logo--}}
                                {{--<div class="form-group">--}}
                                {{--<label for="regulation_Logo_en" class="col-sm-3 control-label">Upload Logo</label>--}}
                                {{--<input id="regulation_Logo_en" name="regulation_Logo_en" type="file" class="col-sm-3" required>--}}
                                {{--<p class="help-block col-sm-10 " >Example abc.JPG / PNG</p>--}}
                                {{--</div>--}}
                                {{--url--}}
                                {{--<div class="form-group">--}}
                                {{--<label for="inputUrlEn" class="col-sm-3 control-label">URL</label>--}}
                                {{--<div class="col-sm-10">--}}
                                {{--<input class="form-control" id="inputUrlEn" placeholder="url..." type="url" required>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--submit--}}
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-10">
                                        <button type="submit" name="regulation_submit_en" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <!-- /.tab-pane -->
                    {{--<div class="tab-pane" id="khmer">--}}
                    {{--<form class="form-horizontal">--}}
                    {{--<div class="form-group">--}}
                    {{--<label for="inputTitleKh" class="col-sm-3 control-label">ចំណងជើង</label>--}}

                    {{--<div class="col-sm-10">--}}
                    {{--<input class="form-control" id="inputTitleKh" placeholder="ចំណងជើង..." type="text">--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--uplord logo--}}
                    {{--<div class="form-group">--}}
                    {{--<label for="inputLogoKh" class="col-sm-3 control-label">អាប់ឡូតរូប</label>--}}
                    {{--<input id="inputLogokh" type="file" class="col-sm-10">--}}
                    {{--<p class="help-block col-sm-10 " >ឧទាហរណ៍ abc.JPG / PNG</p>--}}
                    {{--</div>--}}
                    {{--url--}}
                    {{--<div class="form-group">--}}
                    {{--<label for="inputUrlKh" class="col-sm-3 control-label">អាសយដ្ឋាននៃគេហទំព័រ</label>--}}
                    {{--<div class="col-sm-10">--}}
                    {{--<input class="form-control" id="inputUrlKh" placeholder="អាសយដ្ឋាន..." type="url">--}}
                    {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="form-group">--}}
                    {{--<div class="col-sm-offset-2 col-sm-10">--}}
                    {{--<button type="submit" class="btn btn-danger">បញ្ជូន</button>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</form>--}}
                    {{--</div>--}}
                    <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- /.nav-tabs-custom -->

            </div>
        </section>
    </div>

@stop










