@extends('layouts.admin')
@section('content')
    <div class="content-wrapper" style="min-height: 1143px;">
        <section class="content-header">
            <h1>
                Add New
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('dashboard')}}"><i class="fa fa fa-th"></i>Dashboard</a></li>
                <li><a href="{{url('dashboard/user')}}"><i class="fa fa fa-th"></i>Administrator</a></li>
                <li class="active">Add User</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">

                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        {{--<li class="active"><a href="#activity" data-toggle="tab">Activity</a></li>--}}
                        <li class="active"><a href="#english" data-toggle="tab">User</a></li>
                        {{--<li class=""><a href="#khmer" data-toggle="tab">ខ្មែរ</a></li>--}}
                    </ul>
                    <div class="tab-content">

                        <!-- /.tab-pane -->
                        <div class="active tab-pane" id="english">
                            <form class="form-horizontal" action="{{ route('dashboard.user-create') }} " method="POST" enctype="multipart/form-data">
                            {{--<form class="form-horizontal"  method="POST" enctype="multipart/form-data">--}}
                               @include('admin.administrator.user_fields')

                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-10">
                                        <button type="submit" name="regulation_submit_en" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <!-- /.tab-pane -->
                    {{--<div class="tab-pane" id="khmer">--}}
                    {{--<form class="form-horizontal">--}}
                    {{--<div class="form-group">--}}
                    {{--<label for="inputTitleKh" class="col-sm-3 control-label">ចំណងជើង</label>--}}

                    {{--<div class="col-sm-10">--}}
                    {{--<input class="form-control" id="inputTitleKh" placeholder="ចំណងជើង..." type="text">--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--uplord logo--}}
                    {{--<div class="form-group">--}}
                    {{--<label for="inputLogoKh" class="col-sm-3 control-label">អាប់ឡូតរូប</label>--}}
                    {{--<input id="inputLogokh" type="file" class="col-sm-10">--}}
                    {{--<p class="help-block col-sm-10 " >ឧទាហរណ៍ abc.JPG / PNG</p>--}}
                    {{--</div>--}}
                    {{--url--}}
                    {{--<div class="form-group">--}}
                    {{--<label for="inputUrlKh" class="col-sm-3 control-label">អាសយដ្ឋាននៃគេហទំព័រ</label>--}}
                    {{--<div class="col-sm-10">--}}
                    {{--<input class="form-control" id="inputUrlKh" placeholder="អាសយដ្ឋាន..." type="url">--}}
                    {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="form-group">--}}
                    {{--<div class="col-sm-offset-2 col-sm-10">--}}
                    {{--<button type="submit" class="btn btn-danger">បញ្ជូន</button>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</form>--}}
                    {{--</div>--}}
                    <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- /.nav-tabs-custom -->

            </div>
        </section>
    </div>

@stop






























