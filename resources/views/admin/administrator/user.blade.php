
@extends('layouts.admin')
@section('content')
    <style>
        .center_content{
            text-align: center;
        }
    </style>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Administrator
            {{--<small>advanced tables</small>--}}
          </h1>
          <ol class="breadcrumb">
              <li class=""><a href="{{url('dashboard')}}"><i class="fa fa fa-th"></i>Dashboard</a></li>
              <li class="active">Administrator</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            {{--<h3 class="box-title">Administrator</h3>--}}
                            <a href="{{url('dashboard/user-add')}}" class="btn btn-sm btn-primary">
                                <span class="glyphicon glyphicon-plus"></span> Add New
                            </a>
                        </div>

                        <table class="table table-bordered" id="users-table">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>USER NAME</th>
                                <th>ADDRESS</th>
                                <th>EMAIL</th>
                                <th>PHONE NUMBER</th>
                                <th>ROLE</th>
                                <th>ACTION</th>
                            </tr>
                            </thead>
                        </table>

                        <!-- /.box-header -->
                        {{--<div class="box-body">--}}
                            {{--<div class="box box-primary">--}}
                                {{--<table class="example table table-bordered table-striped">--}}
                                    {{--<thead>--}}
                                    {{--<tr>--}}
                                        {{--<th class="center_content">No</th>--}}
                                        {{--<th class="center_content">USER NAME</th>--}}
                                        {{--<th class="center_content">COMPANY NAME</th>--}}
                                        {{--<th class="center_content">ADDRESS</th>--}}
                                        {{--<th class="center_content">EMAIL</th>--}}
                                        {{--<th class="center_content">PHONE NUMBER</th>--}}
                                        {{--<th class="center_content">ROLE</th>--}}
                                        {{--<th class="center_content">ACTION</th>--}}
                                    {{--</tr>--}}
                                    {{--</thead>--}}
                                    {{--<tbody>--}}

                                    {{--<?php //dd($user->userid); exit(); ?>--}}

                                    {{--@if(0 < count($users) )--}}
                                        {{--<?php $i=1; ?>--}}
                                           {{--@foreach($users as $user)--}}

                                            {{--<tr role="row" class="odd center_content ">--}}
                                                {{--<td> {{ $i++ }}</td>--}}
                                                {{--<td>{{ $user->name }}</td>--}}
                                                {{--<td>{{ $user->company_name }}</td>--}}
                                                {{--<td>{{ $user->address }}</td>--}}
                                                {{--<td>{{ $user->email }}</td>--}}
                                                {{--<td>{{ $user->phone_number }}</td>--}}
                                                {{--<td>{{ $user->role }}</td>--}}
                                                {{--<td>{{ dateFormat($user->created_at) }}</td>--}}
                                                {{--<td>{{ dateFormat($user->updated_at) }}</td>--}}
                                                {{--<td >--}}

                                                    {{--<a href="{{url('dashboard/user-show',$user->id )}}" class="btn btn-sm btn-primary">--}}
                                                        {{--<span class="glyphicon glyphicon-search"></span> Show--}}
                                                    {{--</a>--}}

                                                    {{--<a href="{{url('dashboard/user-edit', $user->id )}}" class="btn btn-sm btn-success">--}}
                                                        {{--<span class="glyphicon glyphicon-pencil"></span> Edit--}}
                                                    {{--</a>--}}
                                                    {{--<a href="{{url('dashboard/user-delete', $user->id )}}" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure you want to delete?')" data-id="{{ $user->id }}" data-token="{{ csrf_token() }}" >--}}
                                                     {{--<span class="glyphicon glyphicon-remove"></span> Delete--}}
                                                    {{--</a>--}}

                                                {{--</td>--}}
                                            {{--</tr>--}}
                                            {{--@endforeach--}}

                                         {{--@else--}}
                                            {{--<tr>--}}
                                                {{--<td colspan="8">No data.</td>--}}
                                            {{--</tr>--}}
                                     {{--@endif--}}

                                    {{--</tbody>--}}
                                    {{--<tfoot>--}}
                                    {{--<tr>--}}
                                        {{--<th class="center_content">No</th>--}}
                                        {{--<th class="center_content">USER NAME</th>--}}
                                        {{--<th class="center_content">EMAIL</th>--}}
                                        {{--<th class="center_content">CREATE BY</th>--}}
                                        {{--<th class="center_content">UPDATE BY</th>--}}
                                        {{--<th class="center_content">UPDATE DATE</th>--}}
                                        {{--<th class="center_content">ACTION</th>--}}
                                    {{--</tr>--}}
                                    {{--</tfoot>--}}
                                {{--</table>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <!-- /.box-body -->

                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@stop

<script>
    $(function() {
        $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('datatables.data') !!}',
            columns: [
                { data: 'id', name: 'id' },
                { data: 'name', name: 'name' },
                { data: 'company_name', name: 'company_name' },
                { data: 'address', name: 'address' },
                { data: 'email', name: 'email' },
                { data: 'phone_number', name: 'phone_number' },
                { data: 'role', name: 'role' }
            ]
        });
    });
</script>
@endpush





