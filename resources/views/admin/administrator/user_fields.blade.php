
{{--Full name--}}
<div class="form-group{{ $errors->has('name_en') ? ' has-error' : '' }}">
    <label for="admin_fullname_en" class="col-sm-3 control-label">Full Name</label>
    <div class="col-sm-3">
        <input class="form-control" id="admin_fullname_en" name="name_en" value="{{ isset($user) ? $user->name : '' }}" placeholder="full name..." type="text" required>
        @if ($errors->has('name_en'))
            <span class="help-block">
                <strong>{{ $errors->first('name_en') }}</strong>
            </span>
        @endif
    </div>
</div>

{{--Password--}}
<div class="form-group{{ $errors->has('password_en') ? ' has-error' : '' }}">
    <label for="admin_password_en" class="col-sm-3 control-label">Password</label>
    <div class="col-sm-3">
        <input class="form-control" id="admin_password_en" name="password_en" placeholder="password..." type="password" required>
        @if ($errors->has('password_en'))
            <span class="help-block">
                <strong>{{ $errors->first('password_en') }}</strong>
            </span>
        @endif
    </div>
</div>

{{--Confirm Password--}}
<div class="form-group{{ $errors->has('confirm_password_en') ? ' has-error' : '' }}">
    <label for="admin_confirm_password_en" class="col-sm-3 control-label">Confirm Password</label>
    <div class="col-sm-3">
        <input class="form-control" id="admin_confirm_password_en" name="confirm_password_en" placeholder="password..." type="password" required>
        @if ($errors->has('confirm_password_en'))
            <span class="help-block">
                <strong>{{ $errors->first('confirm_password_en') }}</strong>
            </span>
        @endif
    </div>
</div>

{{--Company Name--}}
<div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
    <label for="admin_phone_number" class="col-sm-3 control-label">Phone Number</label>
    <div class="col-sm-3">
        <input class="form-control" id="admin_phone_number" name="phone_number" value="{{ isset($user) ? $user->phone_number : '' }}" placeholder="Phone Number..." type="text" required>
        @if ($errors->has('phone_number'))
            <span class="help-block">
                <strong>{{ $errors->first('phone_number') }}</strong>
            </span>
        @endif
    </div>
</div>

{{--Address--}}
<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
    <label for="admin_address" class="col-sm-3 control-label">Address</label>
    <div class="col-sm-3">
        <input class="form-control" id="admin_address" name="address" value="{{ isset($user) ? $user->address : '' }}" placeholder="Address..." type="text" required>
        @if ($errors->has('address'))
            <span class="help-block">
                <strong>{{ $errors->first('address') }}</strong>
            </span>
        @endif
    </div>
</div>

{{--Email--}}
<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
    <label for="admin_email_en" class="col-sm-3 control-label">Email</label>
    <div class="col-sm-3">
        <input class="form-control" id="admin_email_en" name="email" placeholder="email..." value="{{ isset($user) ? $user->email : '' }}" type="email" required>
        @if ($errors->has('email'))
            <span class="help-block">
                 <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
</div>

{{--Company Name--}}
<div class="form-group{{ $errors->has('company_name') ? ' has-error' : '' }}">
    <label for="admin_company_name" class="col-sm-3 control-label">Company Name</label>
    <div class="col-sm-3">
        <input class="form-control" id="admin_company_name" name="company_name" value="{{ isset($user) ? $user->company_name : '' }}" placeholder="Company Name..." type="text" required>
        @if ($errors->has('company_name'))
            <span class="help-block">
                <strong>{{ $errors->first('company_name') }}</strong>
            </span>
        @endif
    </div>
</div>

{{--<div class="form-group">--}}
    {{--<label for="admin_email_en" class="col-sm-3 control-label">Avatar</label>--}}
    {{--<div class="col-sm-3">--}}
        {{--<input class="form-control" name="image" value="{{ isset($user) ? $user->image : '' }}" type="file" />--}}
    {{--</div>--}}
{{--</div>--}}
