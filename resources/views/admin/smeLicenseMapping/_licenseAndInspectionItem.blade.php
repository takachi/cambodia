<div class="col-sm-12 padding-lr-0 parent-item">
    <?php $i = 0; ?>
    @if($licenseAndInspection)
        @foreach($licenseAndInspection->item as $item)
            <fieldset class="parent">
                <button class="btn-delete-item" type="button" onclick="$(this).parent().remove();console.log('success')" style="position: absolute;right: -1px;top: 9px;">x</button>
                <legend>Item {{ $i ? $i : '' }}</legend>

                <label for="">Title</label>
                <input name="license_inspection[{{$licenseAndInspectionType}}][item][{{ $i }}][title_en]" value="{{ isset($item) ? $item->title_en : '' }}" class="dynamic form-control" placeholder="Title">

                <label>Description</label>
                <input name="license_inspection[{{$licenseAndInspectionType}}][item][{{ $i }}][description_en]" value="{{ isset($item) ? $item->description_en : '' }}" class="dynamic form-control">

            </fieldset>
            <?php $i++; ?>
        @endforeach
    @else
        <fieldset>
            <button class="btn-delete-item" type="button" onclick="$(this).parent().remove();console.log('success')" style="position: absolute;right: -1px;top: 9px;">x</button>
            <legend>Item</legend>

            <label for="">Title</label>
            <input name="license_inspection[{{$licenseAndInspectionType}}][item][0][title_en]" value="" class="dynamic form-control" placeholder="Title">

            <label>Description</label>
            <input name="license_inspection[{{$licenseAndInspectionType}}][item][0][description_en]" value="" class="dynamic form-control" placeholder="Description">

        <!-- <label>Link</label>
            <input name="license_inspection[{{$licenseAndInspectionType}}][item][0][link]" value="" class="dynamic form-control" placeholder="Link"> -->
        </fieldset>
    @endif
    <button class="add-new-item btn btn-default btn-sm margin-left-subtract-15" id="add-new-item" value="{{ $licenseAndInspectionType }}" type="button"><i class="glyphicon glyphicon-plus"></i> item</button>
</div>
