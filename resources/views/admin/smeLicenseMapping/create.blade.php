
@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                SME License Mapping
            </h1>
            <ol class="breadcrumb">
                <li class=""><a href="{{url('dashboard')}}"><i class="fa fa fa-th"></i>Dashboard</a></li>
                <li class=""><a href="{{url('dashboard/sme-licence-mapping')}}"><i class="fa fa fa-folder"></i>SME Licence Mapping</a></li>
                <li class="active"><i class="fa fa fa-list"></i> Add New</li>
            </ol>
        </section>

        <section class="content clearfix">
            <div class="col-xs-12">
                <form class="form-horizontal" action="{{ action('admin\LicenseMappingController@store') }} " method="POST" enctype="multipart/form-data">
                    @include('admin.smeLicenseMapping._form')
                    <div class="form-group">
                        <button class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </section>
    </div>
@stop
@section('internalJsFooter')
<script>
  $(document).ready(function(){
    $('.add-new-item').on('click', function(){
      var currentFieldset = $(this).parent().children().first();
      var item = currentFieldset.clone();
      var length = currentFieldset.parent().find('fieldset').length;
      var dynamicFormElement = item.find('.dynamic');
      var dynamicLegendElement = item.find('legend');
      $(dynamicLegendElement[0]).text('Item '+ length);
      $(dynamicFormElement[0]).prop({ name: "license_inspection["+this.value+"][item]["+length+"][title_en]"}).val("");
      $(dynamicFormElement[1]).prop({ name: "license_inspection["+this.value+"][item]["+length+"][description_en]" }).val("");
      $(dynamicFormElement[2]).prop({ name: "license_inspection["+this.value+"][item]["+length+"][link]" }).val("");
      $(this).before(item);
    });

    $('.add-faqs-item').on('click', function(){
      var currentFieldset = $(this).parent().children().first();
      var item = currentFieldset.clone();
      var length = currentFieldset.parent().find('fieldset').length;
      var dynamicFormElement = item.find('.dynamic');
      var dynamicLegendElement = item.find('legend');
      $(dynamicLegendElement[0]).text('Item '+ length);
      $(dynamicFormElement[0]).prop({ name: "faqs_item["+length+"][question_en]"}).val("");
      $(dynamicFormElement[1]).prop({ name: "faqs_item["+length+"][answer_en]" }).val("");
      $(this).before(item);
    });
  });
</script>
@endsection
