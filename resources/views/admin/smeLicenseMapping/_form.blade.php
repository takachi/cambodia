<div class="form-group">
    <label for="title">Title</label><i class="text-red">*</i>
    <input name="title_en" id="title" class="form-control" value="{{ isset($regulationSMELicenseMapping) ? $regulationSMELicenseMapping->title_en : '' }}" placeholder="Title" required>
</div>
<div class="form-group">
    <label for="menu-order">Menu order</label>
    <input type="number" name="ordering" id="menu-order" class="form-control" value="{{ isset($regulationSMELicenseMapping) ? $regulationSMELicenseMapping->ordering : '' }}" placeholder="Number">
</div>
<div class="form-group">
    <label for="thumbnail">Thumbnail (275x X 295px)</label>
    <input type="file" id="thumbnail" class="form-control test" name="thumbnail">
</div>
<div class="form-group">
    <label for="banner">Banner (1920x X 280px)</label>
    <input type="file" id="banner" class="form-control test" name="banner">
</div>
<div class="form-group">
    <label for="diagram">Diagram (495px X 450px)</label>
    <input type="file" id="diagram" class="form-control test" name="diagram_en">
</div>
<div class="form-group">
    <label for="initiative_thumb_en">Initiative (495px X 450px)</label>
    <input type="file" id="initiative_thumb_en" class="form-control test" name="initiative_thumb_en">
</div>
<div class="form-group">
    <label for="document">Document</label>
    <input type="file" id="document" class="form-control test" name="document_en">
</div>
@include('admin.smeLicenseMapping._licenseAndInspection')
