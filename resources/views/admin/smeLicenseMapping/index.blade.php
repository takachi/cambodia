
@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                SME Licence Mapping
            </h1>
            <ol class="breadcrumb">
                <li class=""><a href="{{url('dashboard')}}"><i class="fa fa fa-th"></i>Dashboard</a></li>
                <li class="active"><i class="fa fa fa-folder"></i> SME Licence Mapping</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#license_mapping_item" data-toggle="tab">License Mapping Items</a></li>
                        <li><a href="#hero_banner" data-toggle="tab">Hero Banner</a></li>
                        <li><a href="#page_title" data-toggle="tab">Page Title</a></li>
                    </ul>
                    <div class="tab-content">
                        @include('partials.flushMessage')
                        <div class="active tab-pane clearfix" id="license_mapping_item">
                            <div class="box">
                                <div class="box-header">
                                    <a href="{{url('dashboard/sme-licence-mapping/create')}}" class="btn btn-sm btn-primary">
                                        <span class="glyphicon glyphicon-plus"></span> Add New
                                    </a>
                                </div>
                                <div class="box box-primary">
                                    <div class="box-body">
                                        <table class="default-data-table table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th class="center_content">@lang('label.title')</th>
                                                    <th class="center_content">@lang('label.thumbnail')</th>
                                                    <th class="center_content">@lang('label.document')</th>
                                                    <th class="center_content">@lang('label.order')</th>
                                                    <th class="center_content">@lang('label.created_at')</th>
                                                    <th class="center_content">@lang('label.updated_at')</th>
                                                    <th class="center_content">@lang('label.faqs')</th>
                                                    <th class="center_content">@lang('label.action')</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($licenseMappings as $licenseMapping)

                                                    <tr role="row" class="odd center_content ">
                                                        <td>{{ $licenseMapping->title_en }}</td>
                                                        <td>
                                                            <img class="media-object"
                                                                 src="{{ $licenseMapping->thumbnail  ? asset($licenseMapping->thumbnail) : '' }}"
                                                                 alt="User Avatar"
                                                                 rel="popover"
                                                                 style="height: 40px;"
                                                                 data-img="{{ $licenseMapping->thumbnail  ? asset($licenseMapping->thumbnail) : '' }}"
                                                                 title="{{ $licenseMapping->title_en }}">
                                                        </td>
                                                        <td>
                                                            <?php $fileName = explode('/', $licenseMapping->document_en); ?>
                                                            <a target="_blank" href="{{ asset($licenseMapping->document_en) }}">{{ $fileName[count($fileName)-1] }}</a>
                                                        </td>
                                                        <td>{{ $licenseMapping->ordering }}</td>
                                                        <td>{{ dateFormat($licenseMapping->created_at) }}</td>
                                                        <td>{{ dateFormat($licenseMapping->updated_at) }}</td>
                                                        <td>
                                                            <a title="Manage FAQs" href="{{ action('admin\FaqsItemController@index', $licenseMapping->id) }}" class="btn btn-xs btn-success">
                                                                {{ count($licenseMapping->faqsItems) }}
                                                                | <i class="glyphicon glyphicon-plus"></i>
                                                            </a>
                                                        </td>
                                                        <td>
                                                            @include('partials._actionId', ['entityName' => 'sme-licence-mapping', 'entity' => $licenseMapping])
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane clearfix" id="hero_banner">
                            @include('admin.contentElement._heroBanner', [
                            'contentElementId' =>  isset($ceHeroBanner) ? $ceHeroBanner->id : null,
                            'contentElementName' => \App\Models\ContentElement::LICENSE_MAPPING
                            ])
                        </div>
                        <div class="tab-pane clearfix" id="page_title">
                            @include('admin.contentElement._pageTitle', [
                            'contentElementId' =>  isset($cePageTitle) ? $cePageTitle->id : null,
                            'contentElementName' => \App\Models\ContentElement::LICENSE_MAPPING
                            ])
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop





