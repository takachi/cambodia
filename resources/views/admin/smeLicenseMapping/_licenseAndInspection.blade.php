<?php
    if (isset($regulationSMELicenseMapping)) {
    $licenseInspection = json_decode($regulationSMELicenseMapping->license_inspection);
    $primaryLicense = $licenseInspection->primary_license;
    $otherLicense = $licenseInspection->other_license;
    $applicationProcess = $licenseInspection->application_process;
    $businessOperation = $licenseInspection->business_operation;
    }
?>
<div class="row">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#primary-license" data-toggle="tab">Primary License</a></li>
            <li><a href="#other-license" data-toggle="tab">Other License</a></li>
            <li><a href="#application-process" data-toggle="tab">Inspections during Application Process</a></li>
            <li><a href="#business-operation" data-toggle="tab">Inspections during Business Operation</a></li>
        </ul>
        <div class="tab-content">
            <div class="active tab-pane clearfix" id="primary-license">
                <label for="">Title</label>
                <input name="license_inspection[primary_license][title_en]" class="form-control" value="{{ isset($primaryLicense) ? $primaryLicense->title_en : '' }}" placeholder="Primary License"><br>
                <label for="">Slogan</label>
                <input name="license_inspection[primary_license][slogan_en]" class="form-control" value="{{ isset($primaryLicense->slogan_en) ? $primaryLicense->slogan_en : '' }}" placeholder="Slogan"><br>
                @include('admin.smeLicenseMapping._licenseAndInspectionItem', ['licenseAndInspection' => isset($primaryLicense) ? $primaryLicense : '', 'licenseAndInspectionType' => 'primary_license'])
            </div>
            <div class="tab-pane clearfix" id="other-license">
                <label for="">Title</label>
                <input name="license_inspection[other_license][title_en]" class="form-control" value="{{ isset($otherLicense) ? $otherLicense->title_en : '' }}" placeholder="Other License"><br>
                <label for="">Slogan</label>
                <input name="license_inspection[other_license][slogan_en]" class="form-control" value="{{ isset($otherLicense->slogan_en) ? $otherLicense->slogan_en : '' }}" placeholder="Slogan"><br>
                @include('admin.smeLicenseMapping._licenseAndInspectionItem', ['licenseAndInspection' => isset($otherLicense) ? $otherLicense : '', 'licenseAndInspectionType' => 'other_license'])

            </div>
            <div class="tab-pane clearfix" id="application-process">
                <label for="">Title</label>
                <input name="license_inspection[application_process][title_en]" class="form-control" value="{{ isset($applicationProcess) ? $applicationProcess->title_en : '' }}" placeholder="Application process"><br>
                @include('admin.smeLicenseMapping._licenseAndInspectionItem', ['licenseAndInspection' => isset($applicationProcess) ? $applicationProcess : '', 'licenseAndInspectionType' => 'application_process'])
            </div>
            <div class="tab-pane clearfix" id="business-operation">
                <label for="">Title</label>
                <input name="license_inspection[business_operation][title_en]" class="form-control" value="{{ isset($businessOperation) ? $applicationProcess->title_en : '' }}" placeholder="Business operation"><br>
                @include('admin.smeLicenseMapping._licenseAndInspectionItem', ['licenseAndInspection' => isset($businessOperation) ? $businessOperation : '', 'licenseAndInspectionType' => 'business_operation'])
            </div>
        </div>
    </div>
</div>
