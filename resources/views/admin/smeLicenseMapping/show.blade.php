<?php
if (isset($regulationSMELicenseMapping)) {
    $licenseInspection = json_decode($regulationSMELicenseMapping->license_inspection);
    $primaryLicense = $licenseInspection->primary_license;
    $otherLicense = $licenseInspection->other_license;
    $applicationProcess = $licenseInspection->application_process;
    $businessOperation = $licenseInspection->business_operation;
}
?>
@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Directory Show
            </h1>
            <ol class="breadcrumb">
                <li class=""><a href="{{url('dashboard')}}"><i class="fa fa fa-th"></i>Dashboard</a></li>
                <li class=""><a href="{{url('dashboard/directories')}}"><i class="fa fa fa-folder"></i>Directories</a></li>
                <li class="active"><i class="fa fa fa-list"></i> Directory show</li>
            </ol>
        </section>

        <section class="content clearfix">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <h2>{{ $regulationSMELicenseMapping->title_en }}</h2>
                    @if($regulationSMELicenseMapping->thumbnail)
                    <p>Thumbnail</p>
                    <img class="img-responsive"
                         src="{{ $regulationSMELicenseMapping->thumbnail  ? asset('uploads/'.$regulationSMELicenseMapping->thumbnail) : asset('assets/images/sample_logo.png') }}"
                         alt="{{ $regulationSMELicenseMapping->title_en }}">
                    @endif
                    @if($regulationSMELicenseMapping->banner)
                        <p>Banner</p>
                        <img class="img-responsive"
                             src="{{ $regulationSMELicenseMapping->banner  ? asset('uploads/'.$regulationSMELicenseMapping->banner) : asset('assets/images/sample_logo.png') }}"
                             alt="{{ $regulationSMELicenseMapping->title_en }}"
                             style="max-height: 200px;">
                    @endif
                    @if($regulationSMELicenseMapping->diagram_en)
                        <p>Diagram</p>
                        <img class="img-responsive"
                             src="{{ $regulationSMELicenseMapping->diagram_en  ? asset('uploads/'.$regulationSMELicenseMapping->diagram_en) : asset('assets/images/sample_logo.png') }}"
                             alt="{{ $regulationSMELicenseMapping->diagram_en }}"
                             style="max-height: 200px;">
                    @endif
                    @if($regulationSMELicenseMapping->initiative_thumb_en)
                        <p>Initiative</p>
                        <img class="img-responsive"
                             src="{{ $regulationSMELicenseMapping->initiative_thumb_en  ? asset('uploads/'.$regulationSMELicenseMapping->diagram_en) : asset('assets/images/sample_logo.png') }}"
                             alt="{{ $regulationSMELicenseMapping->initiative_thumb_en }}"
                             style="max-height: 200px;">
                    @endif
                    <h4>{{ $primaryLicense->title_en }}</h4>
                    <p>{{ $primaryLicense->slogan_en }}</p>
                    <hr>
                    @foreach($primaryLicense->item as $item )
                        <h5>{{ $item->title_en }}</h5>
                        <p>{{ $item->description_en }}</p>
                    @endforeach
                </div>
            </div>
        </section>
    </div>
@stop










