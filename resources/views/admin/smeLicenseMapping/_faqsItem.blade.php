<div class="col-sm-12 padding-lr-0 parent-item">
<?php $i = 0; ?>
    @if($faqs != null && $faqs != ' ')
        @foreach($faqs as $item)
            <fieldset class="parent">
                <button class="btn-delete-item" type="button" onclick="$(this).parent().remove();console.log('success')" style="position: absolute;right: -1px;top: 9px;">x</button>
                <legend>Item {{ $i ? $i : '' }}</legend>

                <label for="">Question</label>
                <input name="faqs_item[{{ $i }}][question_en]" value="{{ isset($item) ? $item->question_en : '' }}" class="dynamic form-control" placeholder="Question">

                <label>Description</label>
                <textarea name=faqs_item[{{ $i }}][answer_en]" rows="4" class="dynamic form-control">{{ isset($item) ? $item->answer_en : '' }}</textarea>
            </fieldset>
            <?php $i++; ?>
        @endforeach
    @else
        <fieldset class="parent">
            <button class="btn-delete-item" type="button" onclick="$(this).parent().remove();console.log('success')" style="position: absolute;right: -1px;top: 9px;">x</button>
            <legend>Item {{ $i ? $i : '' }}</legend>

            <label for="">Question</label>
            <input name="faqs_item[{{ $i }}][question_en]" class="dynamic form-control" placeholder="Question">

            <label>Answer</label>
            <textarea name=faqs_item[{{ $i }}][answer_en]" rows="4" class="dynamic form-control"></textarea>
        </fieldset>
    @endif
    <button class="add-faqs-item btn btn-default btn-sm margin-left-subtract-15" type="button"><i class="glyphicon glyphicon-plus"></i> item</button>
</div>
