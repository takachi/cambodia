<div class="form-group">
    <label for="title">Title</label><i class="text-red">*</i>
    <input name="title_en" id="title" class="form-control" value="{{ isset($smePolicy) ? $smePolicy->title_en : '' }}" placeholder="Title" required>
</div>
<!-- todo: this field must use editor -->
<div class="form-group">
    <label for="about">Description</label>
    <textarea name="description_en" id="description" class="form-control" rows="5">{{ isset($smePolicy) ? $smePolicy->description_en : '' }}</textarea>
</div>
<div class="form-group">
    <label for="reference">Reference</label>
    <input type="file" id="reference" class="form-control" name="reference_en" value="{{ isset($smePolicy) ? $smePolicy->reference_en : '' }}">
</div>
<div class="form-group">
    <label>Status</label> <br>
    <div class="col-sm-1">
        <input id="inactive" type="radio" name="status" value="0" checked>
        <label for="inactive">Inactive</label>
    </div>
    <div class="col-sm-1">
        <input id="active" type="radio" name="status" value="1" {{ isset($smePolicy) ? (($smePolicy->status == 1) ? 'checked' : '') : '' }} @if (isset($status) && $status >= 4) title="Pin cannot more than 4" disabled @endif >
        <label for="active">Active</label>
    </div>
    <br>
</div>
