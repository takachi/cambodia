
@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                SME Policy and Frameworks
            </h1>
            <ol class="breadcrumb">
                <li class=""><a href="{{url('dashboard')}}"><i class="fa fa fa-th"></i>Dashboard</a></li>
                <li class="active"><i class="fa fa fa-folder"></i> Directories</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#sme_policy_item" data-toggle="tab">SME Policy and Frameworks Items</a></li>
                        <li><a href="#hero_banner" data-toggle="tab">Hero Banner</a></li>
                        <li><a href="#page_title" data-toggle="tab">Page Title</a></li>
                    </ul>
                    <div class="tab-content">
                        @include('partials.flushMessage')
                        <div class="active tab-pane clearfix" id="sme_policy_item">
                            <div class="box">
                                <div class="box-header">
                                    <a href="{{url('dashboard/smepolicy/create')}}" class="btn btn-sm btn-primary">
                                        <span class="glyphicon glyphicon-plus"></span> Add New
                                    </a>
                                </div>
                                <div class="box-body">
                                    <table class="default-data-table table table-bordered table-striped">
                                        <div class="box box-primary">
                                            <div class="box-header with-border">
                                                <h3 class="box-title">List SME Policy and Frameworks</h3>
                                            </div>
                                            <thead>
                                            <tr>
                                                <th class="center_content">@lang('label.title')</th>
                                                <th class="center_content">@lang('label.reference')</th>
                                                <th class="center_content">@lang('label.date')</th>
                                                <th class="center_content">@lang('label.modifyDate')</th>
                                                <th class="center_content">@lang('label.status')</th>
                                                <th class="center_content">@lang('label.action')</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            @foreach($smePolicies as $smePolicy)
                                                <tr role="row" class="odd center_content">
                                                    <td>{{ $smePolicy->title_en }}</td>
                                                    <td>
                                                        <!-- todo: should display original file name -->
                                                        <?php $fileName = explode('/', $smePolicy->reference_en); ?>
                                                        <a target="_blank" href="{{ asset($smePolicy->reference_en) }}">{{ $smePolicy->reference_en ? $fileName[count($fileName)-1] : '' }}</a>
                                                    </td>
                                                    <td>{{ dateFormat($smePolicy->created_at) }}</td>
                                                    <td>{{ dateFormat($smePolicy->updated_at) }}</td>
                                                    <td>{{ $smePolicy->status ? 'Active' : 'Inactive' }}</td>
                                                    <td>
                                                        @include('partials._actionId', ['entityName' => 'smepolicy', 'entity' => $smePolicy])
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </div>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane clearfix" id="hero_banner">
                            @include('admin.contentElement._heroBanner', [
                            'contentElementId' =>  isset($ceHeroBanner) ? $ceHeroBanner->id : null,
                            'contentElementName' => \App\Models\ContentElement::SME_POLICY
                            ])
                        </div>
                        <div class="tab-pane clearfix" id="page_title">
                            @include('admin.contentElement._pageTitle', [
                            'contentElementId' =>  isset($cePageTitle) ? $cePageTitle->id : null,
                            'contentElementName' => \App\Models\ContentElement::SME_POLICY
                            ])
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop






