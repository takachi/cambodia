<div class="form-group">
    <label for="title">Title</label><i class="text-red">*</i>
    <input name="title_en" id="title" class="form-control" value="{{ isset($directory) ? $directory->title_en : '' }}" placeholder="Title" required>
</div>
<div class="form-group">
    <label for="phone">Phone</label>
    <input name="phone" id="phone" class="form-control" value="{{ isset($directory) ? $directory->phone : '' }}" placeholder="Phone">
</div>
<div class="form-group">
    <label for="mail">Email</label>
    <input name="mail" id="mail" class="form-control" value="{{ isset($directory) ? $directory->mail : '' }}" placeholder="Email">
</div>
<div class="form-group">
    <label for="website">Website</label>
    <input name="website" id="website" class="form-control" value="{{ isset($directory) ? $directory->website : '' }}" placeholder="Website">
</div>
<div class="form-group">
    <label for="address">Address</label>
    <input name="address_en" id="address" class="form-control" value="{{ isset($directory) ? $directory->address_en : '' }}" placeholder="Address">
</div>
<!-- todo: this field must use editor -->
<div class="form-group">
    <label for="about">About</label>
    @include('partials.tinymce', ['name' => 'about_en', 'entity' => isset($directory) ? $directory : ''])
    {{--<textarea name="about_en" id="about" class="form-control" rows="5">{{ isset($directory) ? $directory->about_en : '' }}</textarea>--}}
</div>
<!-- todo: this field must use editor -->
<div class="form-group">
    <label for="service">Service</label>
{{--    {{ dd($directory) }}--}}
    @include('partials.tinymce', ['name' => 'service_en', 'entity' => isset($directory) ? $directory : ''])
    {{--<textarea name="service_en" id="service" class="form-control" rows="5">{{ isset($directory) ? $directory->service_en : '' }}</textarea>--}}
</div>
<div class="form-group">

    <label for="latitude">Latitude</label>
    <?php $toolTipMap = 'Follow the link to get latitude and longitude'?>
    <a href="https://www.gps-coordinates.net" target="_blank">
        <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="{{ $toolTipMap }}"></i>
    </a>
    <input id="latitude" class="form-control" name="latitude" value="{{ isset($directory) ? $directory->latitude : '' }}" placeholder="Latitude">
</div>
<div class="form-group">
    <label for="longitude">Longitude</label>
    <input id="longitude" class="form-control" name="longitude" value="{{ isset($directory) ? $directory->longitude : '' }}" placeholder="Longitude">
</div>
<div class="form-group">
    <label for="logo">Logo</label>
    <input type="file" id="logo" class="form-control" name="logo" value="{{ isset($directory) ? $directory->logo : '' }}">
</div>
<div class="form-group">
    <label>Pin</label> <br>
    <div class="col-sm-1">
        <input id="ping-no" type="radio" name="pin" value="0" checked>
        <label for="ping-no">No</label>
    </div>
    <div class="col-sm-1">
        <input id="ping-yes" type="radio" name="pin" value="1" {{ isset($directory) ? (($directory->pin == 1) ? 'checked' : '') : '' }} @if ($pin >= 4) title="Pin cannot more than 4" disabled @endif >
        <label for="ping-yes">Yes</label>
    </div>
    <br>
</div>

<div class="form-group">
    <label for="service_type">Service type</label>
    {!! Form::select('service_type[]', $services, isset($directory) ? $directory->services->pluck('id')->toArray() : null,  ['class' => 'form-control height-200', 'multiple']) !!}
</div>
