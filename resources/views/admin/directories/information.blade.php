
@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Information
            </h1>
            <ol class="breadcrumb">
                <li class=""><a href="{{url('dashboard')}}"><i class="fa fa fa-th"></i>Dashboard</a></li>
                <li class="active"><i class="fa fa fa-folder"></i> Information</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    @include('partials.flushMessage')
                    <div class="box">
                        <div class="box-header">
                            <a href="{{url('dashboard/directory/create')}}" class="btn btn-sm btn-primary">
                                <span class="glyphicon glyphicon-plus"></span> Add New
                            </a>
                        </div>
                        <div class="box-body">
                            <table class="default-data-table table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>@lang('label.logo')</th>
                                    <th>@lang('label.title')</th>
                                    <th>@lang('label.service')</th>
                                    <th>@lang('label.directoryLogo')</th>
                                    <th>@lang('label.date')</th>
                                    <th>@lang('label.modifyDate')</th>
                                    <th>@lang('label.pin')</th>
                                    <th>@lang('label.action')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($directories as $directory)

                                    <tr role="row" class="odd center_content ">
                                        <td>
                                            <img class="media-object"
                                                 src="{{ $directory->logo  ? asset($directory->logo) : asset('assets/images/sample_logo.png') }}"
                                                 alt="User Avatar"
                                                 rel="popover"
                                                 style="height: 25px;"
                                                 data-img="{{ $directory->logo  ? asset($directory->logo) : asset('assets/images/sample_logo.png') }}"
                                                 title="{{ $directory->title }}">
                                        </td>
                                        {{--<td>{{ lang($directory, 'title') }}</td>--}}
                                        <td>{{$directory-> title_en}}</td>
                                        <td>{{ $directory->services->implode('title_en', ', ') }}</td>
                                        <td>
                                            <a title="Manage Carousel" href="{{ action('admin\DirectoryPartnerController@index')}}?directory={{ $directory->id }}" class="btn btn-xs btn-success">
                                                {{ count($directory->partners) }}
                                                | <i class="glyphicon glyphicon-plus"></i>
                                            </a>
                                        </td>
                                        <td>{{ dateFormat($directory->created_at) }}</td>
                                        <td>
                                        {{ dateFormat($directory->updated_at) }}
                                        <td>{{ $directory->pin ? 'Yes' : '--' }}</td>
                                        <td>
                                            @include('partials._action', ['entityName' => 'directory', 'entity' => $directory])
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop








