
@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Information
            </h1>
            <ol class="breadcrumb">
                <li class=""><a href="{{url('dashboard')}}"><i class="fa fa fa-th"></i>Dashboard</a></li>
                <li class=""><a href="{{url('dashboard/information')}}"><i class="fa fa fa-folder"></i>Information</a></li>
                <li class="active"><i class="fa fa fa-list"></i> Edit</li>
            </ol>
        </section>

        <section class="content clearfix">
            <div class="col-xs-12">
                {!! Form::model($directory, ['method' => 'PUT', 'route' => ['dashboard.directory.update', $directory ],'files' => 'true']) !!}
                    @include('admin.directories._form')
                    @if(isset($directory->logo))
                        <div class="form-group">
                            <img src="{{ asset('uploads/'.$directory->logo) }}" alt="{{ asset('uploads/'.$directory->title) }}" class="logo-preview">
                        </div>
                    @endif
                    <div class="form-group">
                        <button class="btn btn-primary">Update</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </section>
    </div>
@stop







