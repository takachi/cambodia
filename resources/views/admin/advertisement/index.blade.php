
@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Advertisements
            </h1>
            <ol class="breadcrumb">
                <li class=""><a href="{{url('dashboard')}}"><i class="fa fa fa-th"></i>Dashboard</a></li>
                <li class="active"><i class="fa fa fa-folder"></i> Advertisements</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    @include('partials.flushMessage')
                    <div class="box">
                        <div class="box-header">
                            <?php $service = isset($_GET['service']) ? $_GET['service'] : null ?>
                            <a href="{{url('dashboard/advertisement/create')}}{{ isset($service) ? '?service='.$service : ''   }}" class="btn btn-sm btn-primary">
                                <span class="glyphicon glyphicon-plus"></span> Add New
                            </a>
                        </div>
                        <div class="box-body">
                            <table class="default-data-table table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>@lang('label.banner')</th>
                                    <th class="hidden">@lang('label.title')</th>
                                    <th>@lang('label.page')</th>
                                    <th>@lang('label.active_at')</th>
                                    <th>@lang('label.expired_at')</th>
                                    <th>@lang('label.created_at')</th>
                                    <th>@lang('label.updated_at')</th>
                                    <th>@lang('label.status')</th>
                                    <th>@lang('label.action')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($advertisements as $advertisement)
                                    <tr role="row" class="odd center_content ">
                                        <td>
                                            @include(
                                                'partials.imageModal',
                                                [
                                                    'id' => $advertisement->id,
                                                    'title' => $advertisement->title_en,
                                                    'src' => $advertisement->image
                                                ]
                                            )
                                        </td>
                                        <td class="hidden">{{ $advertisement->title_en }}</td>
                                        <td>{{ $advertisement->page }}</td>
                                        <td>{{ dateFormat($advertisement->active_at) }}</td>
                                        <td>{{ dateFormat($advertisement->expired_at) }}</td>
                                        <td>{{ dateFormat($advertisement->created_at) }}</td>
                                        <td>{{ dateFormat($advertisement->updated_at) }}</td>
                                        <td>
                                            @if(strtotime($advertisement->expired_at) < time())
                                                <button type="button" class="btn btn-block btn-danger btn-xs">EXPIRED</button>
                                            @elseif (strtotime($advertisement->expired_at) < strtotime(\Carbon\Carbon::now()->addDays(5)))
                                                <button title="Will expired soon..." type="button" class="btn btn-block btn-warning btn-xs">ACTIVE</button>
                                            @elseif(strtotime($advertisement->active_at) <= time())
                                                <button type="button" class="btn btn-block btn-success btn-xs">ACTIVE</button>
                                            @else
                                                <button title="Will active in future" type="button" class="btn btn-block btn-default btn-xs">DRAFT</button>
                                            @endif
                                        </td>
                                        <td>
                                          @include('partials._actionId', ['entityName' => 'advertisement', 'entity' => $advertisement])
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop










