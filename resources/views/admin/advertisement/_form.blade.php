<div class="form-group hidden">
    <label for="title">Title</label>
    <input name="title_en" id="title" class="form-control" value="{{ isset($advertisement) ? $advertisement->title_en : '' }}" placeholder="Title">
</div>
<div class="form-group">
    <label for="image">Banner (275px X 325px)</label><i class="text-red">*</i>
    <input type="file" name="image" id="image" class="form-control" {{ isset($advertisement) ? '' : '' }}>
</div>
<div class="form-group">
    <label for="link">Link</label>
    <input name="link" id="link" class="form-control" value="{{ isset($advertisement) ? $advertisement->link : '' }}" placeholder="URL">
</div>
<div class="form-group">
    <label for="active_at">Active Date</label><i class="text-red">*</i>
    <input name="active_at" id="active_at" class="form-control datepicker" value="{{ isset($advertisement) ? dateFormat($advertisement->active_at) : '' }}" placeholder="Active Date" required>
</div>
<div class="form-group">
    <label for="expired_at">Expired Date</label><i class="text-red">*</i>
    <input name="expired_at" id="expired_at" class="form-control datepicker" value="{{ isset($advertisement) ? dateFormat($advertisement->expired_at) : '' }}" placeholder="Expired Date" required>
</div>
@if(!isset($_GET['service']))
<div class="form-group">
    <label for="page">Page</label><i class="text-red">*</i>
    <?php
        $pageKey = array();
        if (isset($advertisement))
            $pageKey = array(array_search($advertisement->page, array_values(\App\Models\Advertisement::PAGES)));
    ?>
    {!! Form::select('page', array_values(\App\Models\Advertisement::PAGES), isset($advertisement->page) ? $pageKey : null,  ['class' => 'form-control', 'required' => 'required']) !!}
</div>
@else
    <input type="hidden" name="service" value="{{$_GET['service']}}">
@endif



















