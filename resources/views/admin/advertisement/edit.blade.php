
@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Advertisements
            </h1>
            <ol class="breadcrumb">
                <li class=""><a href="{{url('dashboard')}}"><i class="fa fa fa-th"></i>Dashboard</a></li>
                <li class=""><a href="{{url('dashboard/advertisement')}}"><i class="fa fa fa-folder"></i>Advertisements</a></li>
                <li class="active"><i class="fa fa fa-list"></i> Edit</li>
            </ol>
        </section>

        <section class="content clearfix">
            <div class="col-xs-12">
                {!! Form::model($advertisement, ['method' => 'PUT', 'route' => ['dashboard.advertisement.update', $advertisement ],'files' => 'true']) !!}
                    @include('admin.advertisement._form')
                    @if(isset($advertisement->banner))
                        <div class="form-group">
                            <img src="{{ asset('uploads/'.$advertisement->banner) }}" alt="{{ asset('uploads/'.$advertisement->title_en) }}" class="logo-preview">
                        </div>
                    @endif
                    <div class="form-group">
                        <button class="btn btn-primary">Update</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </section>
    </div>
@stop









