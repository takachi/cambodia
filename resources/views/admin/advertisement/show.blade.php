
@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Directory Show
            </h1>
            <ol class="breadcrumb">
                <li class=""><a href="{{url('dashboard')}}"><i class="fa fa fa-th"></i>Dashboard</a></li>
                <li class=""><a href="{{url('dashboard/directories')}}"><i class="fa fa fa-folder"></i>Directories</a></li>
                <li class="active"><i class="fa fa fa-list"></i> Directory show</li>
            </ol>
        </section>

        <section class="content clearfix">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body box-profile company-profile">
                        @if($directory->latitude && $directory->longitude)
                            <div class="map" style="height: 300px; background: #dadada;"></div>
                        @endif
                        <img class="img-responsive"
                             src="{{ $directory->logo  ? asset('uploads/'.$directory->logo) : asset('assets/images/sample_logo.png') }}"
                             alt="{{ $directory->title }}">
                        <table class="table">
                            <tr>
                                <td><b>Title</b></td>
                                <td>{{ $directory->title }}</td>
                            </tr>
                            <tr>
                                <td><b>Website</b></td>
                                <td><a href="{{ $directory->website }}" target="_blank">{{ $directory->website }}</a></td>
                            </tr>
                            <tr>
                                <td><b>Phone</b></td>
                                <td><a href="tell:{{ $directory->phone }}">{{ $directory->phone }}</a></td>
                            </tr>
                            <tr>
                                <td><b>Email</b></td>
                                <td><a href="mailto:{{ $directory->email }}">{{ $directory->email }}</a></td>
                            </tr>
                            <tr>
                                <td><b>Address</b></td>
                                <td>{{ $directory->address }}</td>
                            </tr>
                            <tr>
                                <td><b>Latitude</b></td>
                                <td>{{ $directory->latitude }}</td>
                            </tr>
                            <tr>
                                <td><b>Longitude</b></td>
                                <td>{{ $directory->longitude }}</td>
                            </tr>
                            <tr>
                                <td><b>Pin</b></td>
                                <td>{{ $directory->pin ? 'Yes' : 'No' }}</td>
                            </tr>
                        </table>
                        <p><strong>About</strong></p>
                        <div class="">
                            {!! $directory->about !!}
                        </div>
                        <p><strong>Service</strong></p>
                        <div class="">
                            {!! $directory->service !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop










