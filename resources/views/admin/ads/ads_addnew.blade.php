@extends('layouts.admin') @section('content')
    <div class="content-wrapper" style="min-height: 1143px;">
        <section class="content-header">
            <h1>
                Add New
            </h1>
            {{--<ol class="breadcrumb">--}}
            {{--<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
            {{--<li><a href="#">Examples</a></li>--}}
            {{--<li class="active">Related Ministry</li>--}}
            {{--</ol>--}}
        </section>
        <section class="content">
            <div class="row">

                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        {{--<li class="active"><a href="#activity" data-toggle="tab">Activity</a></li>--}}
                        <li class="active"><a href="#english" data-toggle="tab">English</a></li>
                        <li class=""><a href="#khmer" data-toggle="tab">ខ្មែរ</a></li>
                    </ul>
                    <div class="tab-content">

                        <!-- /.tab-pane -->
                        <div class="active tab-pane" id="english">
                            <form class="form-horizontal">
                                {{--Title--}}
                                <div class="form-group">
                                    <label for="inputTitleEn" class="col-sm-2 control-label">Title</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" id="inputTitleEn" placeholder="title..." type="text">
                                    </div>
                                </div>
                                {{--uplord logo--}}
                                <div class="form-group">
                                    <label for="inputLogoEn" class="col-sm-2 control-label">Upload Logo</label>
                                    <input id="inputLogoEn" type="file" class="col-sm-10">
                                    <p class="help-block col-sm-10 " >Example abc.JPG / PNG</p>
                                </div>
                                {{--url--}}
                                <div class="form-group">
                                    <label for="inputUrlEn" class="col-sm-2 control-label">Url</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" id="inputUrlEn" placeholder="url..." type="url">
                                    </div>
                                </div>

                                {{--ADS-list--}}
                                <div class="form-group ">
                                    <label class="col-sm-2 control-label">Advertisement</label>
                                    <div class="col-sm-10">
                                        <select class="selectpicker">
                                            <optgroup label="REGULATION">
                                                <option value="">SME Policy & Frameworks</option>
                                                <option value="">SME Licence Mapping</option>
                                                <option value="">Related Ministry</option>
                                            </optgroup>
                                            <optgroup label="SERVICE">
                                                <option value="">business development</option>
                                                <option value="">Accounting & Finance</option>
                                                <option value="">Human Resource</option>
                                                <option value="">Legal</option>
                                                <option value="">Sale & Marketing</option>
                                                <option value="">Operation</option>
                                                <option value="">Technology</option>
                                            </optgroup>
                                            <optgroup label="DICRECTORY">
                                                <option value="">test1</option>
                                                <option value="">test2</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>

                                {{--date expired--}}
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Expire-date</label>
                                    <div class="col-sm-10">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input class="form-control pull-right" id="datepickerEn" type="date">
                                        </div>
                                    </div>
                                </div>


                                {{--submit--}}
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-danger">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="khmer">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label for="inputTitleKh" class="col-sm-2 control-label">ចំណងជើង</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" id="inputTitleKh" placeholder="ចំណងជើង..." type="text">
                                    </div>
                                </div>
                                {{--uplord logo--}}
                                <div class="form-group">
                                    <label for="inputLogoKh" class="col-sm-2 control-label">អាប់ឡូតរូប</label>
                                    <input id="inputLogokh" type="file" class="col-sm-10">
                                    <p class="help-block col-sm-10 " >ឧទាហរណ៍ abc.JPG / PNG</p>
                                </div>
                                {{--url--}}
                                <div class="form-group">
                                    <label for="inputUrlKh" class="col-sm-2 control-label">អាសយដ្ឋាននៃគេហទំព័រ</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" id="inputUrlKh" placeholder="អាសយដ្ឋាន..." type="url">
                                    </div>
                                </div>

                                {{--ADS-list--}}
                                <div class="form-group ">
                                    <label class="col-sm-2 control-label">ផ្សព្វផ្សាយពាណិជ្ជកម្ម</label>
                                    <div class="col-sm-10">
                                        <select class="selectpicker">
                                            <optgroup label="ផ្នែកបទបញ្ញាត្តិ">
                                                <option value="">គោលនយោបាយ និងផែនការយុទ្ធសាស្រ្ត របស់សហគ្រាសធុនតូច និងមធ្យម</option>
                                                <option value="">លក្ខខ័ណ្ឌ និងនីតិវិធីសំរាប់ការចុះបញ្ជីអាជីវកម្ម​របស់សហគ្រាសធុនតូចនិងមធ្យម</option>
                                                <option value="">ក្រសួងពាក់ព័ន្ធ</option>
                                            </optgroup>
                                            <optgroup label="ផ្នែកសេវាកម្ម">
                                                <option value="">ការពង្រីកអាជីវកម្ម</option>
                                                <option value="">គណនេយ្យ និងហិរញ្ញវត្ថុ</option>
                                                <option value="">ធនធានមនុស្ស</option>
                                                <option value="">ច្បាប់</option>
                                                <option value="">ការលក់ និងផ្សព្វផ្សាយ</option>
                                                <option value="">ប្រតិបត្តិការ</option>
                                                <option value="">បច្ចេកវិទ្យា</option>
                                            </optgroup>
                                            <optgroup label="ព័ត៌មានម្ចាស់អាជីវកម្ម">
                                                <option value="">GGear Co.,Ltd.</option>
                                                <option value="">Biz Solution Co., Ltd</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>

                                {{--date expired--}}
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ថ្ងៃផុតកំណត់</label>
                                    <div class="col-sm-10">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input class="form-control pull-right" id="datepickerKh" type="date">
                                        </div>
                                    </div>
                                </div>
                                {{--submit kh--}}
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-danger">បញ្ជូន</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- /.nav-tabs-custom -->

            </div>
        </section>
    </div>

@stop





