
@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Advertisement</h3>
                            <a href="/IBC_project/public/ads_add" class="button"><button>Add New</button></a>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="ads" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                            <thead>
                                            <tr role="row">
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 230px;" aria-label="Rendering engine: activate to sort column ascending">Title</th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 181px;" aria-label="Browser: activate to sort column ascending">On Page</th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 250px;" aria-label="Platform(s): activate to sort column ascending">Date</th>
                                                <th class="sorting_desc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 198px;" aria-label="Engine version: activate to sort column ascending" aria-sort="descending">Expires-Date</th>
                                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 80px;" aria-label="CSS grade: activate to sort column ascending">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            <tr role="row" class="odd">
                                                <td class="">Webkit</td>
                                                <td>Safari 3.0</td>
                                                <td>OSX.4+</td>
                                                <td class="sorting_1">522.1</td>
                                                <td>
                                                    <a href="#" class="btn btn-sm btn-success">
                                                        <span class="glyphicon glyphicon-plus"></span> Add
                                                    </a>
                                                    <a href="#" class="btn btn-sm btn-danger">
                                                        <span class="glyphicon glyphicon-plus"></span> Edit
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr role="row" class="even">
                                                <td class="">Webkit</td>
                                                <td>iPod Touch / iPhone</td>
                                                <td>iPod</td>
                                                <td class="sorting_1">420.1</td>
                                                <td>
                                                    <a href="#" class="btn btn-sm btn-success">
                                                        <span class="glyphicon glyphicon-plus"></span> Add
                                                    </a>
                                                    <a href="#" class="btn btn-sm btn-danger">
                                                        <span class="glyphicon glyphicon-plus"></span> Edit
                                                    </a>
                                                </td>

                                            </tr>
                                            <tr role="row" class="odd">
                                                <td class="">Webkit</td>
                                                <td>OmniWeb 5.5</td>
                                                <td>OSX.4+</td>
                                                <td class="sorting_1">420</td>
                                                <td>
                                                    <a href="#" class="btn btn-sm btn-success">
                                                        <span class="glyphicon glyphicon-plus"></span> Add
                                                    </a>
                                                    <a href="#" class="btn btn-sm btn-danger">
                                                        <span class="glyphicon glyphicon-plus"></span> Edit
                                                    </a>
                                                </td>

                                            </tr>
                                            <tr role="row" class="even">
                                                <td class="">Webkit</td>
                                                <td>Safari 2.0</td>
                                                <td>OSX.4+</td>
                                                <td class="sorting_1">419.3</td>
                                                <td>
                                                    <a href="#" class="btn btn-sm btn-success">
                                                        <span class="glyphicon glyphicon-plus"></span> Add
                                                    </a>
                                                    <a href="#" class="btn btn-sm btn-danger">
                                                        <span class="glyphicon glyphicon-plus"></span> Edit
                                                    </a>
                                                </td>
                                            </tr>

                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th rowspan="1" colspan="1">Title</th>
                                                <th rowspan="1" colspan="1">On page</th>
                                                <th rowspan="1" colspan="1">Date</th>
                                                <th rowspan="1" colspan="1">Expires-Date</th>
                                                <th rowspan="1" colspan="1">Action</th>

                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>
@stop

