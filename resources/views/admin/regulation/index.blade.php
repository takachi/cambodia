@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Regulation Lading
            </h1>
            <ol class="breadcrumb">
                <li class=""><a href="{{url('dashboard')}}"><i class="fa fa fa-th"></i>Dashboard</a></li>
                <li class="active"><i class="fa fa fa-folder"></i> Regulation Lading</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#regulation_category" data-toggle="tab">Regulation Category</a></li>
                        <li><a href="#hero_banner" data-toggle="tab">Hero Banner</a></li>
                        <li><a href="#search_box" data-toggle="tab">Search box</a></li>
                        <li><a href="#page_title" data-toggle="tab">Page Title</a></li>
                    </ul>
                        <div class="tab-content">
                            @include('partials.flushMessage')
                            <div class="active tab-pane clearfix" id="regulation_category">
                                @include('admin.regulation._sectionRegulationCategory')
                            </div>
                            <div class="tab-pane clearfix" id="hero_banner">
                                @include('admin.contentElement._heroBanner', [
                                'contentElementId' => isset($ceHeroBanner) ? $ceHeroBanner->id : null,
                                'contentElementName' => \App\Models\ContentElement::REGULATION
                                ])
                            </div>
                            <div class="tab-pane clearfix" id="search_box">
                                @include('admin.contentElement._searchBox', [
                                'contentElementId' =>  isset($ceSearchBox) ? $ceSearchBox->id : null,
                                'contentElementName' => \App\Models\ContentElement::REGULATION
                                ])
                            </div>
                            <div class="tab-pane clearfix" id="page_title">
                                @include('admin.contentElement._pageTitle', [
                                'contentElementId' =>  isset($cePageTitle) ? $cePageTitle->id : null,
                                'contentElementName' => \App\Models\ContentElement::REGULATION
                                ])
                            </div>
                        </div>
                </div>
            </div>
        </section>
    </div>
    <style>
        .hiddenClass{
            display: none;
        }
    </style>

@stop


