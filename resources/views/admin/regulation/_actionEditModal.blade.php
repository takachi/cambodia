<div class="modal fade" id="edit-{{ $item->id }}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>
                <h4 class="modal-title">{{ $regulationCategory->title_en }}</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="{{ action('admin\ContentElementController@regulationCategoryUpdate', ['id' => $item->id]) }} " method="POST" enctype="multipart/form-data">
                    <label for="title">Title</label> <i class="text-red">*</i>
                    <input name="content[regulation_category][title_en]" value="{{ isset($regulationCategory->title_en) ? $regulationCategory->title_en : '' }}" id="title" class="form-control" placeholder="Title" required>

                    {{--<label for="title">Link</label>--}}
                    {{--<input name="content[regulation_category][link]" id="link" value="{{ isset($regulationCategory->link) ? $regulationCategory->link : '' }}" class="form-control" placeholder="URL">--}}

                    {{--<label for="link">Template type</label>--}}
                    {{--{!! Form::select('content[regulation_category][link]', $regulationCategory, isset($directory) ? $directory->services->pluck('id')->toArray() : null,  ['class' => 'form-control height-200', 'multiple']) !!}--}}


                    <label for="image_background">Image Background</label>
                    <input type="file" name="content[regulation_category][image_background]" id="imagePopup" class="form-control">

                    <label for="title">Template Type</label>
                    <select name="content[regulation_category][link]" id="link" class="form-control">
                        <option value="regulation/cambodia-industry-development-policy">Policy and Framework</option>
                        <option value="regulation/sme-license-mapping">License Mapping</option>
                        <option value="regulation/related-ministry">Related Ministry</option>
                    </select>

                    <button title="Update" class="btn btn-primary margin-tb-15">Update</button>
                </form>
            </div>
        </div>
    </div>
</div>





