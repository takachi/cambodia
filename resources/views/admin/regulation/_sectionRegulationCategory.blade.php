
{{--input form--}}
<form class="form-horizontal" action="{{ action('admin\ContentElementController@regulationCategoryStore') }} " method="POST" enctype="multipart/form-data">

    <label for="title">Title</label> <i class="text-red">*</i>
    <input name="content[regulation_category][title_en]" id="title" class="form-control" placeholder="Title" required>

    <label for="image_background">Image Background (360px X 220px)</label>
    <input type="file" name="content[regulation_category][image_background]" id="image" class="form-control">

    <label for="title">Template Type</label>
    <select name="content[regulation_category][link]" id="link" class="form-control">
        <option value="/regulation/cambodia-industry-development-policy">Policy and Framework</option>
        <option value="/regulation/sme-license-mapping">License Mapping</option>
        <option value="/regulation/related-ministry">Related Ministry</option>
    </select>

@if(count($ceRegulationCategories) >= 3)
        <?php $toolTipDescription = 'Cannot create it more than 3, if you still need create the new one please remove any of them below!' ?>

        <button class="btn btn-primary margin-tb-15" disabled data-toggle="tooltip" data-placement="right" title="{{ $toolTipDescription }}">
            <span class="glyphicon glyphicon-plus"></span> Add New
        </button>

    @else
        <button class="btn btn-primary margin-tb-15">
            <span class="glyphicon glyphicon-plus"></span> Add New
        </button>
    @endIf

</form><br>

<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th class="center_content">@lang('label.imageBackground')</th>
        <th class="center_content">@lang('label.title')</th>
{{--        <th class="center_content">@lang('label.link')</th>--}}
        <th class="center_content">@lang('label.created')</th>
        <th class="center_content">@lang('label.updated')</th>
        <th class="center_content">@lang('label.action')</th>
    </tr>
    </thead>
    <tbody>
    @foreach($ceRegulationCategories as $item)
        <?php  $regulationCategory = json_decode($item->content)->regulation_category;?>
        <tr>
            <td>
                @include(
                    'partials.imageModal',
                     [
                        'id' => $item->id,
                        'title' => $regulationCategory->title_en,
                        'src' => $regulationCategory->image_background
                     ]
                )
            </td>
            <td>{{ $regulationCategory->title_en }}</td>
            {{--<td>{{ $regulationCategory->link }}</td>--}}
            <td>{{ $item->created_at }}</td>
            <td>{{ $item->updated_at }}</td>
            <td>
                <a title="Edit" href="#" class="btn btn-xs btn-success" data-toggle="modal" data-target="#edit-{{$item->id}}">
                    <i class="glyphicon glyphicon-pencil"></i>
                </a>
                @include('admin.regulation._actionEditModal')
                <form action="{{ action('admin\ContentElementController@regulationCategoryDestroy', ['id' => $item->id]) }} "
                      method="POST" enctype="multipart/form-data"
                      style="display: inline-block"
                      onsubmit="return confirm('Are you sure you want to delete?')">

                    <button title="Delete" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i></button>
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>



