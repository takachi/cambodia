
<div class="form-group">
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#en" data-toggle="tab" aria-expanded="true">EN</a></li>
        {{--<li class=""><a href="#kh" data-toggle="tab" aria-expanded="false">KH</a></li>--}}
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="en">

            <label for="shop_en">Shop Name</label><i class="text-red">*</i>
                <input name="shop_en" id="shop_en" class="form-control" value="{{ isset($items) ? $items->shop_name : '' }}" placeholder="Shop Name" required>

            <label for="des_en">Description</label>
                <input name="des_en" id="des_en" class="form-control" value="{{ isset($items) ? $items->shop_desen : '' }}" placeholder="Description" required>

            <label for="phone">Phone</label>
                <input name="phone" id="phone" class="form-control" value="{{ isset($items) ? $items->phone : '' }}" placeholder="Phone" required>

            <label for="address">Address</label>
                <input name="address" id="address" class="form-control" value="{{ isset($items) ? $items->address : '' }}" placeholder="Address" required>

            <label for="facebook">Facebook</label>
                <input name="facebook" id="facebook" class="form-control" value="{{ isset($items) ? $items->facebook : '' }}" placeholder="Facebook" required>

            <label for="google">Google</label>
            <input name="google" id="google" class="form-control" value="{{ isset($items) ? $items->google : '' }}" placeholder="Google" required>

            <label for="map">Map</label>
            <input name="map" id="map" class="form-control" value="{{ isset($items) ? $items->map : '' }}" placeholder="Map" required>
        </div>
        {{--<div class="tab-pane" id="kh">--}}

                {{--<label for="title_kh">Content Title</label><i class="text-red">*</i>--}}
                {{--<input name="title_kh" id="title_kh" class="form-control" value="{{ isset($items) ? $items->title_km : '' }}" placeholder="Title" required>--}}

                {{--<label for="desc_kh">Description</label>--}}
                {{--<input name="desc_kh" id="desc_kh" class="form-control" value="{{ isset($items) ? $items->description_km : '' }}" placeholder="Description" required>--}}

        {{--</div>--}}
    </div>
</div>


<label for="thumb">Thumbnail (275x X 295px)</label>
@include(
    'partials.fileUpload',
    [
        'id' => 'images',
        'name' => 'images',
        'url' => isset($items->images) ? $items->images : ''
    ]
)

</div>
