
@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Shop
            </h1>
            <ol class="breadcrumb">
                <li class=""><a href="{{url('dashboard')}}"><i class="fa fa fa-th"></i>Dashboard</a></li>
                <li class="active"><i class="fa fa fa-folder"></i> Shop</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#upcoming" data-toggle="tab">Shop</a></li>
                        {{--<li><a href="#hero_banner" data-toggle="tab">Hero Banner</a></li>--}}
                        {{--<li><a href="#page_title" data-toggle="tab">Page Title</a></li>--}}
                    </ul>
                    <div class="tab-content">
                        @include('partials.flushMessage')
                        <div class="active tab-pane clearfix" id="upcoming">
                            <div class="box">
                                <div class="box-header">
                                    <a href="{{url('dashboard/shop/create')}}" class="btn btn-sm btn-primary">
                                        <span class="glyphicon glyphicon-plus"></span> Add New
                                    </a>
                                </div>
                                <div class="box box-primary" >
                                    <div class="box-body">
                                        <table class="default-data-table table table-bordered table-striped" >
                                            <thead>
                                            <tr>
                                                <th class="center_content">@lang('Thumbnail')</th>
                                                <th class="center_content">Shop Name</th>
                                                <th class="center_content">Description</th>
                                                <th class="center_content">Phone</th>
                                                <th class="center_content">@lang('Create Date')</th>
                                                <th class="center_content">@lang('Action')</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($item as $items)
                                                <tr role="row" class="odd center_content ">
                                                    <td>
                                                        <img class="media-object"
                                                             src="{{ $items->images  ? asset($items->images) : asset('assets/images/sample_logo.png') }}"
                                                             alt="User Avatar"
                                                             rel="popover"
                                                             style="height: 25px;"
                                                             data-img="{{ $items->images  ? asset($items->images) : asset('assets/images/sample_logo.png') }}"
                                                             title="{{ $items->title }}">
                                                    </td>
                                                    <td>{{ ($items->shop_name)}}</td>
                                                    <td>{{ ($items->shop_desen)}}</td>
                                                    <td>{{ $items->phone}}</td>
                                                    <td>{{ dateFormat($items->date_time) }}</td>
                                                    <td>
                                                        @include('partials._action', ['entityName' => 'shop', 'entity' => $items])
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--<div class="tab-pane clearfix" id="hero_banner">--}}
                            {{--@include('admin.contentElement._heroBanner', [--}}
                            {{--'contentElementId' =>  isset($ceHeroBanner) ? $ceHeroBanner->id : null,--}}
                            {{--'contentElementName' => \App\Models\ContentElement::UPCOMING_NEWS_EVENTS--}}
                            {{--])--}}
                        {{--</div>--}}
                        {{--<div class="tab-pane clearfix" id="page_title">--}}
                            {{--@include('admin.contentElement._pageTitle', [--}}
                            {{--'contentElementId' =>  isset($cePageTitle) ? $cePageTitle->id : null,--}}
                            {{--'contentElementName' => \App\Models\ContentElement::UPCOMING_NEWS_EVENTS--}}
                            {{--])--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop







