@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                FAQs Item
            </h1>
            <ol class="breadcrumb">
                <li class=""><a href="{{url('dashboard')}}"><i class="fa fa fa-th"></i>Dashboard</a></li>
                <li class=""><a href="{{url('dashboard/sme-licence-mapping')}}"><i class="fa fa fa-folder"></i>SME Licence Mapping</a></li>
                <li class="active"><i class="fa fa fa-folder"></i> FAQs List</li>
            </ol>
        </section>

        <section class="content clearfix">
            {!! Form::model($faqsItem, ['method' => 'PUT', 'route' => ['dashboard.faqs-item.update', $faqsItem->id ]]) !!}
                @include('admin.faqsItem._form', ['licenseMapping' => $faqsItem->licenseMapping])
            {!! Form::close() !!}
        </section>
    </div>
@stop

