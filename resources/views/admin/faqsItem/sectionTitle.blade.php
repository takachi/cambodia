
<div class="box box-primary collapsed-box">
    <div class="box-header with-border">
        <h3 class="box-title">Title Section</h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <div class="nav-tabs-custom">
            <form class="form-horizontal"
                  action="{{ action('admin\FaqsItemController@licenseMappingUpdate', ['id' => $licenseMapping->id]) }} "
                  method="POST">
            <ul class="nav nav-tabs">
                    <li class="active"><a href="#faqs_en" data-toggle="tab">EN</a></li>
                    <li><a href="#faqs_kh" data-toggle="tab">KH</a></li>
                </ul>
                <div class="tab-content">
                    <div class="active tab-pane clearfix" id="faqs_en">
                        <label for="faqs_title_en">Title</label>
                        <input name="faqs_title_en"
                               class="form-control"
                               value="{{ isset($licenseMapping) ? $licenseMapping->faqs_title_en : '' }}"
                               placeholder="Title"><br>

                        <label for="faqs_description_en">Description</label>
                        <textarea name="faqs_description_en" id="faqs_description_en" rows="2" class="form-control">
                        {{ isset($licenseMapping) ? $licenseMapping->faqs_description_en : '' }}
                    </textarea>

                    </div>
                    <div class="tab-pane clearfix" id="faqs_kh">
                        <label for="faqs_title_kh">Title</label>
                        <input name="faqs_title_kh"
                               class="form-control"
                               value="{{ isset($licenseMapping) ? $licenseMapping->faqs_title_en : '' }}"
                               placeholder="Title"><br>

                        <label for="faqs_description_kh">Description</label>
                        <textarea name="faqs_description_kh" id="faqs_description_kh" rows="2" class="form-control">
                            {{ isset($licenseMapping) ? $licenseMapping->faqs_description_kh : '' }}
                        </textarea>
                    </div>
                </div>
                <button class="btn btn-primary margin-tb-15">
                    <span class="glyphicon glyphicon-plus"></span> Submit
                </button>
            </form>
        </div>
    </div>
</div>

