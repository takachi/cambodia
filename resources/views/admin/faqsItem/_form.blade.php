<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#faqs_item_en" data-toggle="tab">EN</a></li>
        <li><a href="#faqs_item_kh" data-toggle="tab">KH</a></li>
    </ul>
    <div class="tab-content">
        <div class="active tab-pane clearfix" id="faqs_item_en">
            <label for="">Question</label> <i class="text-red">*</i>
            <input name="question_en" value="{{ isset($faqsItem) ? $faqsItem->question_en : ''  }}" class="dynamic form-control" placeholder="Question" required><br>

            <label>Answer</label> <i class="text-red">*</i>
            @include('partials.tinymce', ['name' => 'answer_en', 'entity' => isset($faqsItem) ? $faqsItem : '', 'required'])
        </div>
        <div class="tab-pane clearfix" id="faqs_item_kh">
            <label for="">Question</label>
            <input name="question_kh" value="{{ isset($faqsItem) ? $faqsItem->question_kh : ''  }}" class="dynamic form-control" placeholder="Question"><br>

            <label>Answer</label>
            @include('partials.tinymce', ['name' => 'answer_kh', 'entity' => isset($faqsItem) ? $faqsItem : ''])
        </div>
    </div>
    <input type="hidden" name="license_mapping_id" value="{{ $licenseMapping->id }}">
    <button class="btn btn-primary margin-tb-15" style="margin-left: 10px;">
        {{ isset($faqsItem) ? 'Update' : 'Create new'  }}
    </button>
</div>
