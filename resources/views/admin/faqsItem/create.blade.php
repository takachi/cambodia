{{--<a title="Edit" href="#" class="btn btn-primary margin-tb-15" data-toggle="modal" data-target="#create_faqs">--}}
    {{--Create--}}
{{--</a>--}}
{{--<div class="modal fade" id="create_faqs">--}}
    {{--<div class="modal-dialog modal-lg">--}}
        {{--<div class="modal-content">--}}
            {{--<div class="modal-header">--}}
                {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>--}}
                {{--<h4 class="modal-title">Create new</h4>--}}
            {{--</div>--}}
            {{--<div class="modal-body">--}}
                {{--<form class="form-horizontal"--}}
                      {{--action="{{ action('admin\FaqsItemController@store') }}"--}}
                      {{--method="POST"--}}
                      {{--enctype="multipart/form-data">--}}
                    {{--@include('admin.faqsItem._form')--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                FAQs Item
            </h1>
            <ol class="breadcrumb">
                <li class=""><a href="{{url('dashboard')}}"><i class="fa fa fa-th"></i>Dashboard</a></li>
                <li class=""><a href="{{url('dashboard/sme-licence-mapping')}}"><i class="fa fa fa-folder"></i>SME Licence Mapping</a></li>
                <li class="active"><i class="fa fa fa-folder"></i> FAQs List</li>
                {{--<li class=""><a href="{{url('dashboard/directories')}}"><i class="fa fa fa-folder"></i>Directories</a></li>--}}
                {{--<li class="active"><i class="fa fa fa-list"></i> Directory new</li>--}}
            </ol>
        </section>

        <section class="content clearfix">
            <form class="form-horizontal"
                  action="{{ action('admin\FaqsItemController@store') }}"
                  method="POST"
                  enctype="multipart/form-data">

                @include('admin.faqsItem._form', ['licenseMapping' => $licenseMapping])
            </form>
        </section>
    </div>
@stop

