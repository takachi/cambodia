
@extends('layouts.admin')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                FAQs List | {{ isset($licenseMapping) ? lang($licenseMapping, 'title') : '' }}
            </h1>
            <ol class="breadcrumb">
                <li class=""><a href="{{url('dashboard')}}"><i class="fa fa fa-th"></i>Dashboard</a></li>
                <li class=""><a href="{{url('dashboard/sme-licence-mapping')}}"><i class="fa fa fa-folder"></i>SME Licence Mapping</a></li>
                <li class="active"><i class="fa fa fa-folder"></i> FAQs List</li>
            </ol>
        </section>

        <section class="content">
            @include('partials.flushMessage')
            @include('admin.faqsItem.sectionTitle')
            <a href="{{ action('admin\FaqsItemController@create', $licenseMapping->id)}}" class="btn btn-sm btn-primary">
                <span class="glyphicon glyphicon-plus"></span> Add New
            </a>

            <div class="nav-tabs-custom margin-tb-15">
                <table class="table table-bordered table-striped mar">
                    <thead>
                        <tr>
                            <th class="center_content">@lang('label.question')</th>
                            <th class="center_content">@lang('label.answer')</th>
{{--                            <th class="center_content">@lang('label.created_at')</th>--}}
{{--                            <th class="center_content">@lang('label.updated_at')</th>--}}
                            <th class="center_content">@lang('label.action')</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach( $faqsItems as $faqsItem)
                            <tr>
                                <td> {{ lang($faqsItem, 'question') }}</td>
                                <td> {!! lang($faqsItem, 'answer') !!}</td>
{{--                                <td> {{ $faqsItem->created_at }}</td>--}}
{{--                                <td> {{ $faqsItem->updated_at }}</td>--}}
                                    <td>
                                        @include('partials._action', ['entity' => $faqsItem, 'entityName' => 'faqs-item'])
                                    </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </section>
    </div>
@stop








