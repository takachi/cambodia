@extends('layouts.admin')

@section('title', 'Setting')

@section('content')
	<div class="content-wrapper">
		<section class="content-header">
			<h1>
				Settings
			</h1>
			<ol class="breadcrumb">
				<li><a href="/admin"><i class="fa fa-dashboard"></i> Admin</a></li>
				<li class="active">Settings</li>
			</ol>
		</section>
		<section class="content">
			@include('partials.flushMessage')
			<div class="row">
				<div class="col-sm-6">
					<div class="box box-primary">
						<div class="box-header with-border">
							<h3 class="box-title">Top Header Label</h3>

							<div class="box-tools pull-right">
								<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
							</div>
						</div>
						<div class="box-body">
							<form action="{{ '' }}" method="POST">
								{{ csrf_field() }}
								<div class="nav-tabs-custom">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#top_header_label_en" data-toggle="tab">EN</a></li>
										<li><a href="#top_header_label_kh" data-toggle="tab">KH</a></li>
									</ul>
									<div class="tab-content">
										<div class="active tab-pane clearfix" id="top_header_label_en">
											<div class="input-group input-group-sm">
												<input required
													   name=""
													   value="{{ isset($topHeaderLabel) ? $topHeaderLabel->about_en : '' }}"
													   placeholder="About Us"
													   class="form-control">

												<span class="input-group-btn">
													<button class="btn btn-info btn-flat">Save</button>
												</span>
											</div>
											<br>
											<div class="input-group input-group-sm">
												<input required
													   name=""
													   value="{{ isset($topHeaderLabel) ? $topHeaderLabel->contact_en : '' }}"
													   placeholder="About Us"
													   class="form-control">

												<span class="input-group-btn">
													<button class="btn btn-info btn-flat">Save</button>
												</span>
											</div>
										</div>
										<div class="tab-pane clearfix" id="top_header_label_kh">
											<div class="input-group input-group-sm">
												<input required
													   name=""
													   value="{{ isset($topHeaderLabel) ? $topHeaderLabel->about_kh : '' }}"
													   placeholder="អំពី​​យើង"
													   class="form-control">

												<span class="input-group-btn">
													<button class="btn btn-info btn-flat">Save</button>
												</span>
											</div>
											<br>
											<div class="input-group input-group-sm">
												<input required
													   name=""
													   value="{{ isset($topHeaderLabel) ? $topHeaderLabel->contact_kh : '' }}"
													   placeholder="About Us"
													   class="form-control">

												<span class="input-group-btn">
													<button class="btn btn-info btn-flat">Save</button>
												</span>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
@endsection
