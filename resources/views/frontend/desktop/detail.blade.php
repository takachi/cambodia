<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="IE=9" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Tripadvisor</title>
    <link href="{{ asset('assets/scripts/bootstrap/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/css/desk-style.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/css/desk-style-home.css')}}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Content:400,700" rel="stylesheet">


    <link href="{{ asset("css/jquery.fancybox.css") }}" rel="stylesheet"​​ type="text/css"/>
    <link href="{{ asset("css/slick.css") }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset("css/slick-theme.css") }}" rel="stylesheet"/>
    <script type="text/javascript" src="{{ asset("http://code.jquery.com/jquery-1.8.2.js")}}"></script>
    <script type="text/javascript" src="{{ asset("js/jquery.fancybox.js")}}"></script>
    <script type="text/javascript" src="{{ asset("js/jquery.elevatezoom.js")}}"></script>

    <style type="text/css">

   .fancybox-skin{ margin-top: 5% !important;}

    #gallery_01 img{border:2px solid white;width: 96px;}
    .active img{border:2px solid #333 !important;}
    .img-detail{
        width: 310px;
        float: left;
    }
    .slick-initialized .slick-slide {
        margin-top: 5px;
    }
    #fancybox-loading, .fancybox-close,
    .fancybox-prev span,
    .fancybox-next span {
        background-image: url("fancybox_sprite.png");
    }
    </style>
</head>

{{--@section('content')--}}

<body>
<div class="wrapper">
    <div class="header">
        <div class="container">
            <div class="pull-left">

                <div class="logo">
                    <a href="{{url('/')}}"><img src="assets/images/logo.svg"></a>
                </div>
                <div class="menu">
                    <nav id="menu-main">
                        <ul>
                            <li><a href="{{url('/hotel')}}">Hotel</a></li>
                            <li><a href="{{url('/restaurant')}}">Restaurant</a></li>
                            <li><a href="{{url('/shop')}}">Shop</a></li>
                            <li><a href="#">Car Rental</a></li>
                            <li><a href="#">Coupon</a></li>
                        </ul>
                    </nav>
                </div>
                <!--menu on mobile-->
                <nav id="menu">
                    <ul>
                        <li><a href="{{url('/hotel')}}">Hotel</a></li>
                        <li><a href="{{url('/restaurant')}}">Restaurant</a></li>
                        <li><a href="{{url('/shop')}}">Shop</a></li>
                        <li><a href="#">Car Rental</a></li>
                        <li><a href="#">Coupon</a></li>
                    </ul>
                </nav>
                <div class="humberger">
                    <a href="#menu">
                        <img src="assets/images/hamburger.svg"/>
                    </a>
                </div>

            </div>
        </div>

    </div><!--.header-->
</div>

<div class="wrapper" style="padding-top: 7%;">
    <div class="content">
        <div class="pvd-vdo section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6" >
                            <div>
                                <div class="img-detail">
                                    <img id="img1" src="{{ asset('images/small/image1.png')}}" data-zoom-image="{{ asset('images/large/image1.jpg')}}" style="width: 310px; height: 315px;" />
                                    <div id="gallery_01">
                                        <a  href="#" class="active" data-image="{{ asset('images/small/image1.png')}}"
                                            data-zoom-image="{{ asset('images/large/image1.jpg')}}" >
                                            <img src="{{ asset('images/small/image1.png')}}" />
                                        </a>
                                        <a href="#" data-image="{{ asset('images/small/image3.png')}}" data-zoom-image="{{ asset('images/large/image3.jpg')}}" >
                                            <img src="{{ asset('images/small/image3.png')}}" />
                                        </a>
                                        <a  href="#" data-image="{{ asset('images/small/image2.png')}}" data-zoom-image="{{ asset('images/large/image2.jpg')}}">
                                            <img src="{{ asset('images/small/image2.png')}}" />
                                        </a>
                                        <a href="#" data-image="{{ asset('images/small/image3.png')}}" data-zoom-image="{{ asset('images/large/image3.jpg')}}" >
                                            <img src="{{ asset('images/small/image3.png')}}" />
                                        </a>
                                        <a href="#" data-image="{{ asset('images/small/image4.png')}}" data-zoom-image="{{ asset('images/large/image4.jpg')}}">
                                            <img src="{{ asset('images/small/image4.png')}}" />
                                        </a>
                                        <a href="#" data-image="{{ asset('images/small/image3.png')}}" data-zoom-image="{{ asset('images/large/image3.jpg')}}" >
                                            <img src="{{ asset('images/small/image3.png')}}" />
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h3 class='m_3 p_title' style="background:#eee; padding:20px;">InterContinental</h3>
                            <p class='m_5' style="color:red;">Discount : 0%</p><br>
                            {{--<p class='m_5' style="color:red;">Close Date : 2017-08-18</p><br>--}}
                            <p class='m_3'>Contact for more information</p>
                            <p class='m_5'>Address : #296 Mao Tse Toung Blvd
                                Phnom Penh
                            </p>
                            <p class='m_5'>Facebook : <a href="https://www.facebook.com/InterContinentalPhnomPenh/"> Click detail... </a></p></p>
                            <p class='m_5'>Tel Contact : 023 424 888</p>
                            <p class='m_5'>Description : Luxury guest rooms, coffee shop, cantonese restaurant, all day dining, spa, fitness centre, deli shop, wedding, conference, banquet, bar.</p>
                            <!--  <p>
                                 <button type="submit" class="black" name="basket">
                                     Add to basket                        </button>
                             </p> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="wrapper">
    <div class="footer maincolor">
        <div class="wrapper">
            <div class="footer">

                <div class="footer-top">
                    <div class="container">
                        <div class="top">
                            <div class="row">

                                <div class="col-sm-6 col-xs-12">
                                    <h1>Contact Us</h1>
                                    <ul>
                                        <li><span class="address"></span>No. 168, National Road No 05, Sangkat Prek Phnov, Khan Sen Sok, Phnom Penh, Cambodia</li>
                                        <li><span class="phone"></span><a href="">(+855) 12 792 707 / 10 571 889</a></li>
                                        <li><span class="emial"></span><a href="">cambodiaintermarket @gmail.com</a></li>
                                    </ul>
                                    <h2>Social Network</h2>
                                    <div class="social">
                                        <ul>
                                            <li>
                                                <a href="#">
                                                    <img src="{{ asset("assets/images/fb.png")}}"/>
                                                    <img src="{{ asset("assets/images/fb-.png")}}"/>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <img src="{{ asset("assets/images/linkedin.png")}}"/>
                                                    <img src="{{ asset("assets/images/linkedin-.png")}}"/>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <img src="{{ asset("assets/images/in.png")}}"/>
                                                    <img src="{{ asset("assets/images/in-.png")}}"/>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <img src="{{ asset("assets/images/g.png")}}"/>
                                                    <img src="{{ asset("assets/images/g-.png")}}"/>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12 description">
                                    <div class="logo-bottom">
                                        <img src="{{ asset("assets/images/logo-bottom.png")}}"/>
                                    </div>
                                    <p>* TripAdvisor LLC is not a booking agent and does not charge any service fees to users of our site... (more)
                                        TripAdvisor LLC is not responsible for content on external web sites. Taxes, fees not included for deals content.</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>


                <div class="footer-bottom">
                    <p>© 2017 TripAdvisor LLC All rights reserved. Terms of Use | Privacy Policy | Site Map</p>
                </div>


            </div><!--.footer-->
        </div><!--.wrapper-->
    </div>
</div>

<div id="fb-root"></div>

</body>
</html>



<script src="{{ asset("js/slick.min.js")}}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#gallery_01').slick({
            dots: false,
            infinite: false,
            speed: 300,
            slidesToShow: 3,
            slidesToScroll: 1,
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#img1").elevateZoom({ gallery: 'gallery_01', cursor: 'pointer', galleryActiveClass: "active" });
        //pass the images to Fancybox--}}
        $("#img1").bind("click", function(e) {
            var ez =   $('#img1').data('elevateZoom');
            $.fancybox(ez.getGalleryList());
            return false;
        });
    });
</script>
