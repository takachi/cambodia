@extends('layouts.frontend.desktop')
@section('content')

    <div class="pvd-vdo section">
        <div class="container">
            <div class="row">

                <div class="title">
                    <h2>Restaurant</h2>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        @foreach($retaurants as $retaurant)
                            <div class="list">
                                <div class="col-sm-3 col-xs-12">
                                    <div class="article">
                                        <a href="/detail">
                                            <div class="thum">
                                                <img src="{{ asset('uploads\enocafe/'. $retaurant->images)}}" alt="">
                                                <div class="mask"></div>
                                            </div>
                                            <h2>{{$retaurant->shop_name}}</h2>
                                        </a>
                                        <div class="description">
                                        <span>
                                            <?php $date = new DateTime($retaurant->date_time); echo $date->format('d M Y | g:i A')?>
                                        </span>
                                            {{--<span>27 Jul 2017 | 11:16 AM</span>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="pagination">
                    <ul>
                        <?php echo $retaurants->links(); ?>
                        {{--{{$retaurants->links()}}--}}
                        {{--<li><a href="#">1</a></li>--}}
                        {{--<li><a href="#">2</a></li>--}}
                        {{--<li><a href="#">3</a></li>--}}
                        {{--<li><a href="#">4</a></li>--}}
                        {{--<li><a href="#">5</a></li>--}}
                    </ul>
                </div>
            </div>
        </div>
    </div>

@stop
