@extends('layouts.frontend.desktop')
@section('content')

<div class="pvd-vdo section">
    <div class="container">
        <div class="row">
        
            <div class="title">
                <h2>Hotel</h2>
            </div>
            <div class="see-all">
                <h2><a href="#">See All</a></h2>
            </div>
            <div class="col-sm-12">
                <div class="row">
                    <?php for($i=1; $i<=8; $i++){; ?>
                        <div class="list">
                            <div class="col-sm-3 col-xs-12">
                                <div class="article">
                                    <a href="#">
                                        <div class="thum">
                                            <img src="assets/images/hotel-<?= $i; ?>.jpg"/>
                                            <div class="mask"></div>
                                        </div>
                                        <h2>Paper Toy</h2>
                                    </a>
                                    <div class="description">
                                        <span>27 Jul 2017 | 11:16 AM</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php }; ?>
                </div>
            </div>
            
            <div class="title">
                <h2>Restaurant</h2>
            </div>
            <div class="see-all">
                <h2><a href="#">See All</a></h2>
            </div>
            <div class="col-sm-12">
                <div class="row">
                    <?php for($i=1; $i<=8; $i++){; ?>
                        <div class="list">
                            <div class="col-sm-3 col-xs-12">
                                <div class="article">
                                    <a href="#">
                                        <div class="thum">
                                            <img src="assets/images/restaurant-<?= $i; ?>.jpg"/>
                                            <div class="mask"></div>
                                        </div>
                                        <h2>Paper Toy</h2>
                                    </a>
                                    <div class="description">
                                        <span>27 Jul 2017 | 11:16 AM</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php }; ?>
                </div>
            </div>
            
            <div class="title">
                <h2>Shop</h2>
            </div>
            <div class="see-all">
                <h2><a href="#">See All</a></h2>
            </div>
            <div class="col-sm-12">
                <div class="row">
                    <?php for($i=1; $i<=8; $i++){; ?>
                        <div class="list">
                            <div class="col-sm-3 col-xs-12">
                                <div class="article">
                                    <a href="#">
                                        <div class="thum">
                                            <img src="assets/images/shop-<?= $i; ?>.jpg"/>
                                            <div class="mask"></div>
                                        </div>
                                        <h2>Paper Toy</h2>
                                    </a>
                                    <div class="description">
                                        <span>27 Jul 2017 | 11:16 AM</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php }; ?>
                </div>
            </div>
            
        </div>
    </div>
</div>
@stop
