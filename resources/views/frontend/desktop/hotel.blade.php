@extends('layouts.frontend.desktop')
@section('content')
    
<div class="pvd-vdo section">
    <div class="container">
        <div class="row">
        
            <div class="title">
                <h2>Hotel</h2>
            </div>
            <div class="col-sm-12">
                <div class="row">
                    @foreach($hotels_pages as $hotels_page)
                        <div class="list">
                            <div class="col-sm-3 col-xs-12">
                                <div class="article">
                                    <a href="/detail">
                                        <div class="thum">
                                            <img src="{{ asset('uploads\asainkkohkong/'. $hotels_page->images)}}" alt="">
                                            <div class="mask"></div>
                                        </div>
                                        <h2>{{$hotels_page->shop_name}}</h2>
                                    </a>
                                    <div class="description">
                                        <span>
                                            <?php $date = new DateTime($hotels_page->date_time); echo $date->format('d M Y | g:i A')?>
                                        </span>
                                        {{--<span>27 Jul 2017 | 11:16 AM</span>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>


                <div class="pagination">
                    <ul>
                        <?php echo $hotels_pages->links(); ?>

                    </ul>
                </div>
            </div>
            
        </div>
    </div>
</div>
@stop
