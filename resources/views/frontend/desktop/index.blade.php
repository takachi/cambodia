@extends('layouts.frontend.desktop')
@section('content')

<div class="pvd-vdo section">
    <div class="container">
        <div class="row">
        
            <div class="title">
                <h2>Hotel</h2>
            </div>
            <div class="see-all">
                <h2><a href="{{ url('/hotel') }}">See All</a></h2>
            </div>
            <div class="col-sm-12">
                <div class="row">
                    @foreach($hotels as $hotel)
                        <div class="list">
                            <div class="col-sm-3 col-xs-12">
                                <div class="article">
                                    <a href="{{url('/detail')}}">
                                        <div class="thum">
                                            <img src="{{ asset('uploads\asainkkohkong/'. $hotel->images)}}" alt="">
                                            <div class="mask"></div>
                                        </div>
                                        <h2>{{$hotel->shop_name}}</h2>
                                    </a>
                                    <div class="description">
                                        <span>
                                            <?php $date = new DateTime($hotel->date_time); echo $date->format('d M Y | g:i A')?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            
            <div class="title">
                <h2>Restaurant</h2>
            </div>
            <div class="see-all">
                <h2><a href="{{url('/restaurant')}}">See All</a></h2>
            </div>
            <div class="col-sm-12">
                <div class="row">
                    @foreach($restaurents as $restaurent)
                        <div class="list">
                            <div class="col-sm-3 col-xs-12">
                                <div class="article">
                                    <a href="{{url('/detail')}}">
                                        <div class="thum">
                                            <img src="{{ asset('uploads\enocafe/'. $restaurent->images)}}" alt="">
                                            <div class="mask"></div>
                                        </div>
                                        <h2>{{$restaurent->shop_name}}</h2>
                                    </a>
                                    <div class="description">
                                        <span>
                                            <?php $date = new DateTime($restaurent->date_time); echo $date->format('d M Y | g:i A')?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            
            <div class="title">
                <h2>Shop</h2>
            </div>
            <div class="see-all">
                <h2><a href="{{ url('/shop')}}">See All</a></h2>
            </div>
            <div class="col-sm-12">
                <div class="row">
                    @foreach($shops as $shop)
                        <div class="list">
                            <div class="col-sm-3 col-xs-12">
                                <div class="article">
                                    <a href="{{url('/detail')}}">
                                        <div class="thum">
                                            <img src="{{ asset('uploads\bengsalang/'. $shop->images)}}" alt="">
                                            <div class="mask"></div>
                                        </div>
                                        <h2>{{$shop->shop_name}}</h2>
                                    </a>
                                    <div class="description">
                                        {{--<span>27 Jul 2017 | 11:16 AM</span>--}}
                                        <span>
                                            <?php $date = new DateTime($shop->date_time); echo $date->format('d M Y | g:i A')?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            
        </div>
    </div>
</div>
@stop
