<!-- todo:should move admin layouts to layout directory -->
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <title>cambodia Intermarket</title>

    @include('partials.cssInclude')

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    {{--header block--}}
    @include('partials.header')

    {{--header slidebar_left--}}
    @include('partials.sidebarLeft')

    {{--content block--}}
    @yield('content')


    {{--footer block    --}}
    @include('partials.footer')

</div>

    @include('partials.sconfig')

    @yield('internalJsFooter')


</body>
</html>







