<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="IE=9" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Cambodia Intermarket</title>
	<link href="{{ asset('assets/scripts/bootstrap/bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{ asset('assets/css/desk-style.css')}}" rel="stylesheet">
	<link href="{{ asset('assets/css/desk-style-home.css')}}" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Content:400,700" rel="stylesheet">


	<!--new main-slide and other slide -->
	<link rel="stylesheet" href="cs/slick.css">
	<link rel="stylesheet" href="cs/slick-theme.css">
	<link rel="stylesheet" href="cs/style-elegant-modal.css">


</head>

<body>
<div class="wrapper">
	@include('partials.frontend.header')
</div>

<div class="wrapper">
	<div class="content">
		@yield('content')
	</div>
</div>

<div class="wrapper">
	<div class="footer maincolor">
		@include('partials.frontend.footer')
	</div>
</div>

<div id="fb-root"></div>

</body>
</html>
