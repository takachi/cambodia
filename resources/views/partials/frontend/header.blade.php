
	<div class="header">

		<div class="container">
			<div class="pull-left">

				<div class="logo">
					<a href="{{url('/')}}"><img src="assets/images/logo.svg"></a>
				</div>
				<div class="menu">
					<nav id="menu-main">
						<ul>
							<li><a href="{{url('/hotel')}}">Hotel</a></li>
							<li><a href="{{url('/restaurant')}}">Restaurant</a></li>
							<li><a href="{{url('/shop')}}">Shop</a></li>
							<li><a href="#">Car Rental</a></li>
							<li><a href="#">Coupon</a></li>
						</ul>
					</nav>
				</div>
				<!--menu on mobile-->
				<nav id="menu">
					<ul>
						<li><a href="{{url('/hotel')}}">Hotel</a></li>
						<li><a href="{{url('/restaurant')}}">Restaurant</a></li>
						<li><a href="{{url('/shop')}}">Shop</a></li>
						<li><a href="#">Car Rental</a></li>
						<li><a href="#">Coupon</a></li>
					</ul>
				</nav>
				<div class="humberger">
					<a href="#menu">
						<img src="assets/images/hamburger.svg"/>
					</a>
				</div>

			</div>
		</div>

	</div><!--.header--> 

	<div class="banner-slider">
		<div class="slider-banner">
			<?php for($i=1; $i<=4; $i++){; ?>
				<div>
					<img src="assets/images/banner<?= $i; ?>.jpg"/>
				</div>
			<?php }; ?>
		</div>
	</div><!--.banner-slider-->

