    <div class="wrapper">
        <div class="footer">
                
            <div class="footer-top">
                <div class="container">
                    <div class="top">
                        <div class="row">
                       
                            <div class="col-sm-6 col-xs-12">
                                <h1>Contact Us</h1>
                                <ul>
                                    <li><span class="address"></span>No. 168, National Road No 05, Sangkat Prek Phnov, Khan Sen Sok, Phnom Penh, Cambodia</li>
                                    <li><span class="phone"></span><a href="">(+855) 12 792 707 / 10 571 889</a></li>
                                    <li><span class="emial"></span><a href="">cambodiaintermarket @gmail.com</a></li>
                                </ul>
                                <h2>Social Network</h2>
                                <div class="social">
                                	<ul>
                                		<li>
                                        	<a href="#">
                                            	<img src="assets/images/fb.png"/>
                                                <img src="assets/images/fb-.png"/>
                                            </a>
                                        </li>
                                        <li>
                                        	<a href="#">
                                            	<img src="assets/images/linkedin.png"/>
                                                <img src="assets/images/linkedin-.png"/>
                                            </a>
                                        </li>
                                        <li>
                                        	<a href="#">
                                            	<img src="assets/images/in.png"/>
                                                <img src="assets/images/in-.png"/>
                                            </a>
                                        </li>
                                        <li>
                                        	<a href="#">
                                            	<img src="assets/images/g.png"/>
                                                <img src="assets/images/g-.png"/>
                                            </a>
                                        </li>
                                	</ul>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12 description">
                                <div class="logo-bottom">
                                    <img src="assets/images/logo-bottom.png"/>
                                </div>
                                <p>* TripAdvisor LLC is not a booking agent and does not charge any service fees to users of our site... (more)
TripAdvisor LLC is not responsible for content on external web sites. Taxes, fees not included for deals content.</p>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            
           
                <div class="footer-bottom">
                    <p>© 2017 TripAdvisor LLC All rights reserved. Terms of Use | Privacy Policy | Site Map</p>
                </div>
            
        
        </div><!--.footer-->
    </div><!--.wrapper-->
    
    <!--library css and js-->


    <link href="{{ asset('assets/scripts/mmenu/jquery.mmenu.all.css') }}" rel="stylesheet"/>

    <link href="{{ asset('assets/scripts/slick/slick.css')}}" rel="stylesheet"/>
    <link href="{{ asset('assets/scripts/color-box/colorbox.css')}}" rel="stylesheet"/>

    <link href="{{ asset('assets/scripts/nerveSlider/nerveSlider.min.css')}} " rel="stylesheet"/>
    <script src="{{ asset('assets/scripts/js/jquery.min.js')}}"></script>
    <script src="{{ asset('assets/scripts/mmenu/jquery.mmenu.all.js')}}"></script>
    <script src="{{ asset('assets/scripts/js/jquery.marquee.min.js')}}"></script>
    <script src="{{ asset('assets/scripts/slick/slick.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/scripts/color-box/jquery.colorbox.js')}}"></script>
    <script src="{{ asset('assets/scripts/nerveSlider/jquery-ui-1.10.2.min.js')}}"></script>
	<script src="{{ asset('assets/scripts/nerveSlider/jquery.nerveSlider.min.js')}}"></script>


    <script>
		// JQuery for menu on mobile
		jQuery(document).ready(function($) {
			$("nav#menu").mmenu({
				"offCanvas": {
					"position": "right"
				},
				"extensions":[
					"fx-panels-zoom",
					"fx-listitems-slide",
					"border-full",
					"shadow-page",
					"fullscreen",
					"theme-white"
				],
				"setSelected":{
					"hover": true,
					"parent": true,
					"current": true
				},
				keyboardNavigation: true,
				screenReader: true,
				counters: true,
				navbars:[{
					position: 'top', //top or bottom
					content:[
						'prev',
						'title',
						'close',

					]
				}]
			});
		});
		
		// JQuery for hero banner slider home page
		$(document).ready(function(){
			$(".slider-banner").nerveSlider({
				slideTransitionSpeed: 2000,
				slideTransitionDelay: 5000,
				slideTransitionEasing: "easeOutExpo",
				sliderFullscreen: false,
				showDots: true,
				showPause: true,
				showTimer: false,
				slideTransitionDirection: "left",
				slideTransition: "slide",
				sliderResizable: true,
				sliderWidth: "1920",
				sliderHeight: "550",
				sliderAutoPlay: true,
				showFilmstrip: false,
				sliderTheme: "light",
				slidesDraggable: true,
				allowKeyboardEvents: true,
				sliderKeepAspectRatio: true
			});
		});
    </script>
             
	</body>
</html>