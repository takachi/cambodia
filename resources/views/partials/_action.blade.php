{{--<a title="Show" href="{{ route('dashboard.'.$entityName.'.show', $entity) }}" class="btn btn-xs btn-primary">--}}
    {{--<i class="glyphicon glyphicon-eye-open"></i>--}}
{{--</a>--}}

<a title="Edit" href="{{ route('dashboard.'.$entityName.'.edit', $entity) }}" class="btn btn-xs btn-success">
    <i class="glyphicon glyphicon-pencil"></i>
</a>

{!! Form::open( ['method' => 'delete', 'url' => route('dashboard.'.$entityName.'.destroy', $entity), 'style' => 'display: inline', 'onSubmit' => 'return confirm("Are you sure you want to delete?")']) !!}
<button title="Delete" class="btn btn-xs btn-danger">
    <i class="glyphicon glyphicon-trash"></i>
</button>
{!! Form::close() !!}

