<?php
    /**
     * Image modal required 3 params
     * Param1: $id
     * Param2: $title
     * Param3: $src
     */
?>
<a data-toggle="modal" data-target="#image-{{ $id }}">
    <img src="{{ $src  ? asset($src) : asset('assets/images/sample_logo.png') }}"
         style="height: 25px;"
         title="{{ $title }}">
</a>
<div class="modal fade" id="image-{{ $id }}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>
                <h4 class="modal-title">{{ $title }}</h4>
            </div>
            <div class="modal-body">
                <img src="{{ $src  ? asset($src) : asset('assets/images/sample_logo.png') }}" class="img-responsive">
            </div>
        </div>
    </div>
</div>
