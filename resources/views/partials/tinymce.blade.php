<textarea class="tinymce form-control"
          name="{{ isset($name) ? $name : '' }}"
          {{ isset($required) ? $required : '' }}>
    {!! isset($entity->$name) ? $entity->$name : '' !!}
</textarea>
