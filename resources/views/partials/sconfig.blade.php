<!-- jQuery 2.2.3 -->
{{--<script src="{{ asset('admin_assets/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>--}}
<script src="{{ asset('//code.jquery.com/jquery-1.10.2.min.js') }}"></script>

<!-- Bootstrap 3.3.6 -->
<script src="{{ asset('admin_assets/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ asset('admin_assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>



<!-- DataTables -->
<script src="{{ asset('//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css') }}"></script>

{{--<script src="{{ asset('admin_assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>--}}
{{--<script src="{{ asset('admin_assets/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>--}}
{{--<script src="{{ asset('admin_assets/plugins/datatables/dataTables.fixedColumns.min.js') }}"></script>--}}

<!-- FastClick -->
<script src="{{ asset('admin_assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('admin_assets/dist/js/app.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('admin_assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('admin_assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('admin_assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- SlimScroll 1.3.0 -->
<script src="{{ asset('admin_assets/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>


{{--<!-- InputMask -->--}}
{{--<script src="{{ asset('admin_assets/plugins/input-mask/jquery.inputmask.js') }}"></script>--}}
{{--<script src="{{ asset('admin_assets/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>--}}
{{--<script src="{{ asset('admin_assets/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>--}}

<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="{{ asset('admin_assets/dist/js/demo.js') }}"></script>

<script>

    $(document).ready(function(){

        $('[rel=popover]').popover({
            html: true,
            trigger: 'hover',
            placement: 'right',
            content: function(){return '<img src="'+$(this).data('img') + '" style="width: 100%" />';}
        });

//         old data table
        jQuery('.default-data-table').dataTable({
            "responsive": true,
            "scrollCollapse": true,
            "paging": true,
            "aaSorting": [],
            "lengthMenu":  [[10, 20, 50, -1], [10, 20, 50, "All"]],
            "oSearch": {
                "bSmart": false
            }
        });

        $('#example').DataTable( {
            "processing": true,
            "serverSide": true,
            "ajax": "../server_side/scripts/server_processing.php"
        } );


        //Date picker
        $('#datepickerEn').datepicker({
            autoclose: true
        });
        $('#datepickerKh').datepicker({
            autoclose: true
        });
    });

    $(document).ready(function() {
        if (location.hash) {
            $("a[href='" + location.hash + "']").tab("show");
        }
        $(document.body).on("click", "a[data-toggle]", function(event) {
            location.hash = this.getAttribute("href");
        });
    });
    $(window).on("popstate", function() {
        var anchor = location.hash || $("a[data-toggle='tab']").first().attr("href");
        $("a[href='" + anchor + "']").tab("show");
    });


    $('.datepicker').datepicker({
      format: 'dd-mm-yyyy',
      startDate: '+0d',
      autoclose: true
    });

    $('.add-carousel-item').on('click', function(){
      var currentFieldset = $(this).parent().children().first();
      var item = currentFieldset.clone();
      var length = currentFieldset.parent().find('fieldset').length;
      var dynamicFormElement = item.find('.dynamic');
      var dynamicLegendElement = item.find('legend');
      $(dynamicLegendElement[0]).text('Item '+ length);
      $(dynamicFormElement[0]).prop({ name: "carousels["+length+"]"});
      $(this).before(item);
    });

    var route_prefix = "{{ url(config('lfm.prefix')) }}";
    var editor_config = {
      path_absolute : "",
      selector: ".tinymce",
      menubar: false,
      plugins: [
        'lists link image charmap print preview anchor textcolor',
      ],
      toolbar: 'formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent',
      relative_urls: false,
      height: 70,
      file_browser_callback : function(field_name, url, type, win) {
        var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
        var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

        var cmsURL = editor_config.path_absolute + route_prefix + '?field_name=' + field_name;
        if (type == 'image') {
          cmsURL = cmsURL + "&type=Images";
        } else {
          cmsURL = cmsURL + "&type=Files";
        }

        tinyMCE.activeEditor.windowManager.open({
          file : cmsURL,
          title : 'Filemanager',
          width : x * 0.8,
          height : y * 0.8,
          resizable : "yes",
          close_previous : "no"
        });
      }
    };
    tinymce.init(editor_config);

</script>

<script>
    var route_prefix = "{{ url(config('lfm.prefix')) }}";
    {!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/lfm.js')) !!}
    $('.lfm').filemanager('image', {prefix: route_prefix});
</script>









