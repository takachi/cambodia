<div class="input-group">
          <span class="input-group-btn">
            <a data-input="{{ $id }}" data-preview="holder-{{ $id }}" class="lfm btn btn-primary">
              <i class="fa fa-picture-o"></i> Choose
            </a>
          </span>
    <input id="{{ $id }}" class="form-control" type="text" name="{{ $name }}" value="{{ isset($url) ? $url : '' }}">
</div>
<div>
    <img id="holder-{{ $id }}" style="margin-top:15px;max-height:100px;">
</div>
