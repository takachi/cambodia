@if($errors->any())
    <div id="sms" class="text-center alert {{ $errors->get('class')[0] }}">
        {{ $errors->get('sms')[0] }}
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <script>
            setTimeout(function()
            {
	            $('#sms').fadeOut();
            }, 4000);

        </script>
    </div>
@endif

