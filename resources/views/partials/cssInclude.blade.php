<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="{{ asset('admin_assets/bootstrap/css/bootstrap.min.css') }}">

<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('admin_assets/plugins/datatables/dataTables.bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin_assets/plugins/datatables/dataTables.responsive.min.css') }}">

<!-- Font Awesome -->
<link rel="stylesheet" href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css') }}">

<!-- todo: should not use cdn-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" />

<!-- Ionicons -->
<link rel="stylesheet" href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css') }}">

<!-- jvectormap -->
<link rel="stylesheet" href="{{ asset('admin_assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">

<!-- Skin -->
<link type="text/css" rel="stylesheet" href="{{ asset('admin_assets/dist/css/skins/_all-skins.min.css') }}">

<!-- Theme style -->
<link rel="stylesheet" href="{{ asset('admin_assets/dist/css/AdminLTE.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin_assets/dist/css/app.css') }}">
