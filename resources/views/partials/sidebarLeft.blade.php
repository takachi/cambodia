<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            {{--<li class="header">MAIN NAVIGATION</li>--}}
            <li class=" ">
                <a href="/dashboard">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                    <span class="pull-right-container">
                        <small class="label pull-right bg-green"></small>
                    </span>
                </a>
            </li>
            <li class="{{ isset($is_user_active) ? 'active' : '' }}">
                <a href="{{url('dashboard/user')}}">
                    <i class="fa fa-users" aria-hidden="true"></i>
                    <span>Administrator</span>
                    <span class="pull-right-container">
                        <small class="label pull-right bg-green"></small>
                    </span>
                </a>
            </li>
            <li class="{{ isset($is_hotel_active) ? 'active' : '' }}">
                <a href="{{url('dashboard/hotel')}}">
                    <i class="fa fa-bed" aria-hidden="true"></i>
                    <span>Hotel</span>
                    <span class="pull-right-container">
                        <small class="label pull-right bg-green"></small>
                    </span>
                </a>
            </li>
            <li class="{{ isset($is_restaurant_active) ? 'active' : '' }}">
                <a href="{{url('dashboard/restaurant')}}">
                    <i class="fa fa-cutlery" aria-hidden="true"></i>
                    <span>Restaurant</span>
                    <span class="pull-right-container">
                        <small class="label pull-right bg-green"></small>
                    </span>
                </a>
            </li>
            <li class="{{ isset($is_shop_active) ? 'active' : '' }}">
                <a href="{{url('dashboard/shop')}}">
                    <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                    <span>Shop</span>
                    <span class="pull-right-container">
                        <small class="label pull-right bg-green"></small>
                    </span>
                </a>
            </li>
            {{--<li class="{{ isset($is_main_regulation_active) ? 'active' : '' }}">--}}
                {{--<a href="{{url('dashboard/regulation-ladding-page')}}">--}}
                    {{--<i class="fa fa-th"></i>--}}
                    {{--<span >Regulations</span>--}}
                    {{--<span class="pull-right-container">--}}
                        {{--<i class="fa fa-angle-left pull-right"></i>--}}
                    {{--</span>--}}
                {{--</a>--}}

                {{--<ul class="treeview-menu">--}}
                    {{--<li class="{{ isset($is_regulation_active) ? 'active' : '' }}">--}}
                        {{--<a href="{{url('dashboard/regulation-ladding-page')}}"><i class="fa fa-circle-o"></i>Regulation</a>--}}
                    {{--</li>--}}
                    {{--<li class="{{ isset($is_regulation_active) ? 'active' : '' }}">--}}
                        {{--<a href="{{url('dashboard/regulation')}}"><i class="fa fa-circle-o"></i>Regulation Lading</a>--}}
                    {{--</li>--}}
                    {{--<li class="{{ isset($is_sme_policy_frameworks_active) ? 'active' : '' }}">--}}
                        {{--<a href="{{url('dashboard/smepolicy')}}"><i class="fa fa-circle-o"></i>SME Policy & Frameworks</a>--}}
                    {{--</li>--}}
                    {{--<li class=""><a href="{{url('dashboard/sme-licence-mapping')}}"><i class="fa fa-circle-o"></i> SME Licence Mapping</a></li>--}}
                    {{--<li class="{{ isset($is_main_sme_active) ? 'active' : '' }}">--}}
                        {{--<a href="#"><i class="fa fa-circle-o"></i> SME Licence Mapping--}}
                            {{--<span class="pull-right-container">--}}
                                {{--<i class="fa fa-angle-left pull-right"></i>--}}
                            {{--</span>--}}
                        {{--</a>--}}
                        {{--<ul class="treeview-menu">--}}
                            {{--<li class="{{ isset($is_sme_licence_mapping_landing_page_active) ? 'active' : '' }}">--}}
                                {{--<a href="{{url('dashboard/sme-licence-mapping-ladding')}}"><i class="fa fa-circle-o"></i> Laddding Page</a>--}}
                            {{--</li>--}}
                            {{--<li class="{{ isset($is_sme_licence_mapping_active) ? 'active' : '' }}">--}}
                                {{--<a href="{{url('dashboard/sme-licence-mapping')}}"><i class="fa fa-circle-o"></i> Detail Page</a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li class="{{ isset($is_sme_license_mapping_active) ? 'active' : '' }}">--}}
                        {{--<a href="{{url('dashboard/sme-licence-mapping')}}"><i class="fa fa-circle-o"></i>SME Licence Mapping</a>--}}
                    {{--</li>--}}
                    {{--<li class="{{ isset($is_related_ministry_active) ? 'active' : '' }}">--}}
                        {{--<a href="{{url('dashboard/relatedministry')}}"><i class="fa fa-circle-o"></i>Related Ministry</a>--}}
                    {{--</li>--}}

                {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="{{ isset($is_services_active) ? 'active' : '' }}">--}}
                {{--<a href="{{ action('admin\ServiceController@index') }}">--}}
                    {{--<i class="fa fa-th"></i>--}}
                    {{--<span>Service</span>--}}
                   {{--<span class="pull-right-container">--}}
                        {{--<i class="fa fa-angle-left pull-right"></i>--}}
                    {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu">--}}
                    {{--<li class="{{ isset($is_services_active) ? 'active' : '' }}">--}}
                        {{--<a href="{{ action('admin\ServiceController@index') }}"><i class="fa fa-circle-o"></i>Service Lading</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="{{ isset($is_main_directories_active) ? 'active' : '' }}">--}}
                {{--<a href="{{ url('dashboard/directory') }}">--}}
                    {{--<i class="fa fa-folder"></i>--}}
                    {{--<span>@lang('label.directories')</span>--}}
                    {{--<span class="pull-right-container">--}}
                        {{--<i class="fa fa-angle-left pull-right"></i>--}}
                    {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu">--}}
                    {{--<li class="{{ isset($is_directories_active) ? 'active' : '' }}">--}}
                        {{--<a href="{{url('dashboard/directory')}}"><i class="fa fa-circle-o"></i>Directory Lading</a>--}}
                    {{--</li>--}}
                    {{--<li class="{{ isset($is_information_active) ? 'active' : '' }}">--}}
                        {{--<a href="{{url('dashboard/information')}}"><i class="fa fa-circle-o"></i>Information</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}

            {{--</li>--}}
            {{--<li class="hidden {{ isset($is_herobanner_active) ? 'active' : '' }}">--}}
                {{--<a href="{{ url('dashboard/herobanner') }}">--}}
                    {{--<i class="fa fa-picture-o"></i>--}}
                    {{--<span>@lang('label.herobanner')</span>--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--<li class="{{ isset($is_advertisement_active) ? 'active' : '' }}">--}}
                {{--<a href="{{ url('dashboard/advertisement') }}">--}}
                    {{--<i class="fa fa-image"></i>--}}
                    {{--<span>@lang('label.advertisement')</span>--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--<li class="{{ isset($is_media_active) ? 'active' : '' }}">--}}
                {{--<a href="/laravel-filemanager?type=image">--}}
                    {{--<i class="fa fa-file"></i>--}}
                    {{--<span>@lang('label.media')</span>--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--<li class="{{ isset($is_setting_active) ? 'active' : '' }}">--}}
                {{--<a href="{{ url('dashboard/setting') }}">--}}
                    {{--<i class="fa fa-cog"></i>--}}
                    {{--<span>@lang('label.setting')</span>--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--<li class="{{ isset($is_menu_active) ? 'active' : '' }}">--}}
                {{--<a href="{{ url('dashboard/menu') }}">--}}
                    {{--<i class="fa fa-cog"></i>--}}
                    {{--<span>@lang('label.menu')</span>--}}
                {{--</a>--}}
            {{--</li>--}}
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>














