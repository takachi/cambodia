<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Business Information Center</title>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css"/>
<script type="text/javascript" src="https//cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript" src="https//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<style type="text/css">
.background{
	background: rgba(0, 0, 0, 0) url("{{asset('assets/images/temp/background.jpg')}}") no-repeat fixed center top / cover ;
	bottom: 0;
	float: left;
	height: 100%;
	position: absolute;
	width: 100%;
	z-index: -1;
}
.main-form{
	top: 45%;
	left: 0;
	right: 0;
	position: absolute;
	color: #fff;
	-webkit-transform: translateY(-50%);
	   -moz-transform: translateY(-50%);
		-ms-transform: translateY(-50%);
		 -o-transform: translateY(-50%);
			transform: translateY(-50%)
}
.main-form .main-content{
	float: left;
	padding: 50px;
	text-align: center;
	width: 100%;
}
.main-form .main-content .main-logo > img{ margin: 0 auto }
.main-form .main-content .description{
	margin: 30px auto 50px
}
.main-form .main-content .description > h3{ 
	font-size: 36px;
	line-height: 46px
}
.main-form .main-content .form-group{
	display: inline-block;
	float: none;
	margin: 0 auto 20px;
	width: 80%;
}
.main-form .main-content .form-group input::-moz-placeholder{ color: #999 }
.main-form .main-content .form-group > .form-control{
	float: left;
	margin-bottom: 0;
	box-shadow: none; 
	border: 1px solid #fff;
	background: transparent;
	border-radius: 0;
	height: 50px;
	line-height: 50px;
	font-size: 22px;
	color: #fff;
	width: 48%;
	font-weight: 100
}
.main-form .main-content .form-group > .form-control:nth-child(1){ float: left }
.main-form .main-content .form-group > .form-control:nth-child(2){ float: right }

.main-form .main-content .form-group .form-control:focus{ box-shadow: none; border: 1px solid #fff }


.main-form .main-content .form-group > .submit{
	background: #2bb813;
	border-left: 0 none;
	width: 100%;
	text-transform: uppercase;
	border-radius: 0;
	height: 50px;
	font-size: 22px;
	color: #fff;
	font-weight: 100;
	float: left;
	letter-spacing: 1px
}

.main-form .main-content ul.socialmedia {
	display: inline-block;
	float: none;
	margin-top: 25px;
	padding: 0;
	text-align: center
}
.main-form .main-content ul.socialmedia > li {
	width: 28px;
	float: left;
	list-style-type: none;
	margin: 0 10px
}
.main-form .main-content ul.socialmedia > li > a {
	background-size: 100% auto;
	border-radius: 50%;
	float: left;
	height: 28px;
	line-height: 0;
	margin: 6px 0;
	overflow: hidden;
	width: 28px;
}
.main-form .main-content ul > li > a, .header-top .top-menu ul > li > a:hover {
	transition: color 0.3s ease 0s;
}
ul.socialmedia li[title="Facebook"] > a {
	background-image: url("{{asset('assets/images/temp/facebook-42x42.png')}}");
}
ul.socialmedia li[title="Twitter"] > a {
	background-image: url("{{asset('assets/images/temp/twitter-42x42.png')}}");
}
ul.socialmedia li[title="YouTube"] > a {
	background-image: url("{{asset('assets/images/temp/youtube-42x42.png')}}"); 	}

ul.socialmedia li[title="Facebook"]:hover > a {
	background-color: #3b5998;
}
ul.socialmedia li[title="Twitter"]:hover > a {
	background-color: #1da1f2;
}
ul.socialmedia li[title="YouTube"]:hover > a {
	background-color: #cd201f;
}
ul.socialmedia > li, ul.socialmedia > li:hover, ul.socialmedia > li a, ul.socialmedia > li:hover a {
	transition: transform 0.3s ease 0s;
}

.user-login{ 
	position: absolute; 
	top: 30px; 
	right: 30px;
	z-index: 1 
}
.user-login > a{
	color: #fff;
	font-size: 22px;
	font-weight: 100;
	letter-spacing: 1px
}
.user-login > a:hover{
	text-decoration: underline;
}
@media only screen 
and (max-device-width : 767px){
	.main-form .main-content .form-group > .submit{ font-size: 16px !important }
}
</style>
<body>
	<div class="background"></div>
	<div class="content">
		<div class="user-login">
			<?php /*?><a href="{{asset('home')}}">Login</a><?php */?>
		</div>
		<div class="main-form">
			<div class="container">
				<div class="row">
					<div class="main-content">
						
						<div class="main-logo">
							<img src="{{asset('assets/images/temp/mainlogo-footer.png')}}">
						</div>
						<div class="description">
							<h3>We are still working on it!</h3>
						</div>
				  		
				  		<form action="{{ asset('subscribe') }}">
							<div class="form-group">
								<input type="text" name="subscriber_name" id="full-name" tabindex="1" class="form-control full-name" placeholder="Full Name" value="">
								<input type="text" name="subscriber_email" id="email" tabindex="2" class="form-control email" placeholder="you@company.com" >
								<input type="hidden" name="subscriber_category" value="all-category"/>
							</div>
							<div class="form-group">
								<button type="submit" class="btn submit">Submit this information to get early access</button>
							</div>
						</form>

						<ul class="socialmedia">
							<li title="Facebook">
								<a href="https://www.facebook.com/BizInfoCambodia/" target="_blank"></a>
							</li>
							<li title="Twitter">
								<a href="https://twitter.com/BicCambodia" target="_blank"></a>
							</li>
							<li title="YouTube">
								<a href="https://www.youtube.com/channel/UCGt2eOZSCpPwqPDCUCIw6nA" target="_blank"></a>
							</li>
						</ul> <!-- .socialmedia -->

				  	</div>
			  	</div>
			</div>
		</div>
	</div>
</body>
</html>







