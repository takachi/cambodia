@extends('login.layout_login')
@section('content')
<div class="login-box">
    <div class="login-logo">
        <a href="#"><b>Cambodia</b></br>Intermarket</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>

        <!-- MESSAGE INSERT SUCCESS -->
        @if ($message = Session::get('sms'))
           <div class="alert alert-danger">
            <i class="fa fa-wrench" aria-hidden="true"> {{ $message }}</i>
            </div>
        @endif 
        <!-- END MESSAGE -->


         {{ Form::open(['route'=>'auth.login']) }}

            <div class="form-group has-feedback">
                <input type="text" class="form-control" name="username" placeholder="Username" value="{{ old('username') }}" >

                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control"  name="password" placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8" style="float: right !important; ">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox" name="remember"> Remember Me
                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
                <!-- /.col -->
            </div>
         {{ Form::close() }}

        {{--<div class="social-auth-links text-center">--}}
            {{--<p>- OR -</p>--}}
            {{--<a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using--}}
                {{--Facebook</a>--}}
            {{--<a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using--}}
                {{--Google+</a>--}}
        {{--</div>--}}
        <!-- /.social-auth-links -->

        <a href="#">I forgot my password</a><br>
        {{--<a href="register.html" class="text-center">Register a new membership</a>--}}

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
@stop





