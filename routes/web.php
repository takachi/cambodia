<?php

// use App\Models\Setting;
// use App\Models\Category;
// use App\Models\ContentElement as CE;
// use App\Models\Carousel;
// use App\Models\Service;

// /*
// |--------------------------------------------------------------------------
// | Web Routes
// |--------------------------------------------------------------------------
// |
// | Here is where you can register web routes for your application. These
// | routes are loaded by the RouteServiceProvider within a group which
// | contains the "web" middleware group. Now create something great!
// |
// */

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('login', function () {
    auth()->loginUsingId(1);
    return redirect()->intended();
});



// // change http --> https
if (env('APP_ENV') === 'production') {
    URL::forceScheme('https');
}

Route::get('unisharp', function () {
    return view('demo');
});

// // ------------------
// //	Login User Admin
// // ------------------

Route::group(array('as' => 'auth.', 'prefix' => 'auth', 'middleware' => ['web']), function()
{
    Route::get('login', ['as' => 'login', 'uses' => 'admin\LoginController@index']);
    Route::post('login', ['as' => 'login', 'uses' => 'admin\LoginController@login']);
    Route::get('logout', ['as' => 'logout', 'uses' => 'admin\LoginController@logout']);

});



//Route::group(['middleware' => 'auth'], function () {
//    Route::get('/laravel-filemanager', '\Unisharp\Laravelfilemanager\controllers\LfmController@show');
//    Route::post('/laravel-filemanager/upload', '\Unisharp\Laravelfilemanager\controllers\UploadController@upload');
//    // list all lfm routes here...
//});


// // -----------------
// //  ADMIN
// // ------------------
//Route::group(array('as' => 'auth.', 'prefix' => 'auth', 'middleware' => ['web']), function()
Route::group(array('as' => 'dashboard.', 'prefix' => 'dashboard'), function() {

    //Home Deskboard
    Route::get('/', ['as' => '/', 'uses' => 'admin\Dashboard@index']);

    //User CRUD
    Route::controller('user', 'UserController', [
        'anyData'  => 'datatables.data',
        'index' => 'admin.administrator.user',
    ]);

   // Route::get('user', ['as' => 'user', 'uses' => 'admin\UserController@index']);
    Route::get('user-add', ['as' => 'user-add', 'uses' => 'admin\UserController@addUser']);
    Route::get('user-edit/{id}', ['as' => 'user-edit', 'uses' => 'admin\UserController@editUser']);
    Route::get('user-show/{id}', ['as' => 'user-show', 'uses' => 'admin\UserController@showUser']);

    Route::post('user-create', ['as' => 'user-create', 'uses' => 'admin\UserController@createUser']);
    Route::post('user-update/{id}', ['as' => 'user-update', 'uses' => 'admin\UserController@updateUser']);
    Route::get('user-delete/{id}', ['as' => 'user-delete', 'uses' => 'admin\UserController@deleteUser']);


    //hotel
    Route::get('hotel/test', 'admin\HotelController@test');
    Route::resource('hotel', 'admin\HotelController');


    //restaurant
    Route::resource('restaurant', 'admin\RestaurantController');

    //shop
    Route::resource('shop', 'admin\ShopController');


});



/////////////
//front-end//
/////////////

// Home Page
Route::get('/', 'Frontend\HomeController@landingPage');

//Hotel page
Route::get('/hotel', 'Frontend\HotelController@index');

Route::get('/detail', 'Frontend\DetailController@index');

// Restaurant page
Route::get('/restaurant', 'Frontend\RestaurantController@index');

// Shop page
Route::get('/shop', 'Frontend\ShopController@index');






// Route::get('demo', function () {

//     $topHeaderLabel = json_decode(Setting::where('name', Setting::$settings['topHeaderLabel'])->first()->content);
//     $logo = json_decode(Setting::where('name', Setting::$settings['logo'])->first()->content);
//     $bizInfo = json_decode(Setting::where('name', Setting::$settings['bizInfo'])->first()->content);
//     $bizContact = json_decode(Setting::where('name', Setting::$settings['bizContact'])->first()->content);
//     $categories = Category::where('parent', 0)->get();
//     $subCategories = Category::where('parent', '!=', 0)->get();
//     $socialMedia = json_decode(Setting::where('name', Setting::$settings['socialMedia'])->first()->content);

//     $ceHeroBanner = CE::where('type', CE::HERO_BANNER)->where('name', CE::REGULATION)->first();
//     $heroBanner = json_decode($ceHeroBanner->content)->hero_banner;
//     $cePageTitle = CE::where('type', CE::PAGE_TITLE)->where('name', CE::REGULATION)->first();
//     $pageTitle = json_decode($cePageTitle->content)->page_title;

//     $carousels = Carousel::where('name', Carousel::$pages['service'])->limit(5)->get();

//     $services = Service::all();
// //    dd($services);

//     return view(
//         'frontend.desktop.demo',
//         compact(
//             'topHeaderLabel',
//             'logo',
//             'bizInfo',
//             'bizContact',
//             'socialMedia',
//             'categories',
//             'subCategories',
//             '',
//             'heroBanner',
//             'pageTitle',
//             'carousels',
//             'services'
//         )
//     );
// });



// //use App\Models\Category;
// //use App\Models\Setting;
// use Illuminate\Support\Facades\Input;
// //use Mail;

// Route::group(
// [
// 	'prefix' => LaravelLocalization::setLocale(),
// 	'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
// ],
// function(){



// // ----------------
// //	Administrator
// // ----------------
//
//Route::group(array('as' => 'dashboard.', 'prefix' => 'dashboard', 'middleware' => ['admin']), function()
//
//{
//

//	//Home Deskboard
//	Route::get('/', ['as' => '/', 'uses' => 'admin\Dashboard@index']);
//
//   	//User CRUD
//   	Route::get('user', ['as' => 'user', 'uses' => 'admin\UserController@index']);
//   	Route::get('user-add', ['as' => 'user-add', 'uses' => 'admin\UserController@addUser']);
//    Route::get('user-edit/{id}', ['as' => 'user-edit', 'uses' => 'admin\UserController@editUser']);
//    Route::get('user-show/{id}', ['as' => 'user-show', 'uses' => 'admin\UserController@showUser']);
//
//    Route::post('user-create', ['as' => 'user-create', 'uses' => 'admin\UserController@createUser']);
//    Route::post('user-update/{id}', ['as' => 'user-update', 'uses' => 'admin\UserController@updateUser']);
//    Route::get('user-delete/{id}', ['as' => 'user-delete', 'uses' => 'admin\UserController@deleteUser']);
//
//    //regulation_related_ministry   new version
//    Route::get('related-ministry', ['as' => 'related-ministry', 'uses' => 'admin\RegulationController@RelatedMinistry']);
//
//        Route::get('related-ministry-add', ['as' => 'related-ministry-add', 'uses' => 'admin\RegulationController@RelatedMinistry_add']);
//        Route::get('related-ministry-edit/{id}', ['as' => 'related-ministry-edit', 'uses' => 'admin\RegulationController@RelatedMinistry_edit']);
//        Route::get('related-ministry-show/{id}', ['as' => 'related-ministry-show', 'uses' => 'admin\RegulationController@RelatedMinistry_show']);
//        Route::get('related-ministry-create', ['as' => 'related-ministry-create', 'uses' => 'admin\RegulationController@related-ministry_create']);
//        Route::get('related-ministry-update/{id}', ['as' => 'related-ministry-update', 'uses' => 'admin\RegulationController@RelatedMinistry_update']);
//        Route::get('related-ministry-delete/{id}', ['as' => 'related-ministry-delete', 'uses' => 'admin\UserController@RelatedMinistry_delete']);
//
//
//        Route::get('related-ministry-ladding-page-add', ['as' => 'related-ministry-ladding-page-add', 'uses' => 'admin\RegulationController@Related_ministry_ladding_page_add']);
//        Route::get('related-ministry-ladding-page-edit/{id}', ['as' => 'related-ministry-ladding-page-edit', 'uses' => 'admin\RegulationController@Related_ministry_ladding_page_edit']);
//        Route::get('related-ministry-ladding-page-show/{id}', ['as' => 'related-ministry-ladding-page-show', 'uses' => 'admin\RegulationController@Related_ministry_ladding_page_show']);
//
//        Route::get('related-ministry-ladding-page-create', ['as' => 'related-ministry-ladding-page-create', 'uses' => 'admin\RegulationController@Related_ministry_ladding_page_create']);
//        Route::get('related-ministry-ladding-page-update/{id}', ['as' => 'related-ministry-ladding-page-update', 'uses' => 'admin\RegulationController@Related_ministry_ladding_page_update']);
//        Route::get('related-ministry-ladding-page-delete/{id}', ['as' => 'related-ministry-ladding-page-delete', 'uses' => 'admin\UserController@Related_ministry_ladding_page_delete']);
//
//
////    regulation ladding page
//    Route::get('regulation-ladding-page', 'admin\RegulationController@Regulation');
//    Route::get('regulation-ladding-page-add', 'admin\RegulationController@Regulation_add');
//    Route::get('regulation-ladding-page-edit', 'admin\RegulationController@Regulation_edit');
//    Route::get('regulation-ladding-page-show', 'admin\RegulationController@Regulation_show');
//
//    //Regulation_sme_policy_frameworks
//    Route::get('sme-policy-frameworks', 'admin\RegulationController@sme_policy_frameworks');
//    Route::get('sme-policy-frameworks-add', 'admin\RegulationController@sme_policy_frameworks_add');
//    Route::get('sme-policy-frameworks-edit', 'admin\RegulationController@sme_policy_frameworks_edit');
//    Route::get('sme-policy-frameworks-show', 'admin\RegulationController@sme_policy_frameworks_show');
//
//    Route::get('sme-policy-frameworks-ladding-page-add', 'admin\RegulationController@sme_policy_frameworks_ladding_page_add');
//    Route::get('sme-policy-frameworks-ladding-page-edit', 'admin\RegulationController@sme_policy_frameworks_ladding_page_edit');
//    Route::get('sme-policy-frameworks-ladding-page-show', 'admin\RegulationController@sme_policy_frameworks_ladding_page_show');
//
//
//	//Regulation_sme_licence_frameworks
//	Route::get('sme-licence-mapping', 'admin\RegulationController@sme_licence_mapping');
//    //hero banner
//    Route::get('sme-licence-mapping-herobanner-add', 'admin\RegulationController@sme_licence_mapping_herobanner_add');
//    Route::get('sme-licence-mapping-herobanner-edit', 'admin\RegulationController@sme_licence_mapping_herobanner_edit');
//    Route::get('sme-licence-mapping-herobanner-show', 'admin\RegulationController@sme_licence_mapping_herobanner_show');
//
//    //section top
//    Route::get('sme-licence-mapping-section-top-add', 'admin\RegulationController@sme_licence_mapping_section_top_add');
//    Route::get('sme-licence-mapping-section-top-edit', 'admin\RegulationController@sme_licence_mapping_section_top_edit');
//    Route::get('sme-licence-mapping-section-top-show', 'admin\RegulationController@sme_licence_mapping_section_top_show');
//
//    //section buttom
//    Route::get('sme-licence-mapping-section-buttom-add', 'admin\RegulationController@sme_licence_mapping_section_buttom_add');
//    Route::get('sme-licence-mapping-section-buttom-edit', 'admin\RegulationController@sme_licence_mapping_section_buttom_edit');
//    Route::get('sme-licence-mapping-section-buttom-show', 'admin\RegulationController@sme_licence_mapping_section_buttom_show');
//
//	//sme_licence ladding page
//	Route::get('sme-licence-mapping-ladding', 'admin\RegulationController@sme_licence_mapping_ladding_page');
//
//    //sme-licence ladding-hero banner
//    Route::get('sme-licence-mapping-ladding-page-hero-banner-add', 'admin\RegulationController@sme_licence_mapping_ladding_page_hero_banner_add');
//    Route::get('sme-licence-mapping-ladding-page-hero-banner-edit', 'admin\RegulationController@sme_licence_mapping_ladding_page_hero_banner_edit');
//    Route::get('sme-licence-mapping-ladding-page-hero-banner-show', 'admin\RegulationController@sme_licence_mapping_ladding_page_hero_banner_show');
//
//    //sme-licence ladding-ads
//    Route::get('sme-licence-mapping-ladding-page-ads-add', 'admin\RegulationController@sme_licence_mapping_ladding_page_ads_add');
//    Route::get('sme-licence-mapping-ladding-page-ads-edit', 'admin\RegulationController@sme_licence_mapping_ladding_page_ads_edit');
//    Route::get('sme-licence-mapping-ladding-page-ads-show', 'admin\RegulationController@sme_licence_mapping_ladding_page_ads_show');
//
//    //sme-licence ladding-detail
//    Route::get('sme-licence-mapping-ladding-page-categories-add', 'admin\RegulationController@sme_licence_mapping_ladding_page_categories_add');
//    Route::get('sme-licence-mapping-ladding-page-categories-edit', 'admin\RegulationController@sme_licence_mapping_ladding_page_categories_edit');
//    Route::get('sme-licence-mapping-ladding-page-categories-show', 'admin\RegulationController@sme_licence_mapping_ladding_page_categories_show');
//
//    Route::resource('directory', 'admin\DirectoryController');
//    Route::get('information', 'admin\DirectoryController@information');
//
//    Route::resource('smepolicy', 'admin\PolicyAndFrameworkController');
//    Route::resource('relatedministry', 'admin\RelatedMinistryController');
//    Route::resource('sme-licence-mapping', 'admin\LicenseMappingController');
//    Route::resource('faqs-item', 'admin\FaqsItemController');
//    Route::get('faqs-item/create/{id}', 'admin\FaqsItemController@create');
//    Route::post('faqs-item/update-title/{id}', 'admin\FaqsItemController@licenseMappingUpdate');
//    Route::get('sme-licence-mapping/{id}/faqs', 'admin\FaqsItemController@index');
//
//    Route::resource('herobanner', 'admin\HeroBannerController');
//    Route::resource('slideshow', 'admin\SlideShowController');
//    Route::get('advertisement{?service}', 'admin\AdvertisementController@index');
//    Route::resource('advertisement', 'admin\AdvertisementController');
//    Route::resource('carousel', 'admin\CarouselController');
//    Route::post('carousel/{id}', 'admin\CarouselController@destroy');
//    Route::post('carousel/{id}/edit', 'admin\CarouselController@update');
//
//    Route::resource('setting', 'admin\SettingController');
//    Route::resource('service', 'admin\ServiceController');
//    Route::get('service/{id}/carousel', 'admin\ServiceController@carousel');
//
//    Route::get('service/directories/{id}', 'admin\ServiceController@directories');
//    Route::resource('service', 'admin\ServiceController');
//
//    Route::get('regulation', 'admin\RegulationController@index');
//
//    /** Content Elements */
//    /** todo: should move into route group */
//    Route::post('ce-regulation-category', 'admin\ContentElementController@regulationCategoryStore');
//    Route::post('ce-regulation-category/update/{id}', 'admin\ContentElementController@regulationCategoryUpdate');
//    Route::post('ce-regulation-category/destroy/{id}', 'admin\ContentElementController@regulationCategoryDestroy');
//
//    Route::post('ce-hero-banner', 'admin\ContentElementController@heroBannerStore');
//    Route::post('ce-hero-banner/update/{id}', 'admin\ContentElementController@heroBannerUpdate');
//
//    Route::post('ce-search-box', 'admin\ContentElementController@searchBoxStore');
//    Route::post('ce-search-box/update/{id}', 'admin\ContentElementController@searchBoxUpdate');
//
//    Route::post('ce-page-title', 'admin\ContentElementController@pageTitleStore');
//    Route::post('ce-page-title/update/{id}', 'admin\ContentElementController@pageTitleUpdate');
//
//    Route::group(['prefix' => 'ce'], function()
//    {
//        $path = 'admin\ContentElement';
//        Route::post('service/slide-show', $path.'\SlideShowController@store');
//        Route::post('service/slide-show/update/{id}', $path.'\SlideShowController@update');
//        Route::post('service/slide-show/destroy/{id}', $path.'\SlideShowController@destroy');
//    });
//
//    Route::get('directory-partner{directory?}', 'admin\DirectoryPartnerController@index');
//    Route::get('directory-partner/create{directory?}', 'admin\DirectoryPartnerController@create');
//    Route::post('directory-partner{directory?}', 'admin\DirectoryPartnerController@store');
//    Route::get('directory-partner/edit{directory?}', 'admin\DirectoryPartnerController@edit');
//    Route::post('directory-partner/update{directory?}', 'admin\DirectoryPartnerController@update');
//    Route::post('directory-partner/destroy{id?}', 'admin\DirectoryPartnerController@destroy');
//});

// //-----------
// // End Admin
// //-----------

//     // Home Page
// 	/*Route::get('/', function () {
// 		if(Cookie::get('bizinfo') !== NULL){ return redirect('/home'); }
// 		else{ return view('spash'); }
// 	});*/





	// About Us Page
// 	Route::get('/about-us', 'Frontend\AboutUsController@landingPage');


// 	// Contact Us Page
// 	Route::get('/contact-us', 'Frontend\ContactUsController@landingPage');


// 	// Regulation
// 	Route::get('/regulation', 'Frontend\RegulationController@landingPage');
// 	Route::any('/regulation/search',function(){
// 		$s = Input::get( 'search' );
// 		return redirect('/regulation/sme-license-mapping/'.$s);
// 	});
//     Route::get('/regulation/related-ministry','Frontend\RegulationController@relatedMinistry');
//     Route::get('/regulation/sme-license-mapping','Frontend\RegulationController@licenseMapping');
//     Route::get('/regulation/sme-license-mapping/{item}','Frontend\RegulationController@licenseMappingItem');
//     Route::get('/regulation/cambodia-industry-development-policy','Frontend\RegulationController@policyAndFramework');

//     Route::get('/regulation/{sub_menu}','Frontend\RegulationController@listPage');
// 	Route::get('regulation/{sub_menu}/{pagename}','Frontend\RegulationController@detailPage');


// 	// Business Guides
// 	Route::get('/business-guides', 'Frontend\BusinessGuidesController@landingPage');
// 	Route::get('/business-guides/{sub_menu}','Frontend\BusinessGuidesController@listPage');


// 	// Service
// 	Route::get('/service', 'Frontend\ServiceController@landingPage');
// 	Route::any('/service/search',function(){
// 		$s = Input::get( 'search' );
// 		$c = Input::get( 'category' );
// 		$n = Input::get( 'companyname' );

// 		if($c){ return redirect('/service/'.$c.'?companyname='.$n.'&sort=asc'); }

// 		return redirect('/service/'.$s);
// 	});
// 	Route::get('/service/{name}','Frontend\ServiceController@serviceList');
// 	//Route::get('/service/search','Frontend\ServiceController@listPage');
// 	Route::get('/service/{sub_menu}/{pagename}','Frontend\ServiceController@detailPage');


// 	// Directory
// 	Route::get('/directory', 'Frontend\DirectoryController@landingPage');
// //	Route::get('/directory/search','Frontend\DirectoryController@listPage');
// 	Route::get('/directory/search','Frontend\DirectoryController@directoryList');
// //    Route::get('/directory/list','Frontend\DirectoryController@listPage');
//     Route::get('/directory/list','Frontend\DirectoryController@directoryList');
// //    Route::get('/directory/list/{pagename}','Frontend\DirectoryController@detailPage');
//     Route::get('/directory/list/{pagename}','Frontend\DirectoryController@directoryItem');


// 	// Financing
// 	Route::get('/financing', 'Frontend\FinancingController@landingPage');
// 	Route::get('/financing/{sub_menu}','Frontend\FinancingController@listPage');


// 	// Insight
// 	Route::get('/insight', 'Frontend\InsightController@landingPage');
// 	Route::get('/insight/{sub_menu}','Frontend\InsightController@listPage');


// 	// Opportunity
// 	Route::get('/opportunity', 'Frontend\OpportunityController@landingPage');
// 	Route::get('/opportunity/{sub_menu}','Frontend\OpportunityController@listPage');

// 	// News & Events
// 	Route::get('/news-events', 'Frontend\NewsEventsController@landingPage');
// 	Route::get('/news-events/{sub_menu}', 'Frontend\NewsEventsController@listPage');
// 	Route::get('/news-events/{sub_menu}/{pagename}', 'Frontend\NewsEventsController@detailPage');


// 	Route::any('/subscribe',function(){

// 		$sc = Input::get( 'subscriber_category' );
// 		$sn = Input::get( 'subscriber_name' );
// 		$sm = str_replace('%40','@',Input::get( 'subscriber_email' ));

// 		if($sm){
// 			$message = 'Message';
// 			Mail::send([],[], function ($message){
// 				$message->to('noreply.bizinfo.center@gmail.com');
// 				$message->subject('Subscription from www.bizinfo.center');
// 				$message->from('noreply.bizinfo.center@gmail.com');
// 				$message->setBody('Email: '.str_replace('%40','@',Input::get( 'subscriber_email' )).', Category: '.Input::get( 'subscriber_category' ).', name: '.Input::get( 'subscriber_name' ), 'text/html');
// 			});

// 			DB::table('b121nf0_subscription')->insert(['email' => str_replace('%40','@',Input::get( 'subscriber_email' )), 'category' => Input::get( 'subscriber_category' ), 'date' => date('Y-m-d H:i:s')]);

// 			//Cookie::queue(Cookie::make('bizinfo', 'bizsolution', '1000'));

// 		}

// 		return redirect('/home');
// 	});
// });

// // Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

//Auth::routes();
//
//Route::get('/home', 'HomeController@index')->name('home');
