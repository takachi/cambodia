<?php

use Illuminate\Database\Seeder;
use App\Models\Setting;
use Illuminate\Support\Facades\DB;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $topHeaderLabel = [
            'about_en' => 'About Us',
            'about_kh' => 'អំពី​​យើង',
            'contact_en' => 'Contact Us',
            'contact_kh' => 'ទំនាក់ទំនង'
        ];
        DB::table('b121nf0_settings')->insert([
            'name' => Setting::$settings['topHeaderLabel'],
            'content' => json_encode($topHeaderLabel),
        ]);
        echo "1 top header label created \n";

        $logo = [
            'logo' => 'uploads/mainlogo.png',
        ];
        DB::table('b121nf0_settings')->insert([
            'name' => Setting::$settings['logo'],
            'content' => json_encode($logo),
        ]);
        echo "1 biz logo created \n";

        $bizInfo = [
            'logo_footer' => 'mainlogo-footer.png',
            'biz_info_en' => 'BIC aims to contribute to the development of sustainable & competitive Cambodian SMEs. BIC will promote transparency and effectiveness in implementing related laws and regulations for private enterprises. With this, BIC will provide easy access to reliable information for MSMEs particularly in areas related legal and regulations including registration and licensing; and business operations such as market opportunities, business development service, access to finance, training and networking.',
            'biz_info_kh' => 'មជ្ឈមណ្ឌលសេវាកម្មអាជីវកម្ម មានគោលបំនងដើម្បីរួមចំនែកក្នុងការអភិវឌ្ឍន៍ និរន្តរភាព និង ភាពប្រកួតប្រជែង របស់សហគ្រាសធុនតូច-មធ្យម កម្ពុជា។ មជ្ឈមណ្ឌលនេះ និងជួយបងើ្កនតំលាភាព និង ប្រសិទ្ធភាព ទាក់និងការអនុវត្តន៍ច្បាប់ និង បទបញ្ញាត្តិ សំរាប់ស្ថាប័នឯកជន ជាមួយគ្នានេះដែរ មជ្ឈមណ្ឌលសេវាកម្មអាជីវកម្ម និង ផ្តល់ភាពងាយស្រួល ក្នុងការទទួលបាននូវពត៌មានមួយជាក់លាក់ របស់សហគ្រាសធុនតូច-មធ្យម កម្ពុជា ជាពិសេសទាក់ទងទៅនិង ផ្លូវច្បាប់ និង បទបញ្ញាត្តិ រួមមាន ការចុះបញ្ជីា, អាជ្ញាប័ណ្ណអាជីវកម្ម និងពត៌មានលើដំណើរការអាជីវកម្មដូចជា៖ ឳកាសទីផ្សារ, សេវាកម្ម អភិវឌ្ឍន៍អាជីវកម្ម, ហរិញ្ញប្បទាន, ការបណ្តុះបណ្តាល និង បណ្តាញទំនាក់ទំនង',
        ];
        DB::table('b121nf0_settings')->insert([
            'name' => Setting::$settings['bizInfo'],
            'content' => json_encode($bizInfo),
        ]);
        echo "1 biz info created \n";

        /** todo biz contact */
        $bizContact = [
            'biz_contact_en' => 'Contact Us',
            'biz_contact_kh' => 'ទំនាក់ទំនង',
            'address_en' => 'No. 113, Parkway Square, 2rd Floor, Suite 2FG, 245 Mao Tse Toung Boulevard (245), Phnom Penh',
            'address_kh' => 'kh',
            'phone' => 'Tel: (855) 16 999 762',
            'mail' => 'info@bizinfo.center'
        ];
        DB::table('b121nf0_settings')->insert([
            'name' => Setting::$settings['bizContact'],
            'content' => json_encode($bizContact),
        ]);
        echo "1 biz contact created \n";

        /** todo biz contact */
        $socialMedia = [
            'title_en' => 'Follow Us',
            'title_kh' => 'សកម្មភាពថ្មីៗអំពីយើង',
            'facebook_link' => 'https://www.facebook.com/BizInfoCambodia',
            'tweeter_link' => 'https://twitter.com/BicCambodia',
            'youtube_link' => 'https://www.youtube.com/channel/UCGt2eOZSCpPwqPDCUCIw6nA',
        ];
        DB::table('b121nf0_settings')->insert([
            'name' => Setting::$settings['socialMedia'],
            'content' => json_encode($socialMedia),
        ]);
        echo "1 biz contact created \n";
    }
}
