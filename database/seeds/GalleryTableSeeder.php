<?php

use Illuminate\Database\Seeder;

class GalleryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $images = [4, 5, 6, 7, 8, 9, 10, 11];


        for ($i = 0; $i < 8; $i++) {
            DB::table('b121nf0_gallery')->insert([
                'title_en' => 'Title in english',
                'title_km' => 'Title in khmer',
                'image' => $images[$i],
                'date' => \Carbon\Carbon::now(),
                'modify_date' => \Carbon\Carbon::now(),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]);
        }
    }

}
