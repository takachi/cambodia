<?php

use Illuminate\Database\Seeder;

class AdvertisementTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $urls = [
            'http://bizsolution.biz',
            'http://ggear.com.kh',
            'http://example.com',
            'http://example.com',
            'http://example.com',
            'http://www.agl-group.com',
            'http://www.agl-group.com',
        ];

        $images = [30, 245, 80, 81, 82, 255, 255];

        $sections = [1, 1, 2, 2, 2, 2, 1, 2];

        for ($i = 0; $i < 7; $i++) {
            DB::table('b121nf0_advertisements')->insert([
                'title_en' => 'Title in english',
                'title_kh' => 'Title in khmer',
                'url' => $urls[$i],
                'image' => $images[$i],
                'section' => $sections[$i],
                'date' => \Carbon\Carbon::now(),
                'modify_date' => \Carbon\Carbon::now(),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]);
        }
    }
}
