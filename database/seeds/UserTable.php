<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'=>'admin',
            'email' => ('admin@gmail.com'),
            'password' => bcrypt('123456'),
            'company_name' => 'aa',
            'address' => '60z',
            'phone_number' => '011101010',
            'role' => 'admin',
        ]);
    }
}
