<?php

use Illuminate\Database\Seeder;

class SocialNetworkTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $titles = [
            'en' => [
                'Facebook',
                'Twitter',
                'YouTube',
            ],
            'kh' => [
                'ហ្វេសប៊ុក',
                'Twitter',
                'YouTube',
            ],

        ];

        $urls = [
            'https://www.facebook.com/BizInfoCambodia/',
            'https://twitter.com/BicCambodia',
            'https://www.youtube.com/channel/UCGt2eOZSCpPwqPDCUCIw6nA',
        ];


        for ($i = 0; $i < 3; $i++) {
            DB::table('b121nf0_socialnetwork')->insert([
                'title_en' => $titles['en'][$i],
                'title_km' => $titles['kh'][$i],
                'url' => 0,
                'menu_order' => $i + 1,
                'date' => \Carbon\Carbon::now(),
                'modify_date' => \Carbon\Carbon::now(),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]);
        }
    }
}
