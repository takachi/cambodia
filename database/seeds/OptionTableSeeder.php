<?php

use Illuminate\Database\Seeder;

class OptionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $optionNames = [
            "sitename_en",
            "sitename_km",
            "sitelogo",
            "sitelogo_footer",
            "description_footer_en",
            "description_footer_km",
            "copyright_en",
            "copyright_km",
            "regulation_banner",
            "business_banner",
            "service_banner",
            "directory_banner",
            "financing_banner",
            "insight_banner",
            "opportunity_banner",
            "news_events_banner",
            "home_pin_category_banner_1",
            "home_pin_category_banner_2",
            "home_pin_category_banner_3",
            "related_ministry_banner",
            "sme_license_mapping_banner",
            "business_development_banner",
            "accounting_finance_banner",
            "human_resource_banner",
            "legal_banner",
            "sale_marketing_banner",
            "operation_banner",
            "technology_banner",
            "directory_list_banner",
            "business-development",
            "sme_loan_banner",
            "general_insurance_banner",
            "publication_banner",
            "statistic_banner",
            "job_announcement_banner",
            "bidding_project_banner",
            "project_announcement_banner",
            "industry_events_banner",
            "upcoming_news_events_banner",
            "bic_news_events_banner",
            "about_us_banner",
            "agricultural_input_banner",
            "bakery_banner",
            "grain_milling_banner",
            "guest_house_banner",
            "hotel_banner",
            "meat_ball_banner",
            "restaurant_banner",
            "soy_chilly_banner",
            "tour_operator_banner",
            "water_purify_and_non_alcohol_banner",
            "wholesale_banner",
        ];
        $optionValues = [

            "Business Information Center",
            "មជ្ឈមណ្ឌលព័ត៌មានពាណិជ្ជកម្ម",
            "1",
            "2",
            "BIC aims to contribute to the development of sustainable & competitive Cambodian SMEs. BIC will promote transparency and effectiveness in implementing related laws and regulations for private enterprises. With this, BIC will provide easy access to reliable information for MSMEs particularly in areas related legal and regulations including registration and licensing; and business operations such as market opportunities, business development service, access to finance, training and networking. ",
            "មជ្ឈមណ្ឌលសេវាកម្មអាជីវកម្ម មានគោលបំនងដើម្បីរួមចំនែកក្នុងការអភិវឌ្ឍន៍ និរន្តរភាព និង ភាពប្រកួតប្រជែង របស់សហគ្រាសធុនតូច-មធ្យម កម្ពុជា។ មជ្ឈមណ្ឌលនេះ និងជួយបងើ្កនតំលាភាព និង ប្រសិទ្ធភាព ទាក់និងការអនុវត្តន៍ច្បាប់ និង បទបញ្ញាត្តិ សំរាប់ស្ថាប័នឯកជន ជាមួយគ្នានេះដែរ មជ្ឈមណ្ឌលសេវាកម្មអាជីវកម្ម និង ផ្តល់ភាពងាយស្រួល ក្នុងការទទួលបាននូវពត៌មានមួយជាក់លាក់ របស់សហគ្រាសធុនតូច-មធ្យម កម្ពុជា ជាពិសេសទាក់ទងទៅនិង ផ្លូវច្បាប់ និង បទបញ្ញាត្តិ រួមមាន ការចុះបញ្ជីា, អាជ្ញាប័ណ្ណអាជីវកម្ម និងពត៌មានលើដំណើរការអាជីវកម្មដូចជា៖ ឳកាសទីផ្សារ, សេវាកម្ម អភិវឌ្ឍន៍អាជីវកម្ម, ហរិញ្ញប្បទាន, ការបណ្តុះបណ្តាល និង បណ្តាញទំនាក់ទំនង",
            "Copyright © %y Business Information Center All Rights Reserved. ",
            "Copyright © %y Business Information Center All Rights Reserved. ",
            "17",
            "16",
            "16",
            "17",
            "16",
            "247",
            "16",
            "16",
            "18",
            "19",
            "20",
            "15",
            "15",
            "15",
            "15",
            "15",
            "15",
            "15",
            "15",
            "15",
            "15",
            "15",
            "15",
            "15",
            "15",
            "15",
            "15",
            "15",
            "15",
            "15",
            "15",
            "15",
            "15",
            "259",
            "260",
            "261",
            "262",
            "263",
            "264",
            "266",
            "267",
            "268",
            "265",
            "269",

        ];

        for ($i = 0; $i < 52; $i++) {
            DB::table('b121nf0_options')->insert([
                'option_name' => $optionNames[$i],
                'option_value' => $optionValues[$i],
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]);
        }
        echo "52 Options created \n";
    }
}
