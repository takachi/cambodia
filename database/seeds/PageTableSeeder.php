<?php

use Illuminate\Database\Seeder;

class PageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $titles = [
            'en' => [
                'Home',
                'About Us',
                'Contact Us',
            ],
            'kh' => [
                'ទំព័រដើម',
                'អំពី​​យើង',
                'ទំនាក់ទំនង',
            ],

        ];


        for ($i = 0; $i < 3; $i++) {
            DB::table('b121nf0_page')->insert([
                'title_en' => $titles['en'][$i],
                'title_km' => $titles['kh'][$i],
                'name' => str_replace(str_split('\\/:*?"<>|& '), '-', strtolower($titles['en'][$i])),
                'parent' => 0,
                'landing_page' => 0,
                'menu_order' => $i + 1,
                'date' => \Carbon\Carbon::now(),
                'modify_date' => \Carbon\Carbon::now(),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]);
        }
    }
}
