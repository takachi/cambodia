<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $titles = [
            'en' => [
                'Business Development',
                'Accounting & Finance',
                'Human Resource',
                'Legal',
                'Sale & Marketing',
                'Operation',
                'Technology',
            ],
            'kh' => [
                'ការពង្រីកអាជីវកម្ម',
                'គណនេយ្យ និងហិរញ្ញវត្ថុ',
                'ធនធានមនុស្ស',
                'ច្បាប់',
                'ការលក់ និងផ្សព្វផ្សាយ',
                'ប្រតិបត្តិការ',
                'បច្ចេកវិទ្យា',
            ]
        ];

        $thumbUrls = [
            'uploads/operation.jpg',
            'uploads/accounting-finance.jpg',
            'uploads/human-resource.jpg',
            'uploads/legal.jpg',
            'uploads/sale-marketing.jpg',
            'uploads/business-development.jpg',
            'uploads/technology.jpg',
        ];


        for ($i = 0; $i < 7; $i++) {
            DB::table('b121nf0_services')->insert([
                'title_en' => $titles['en'][$i],
                'title_kh' => $titles['kh'][$i],
                'thumb' => $thumbUrls[$i],
                'banner' => 'seeder/service-list.jpg',
                'ordering' => (7 - $i),
                'name' => str_replace(str_split('\\/:*?"<>|& '), '-', strtolower($titles['en'][$i])),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]);
        }

        echo("7 services was create \n");
    }
}
