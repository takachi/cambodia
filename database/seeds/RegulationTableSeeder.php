<?php

use Illuminate\Database\Seeder;

class RegulationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $titles = [
            'en' => [
                'SME Policy & Frameworks',
                'SME Licence Mapping',
                'Related Ministry',
            ],
            'kh' => [
                'គោលនយោបាយ និងផែនការយុទ្ធសាស្រ្ត របស់សហគ្រាសធុនតូច និងមធ្យម',
                'លក្ខខ័ណ្ឌ និងនីតិវិធីសំរាប់ការចុះបញ្ជីអាជីវកម្ម​របស់សហគ្រាសធុនតូចនិងមធ្យម',
                'ក្រសួងពាក់ព័ន្ធ',
            ]
        ];

        for ($i = 0; $i < 3; $i++) {
            DB::table('b121nf0_regulations')->insert([
                'title_en' => $titles['en'][$i],
                'title_kh' => $titles['kh'][$i],
                'thump' => '',
                'name' => str_replace(str_split('\\/:*?"<>|& '), '-', strtolower($titles['en'][$i])),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]);
        }
    }
}
