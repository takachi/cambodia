<?php

use Illuminate\Database\Seeder;

class CategoryDirectoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $catId = [19, 19, 19, 19, 20, 20, 20, 21, 21, 22, 22, 23, 23, 23, 25, 25, 25, 25, 25];

        $directoryId = [9, 39, 40, 53, 11, 14, 15, 39, 40, 15, 38, 1, 25, 81, 70, 57, 74, 6, 8];

        for ($i = 0; $i < 19; $i++) {
            DB::table('b121nf0_category_directories')->insert([
                'category_id' => $catId[$i],
                'directory_id' => $directoryId[$i],
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]);
        }
        echo "19 category and directory relationship created \n";
    }
}
