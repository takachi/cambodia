<?php

use Illuminate\Database\Seeder;

class RalatedMinistryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $logos = [
            'uploads/General-Department-of-Taxation.jpg',
            'uploads/Institute-of-Standards-of-Cambodia.jpg',
            'uploads/Ministry-of-Agriculture-forestry-and-Fisheries.jpg',
            'uploads/Ministry-of-Commerce.jpg',
            'uploads/Ministry-of-Economy-and-Finance.jpg',
            'uploads/Ministry-of-Industry-and-Handicraft.jpg',
            'uploads/Ministry-of-Labor-and-Vocational-Training.jpg',
            'uploads/Ministry-of-Tourism.jpg',
            'uploads/Phnom-Penh-Municipality.jpg',

        ];

        $titles = [
            'en' => [
                'General Department of Taxation',
                'Institute of Standards of Cambodia',
                'Ministry of Agriculture forestry and Fisheries',
                'Ministry of Commerce',
                'Ministry of Economy and Finance',
                'Ministry of Industry and Handicraft',
                'Ministry of Labor and Vocational Training',
                'Ministry of Tourism',
                'Phnom Penh Municipality',
            ],
            'kh' => [
                'អគ្គនាយកដ្ឋានពន្ធដារ នៃក្រសួងសេដ្ឋកិច្ច និងហិរញ្ញវត្ថុ',
                'វិទ្យាស្ថានស្តង់ដាកម្ពុជា',
                'ក្រសួងកសិកម្ម រុក្ខាប្រមាញ់ និង​នេសាទ',
                'ក្រសួង​ពាណិជ្ជកម្ម​',
                'ក្រសួងសេដ្ឋកិច្ច និងហិរញ្ញវត្ថុ',
                'ក្រសួងឧស្សាហកម្ម និងសិប្បកម្ម',
                'ក្រសួងការងារ និងបណ្តុះបណ្តាលវិជ្ជាជីវៈ',
                'ក្រសួងទេសចរណ៍កម្ពុជា',
                'សាលារាជធានីភ្នំពេញ',
            ]
        ];

        $websites = [
            'http://www.tax.gov.kh/',
            'http://www.isc.gov.kh/en',
            'http://www.maff.gov.kh/',
            'http://www.moc.gov.kh/en-us/',
            'http://www.mef.gov.kh/',
            'http://www.mih.gov.kh/',
            'https://www.mlvt.gov.kh/index.php/km/',
            'http://www.tourismcambodia.org/',
            'http://phnompenh.gov.kh/',
        ];

        $regulation_id = \App\Models\Regulation::pluck('id')->last();

        for ($i = 0; $i < 9; $i++) {
            DB::table('b121nf0_related_ministries')->insert([
                'logo' => $logos[$i],
                'title_en' => $titles['en'][$i],
                'title_kh' => $titles['kh'][$i],
                'website' => $websites[$i],
                'uri' => str_replace(str_split('\\/:*?"<>|& '), '-', strtolower($titles['en'][$i])),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'regulation_id' => $regulation_id
            ]);
        }

        echo "9 Related Ministries created \n";
    }
}
