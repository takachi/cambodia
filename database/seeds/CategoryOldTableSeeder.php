<?php

use Illuminate\Database\Seeder;

class CategoryOldTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $titles = [
            'en' => [
                'Regulation',
                'Business Guides',
                'Service',
                'Directory',
                'Financing',
                'Insight',
                'Opportunity',
                'News & Events',
                'Industry Events',
                'Upcoming News Events',
                'BIC News & Event',
                'Job Announcement',
                'Bidding Project',
                'Project Announcement',
                'Publication',
                'Statistic',
                'SME Loan',
                'General Insurance',
                'Business Development',
                'Accounting & Finance',
                'Human Resource',
                'Legal',
                'Sale & Marketing',
                'Operation',
                'Technology',
                'SME Policy & Frameworks',
                'SME Licence Mapping',
                'Related Ministry',
            ],
            'kh' => [
                'ផ្នែកបទបញ្ញាត្តិ',
                'ព័ត៌មាន​ែណនាំគ្រប់គ្រងអាជីវកម្ម',
                'ផ្នែកសេវាកម្ម',
                'ព័ត៌មានម្ចាស់អាជីវកម្ម',
                'ហិរញ្ញប្បទាន',
                'ស្ថិតិ​​ និង​ចំណេះដឹង',
                'ឱកាសធុរះកិច្ច',
                'ព័ត៌មាន និងព្រឹតិ្តការណ៍',
                'ព្រឹត្តិការណ៍ឧស្សាហកម្ម',
                'ព័ត៌មាន និងព្រឹតិ្តការណ៍ថ្មីៗបន្ទាប់',
                'ព័ត៌មាន និងព្រឹតិ្តការណ៍របស់ BIC',
                'ការប្រកាសជ្រើសរើស បុគ្គលិក',
                'ការប្រកួតប្រជែងគំរោង',
                'ការប្រកាសព័តមាន អំពីគំរោងផ្សេងៗ',
                'ការបោះពុម្ពផ្សាយ',
                'ស្ថិតិ',
                'កម្ចីសំរាប់សហគ្រាសធុនតូច និងមធ្យម',
                'ព័ត៌មានអំពី ធានារ៉ាប់រង',
                'ការពង្រីកអាជីវកម្ម',
                'គណនេយ្យ និងហិរញ្ញវត្ថុ',
                'ធនធានមនុស្ស',
                'ច្បាប់',
                'ការលក់ និងផ្សព្វផ្សាយ',
                'ប្រតិបត្តិការ',
                'បច្ចេកវិទ្យា',
                'គោលនយោបាយ និងផែនការយុទ្ធសាស្រ្ត របស់សហគ្រាសធុនតូច និងមធ្យម',
                'លក្ខខ័ណ្ឌ និងនីតិវិធីសំរាប់ការចុះបញ្ជីអាជីវកម្ម​របស់សហគ្រាសធុនតូចនិងមធ្យម',
                'ក្រសួងពាក់ព័ន្ធ',
            ]
        ];

        $names = [
            'regulation',
            'business-guides',
            'service',
            'directory',
            'financing',
            'insight',
            'opportunity',
            'news-events',
            'industry-events',
            'upcoming-news-events',
            'bic-news-events',
            'job-announcement',
            'bidding-project',
            'project-announcement',
            'publication',
            'statistic',
            'sme-loan',
            'general-insurance',
            'business-development',
            'accounting-finance',
            'human-resource',
            'legal',
            'sale-marketing',
            'operation',
            'technology',
            'cambodia-industry-development-policy',
            'sme-license-mapping',
            'related-ministry',
        ];

        $thumbs = [
            ''
        ];

        $ordering = [0, 1, 0, 0, 2, 3, 4, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

        $parent = [0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 8, 7, 7, 7, 6, 6, 5, 5, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1];

        $images = [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 253, 249, 250, 251, 252, 248, 254, 0, 0, 0];

        $langingPages = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, -1];

        for ($i = 0; $i < 28; $i++) {
            DB::table('b121nf0_category')->insert([
                'title_en' => $titles['en'][$i],
                'title_km' => $titles['kh'][$i],
                'status_en' => 'active',
                'status_kh' => '',
                'image' => $images[$i],
                'landing_page' => $langingPages[$i],
                'menu_order' => $ordering[$i],
                'parent' => $parent[$i],
                'name' => $names[$i],
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]);
        }
        echo "28 category created \n";
    }

}
