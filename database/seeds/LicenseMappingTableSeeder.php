<?php

use App\Models\LicenseMapping;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LicenseMappingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $titles = [
            'en' => [
                "Wholesale or Retail Business Selling Agricultural Input Suppliers",
                "Bakery Products Business",
                "Grain Milling Business",
                "Guesthouse Business",
                "Hotel Business",
                "Factory/Handicraft Produced Fish Sauces, Fish Meat Processing, Meat Ball and Sausage Business",
                "Restaurant Business",
                "Factory/handicraft Produced Cunning and Preserving of Fruit and Vegetable Business",
                "Tour Operator and Travel Agency Business",
                "Water Purify and Non Alcohol",
                "Wholesale/Retail Business",
            ],
            'kh' => [
                "អាជីវកម្មលក់ដុំ លក់រាយលើការផ្គត់ផ្គង់វត្ថុធាតុដើមកសិកម្ម",
                "អាជីវកម្មផលិតនំប៉័ង និងផលិតផលពាក់ព័ន្ធ",
                "អាជីវកម្ម កិនស្រូវ ម្សៅ ដំឡួង កាហ្វេ តែ និងមី",
                "អាជីវកម្មផ្ទះភ្ញៀវ",
                "អាជីវកម្មសណ្ឋាគារ",
                "អាជីវកម្មផលិតទឹកត្រី កែច្នៃសាច់ត្រី ប្រហិត សាច់ក្រក",
                "អាជីវកម្មភោជនីយដ្ឋាន",
                "អាជីវកម្ម ផលិតទឹកសណ្តែក ទឹកម្ទេស ទឹកប៉េងប៉ោះ",
                "អាជីវកម្មការីទេសចរណ៍  ភ្នាក់ងារទេសចរណ៍",
                "អាជីវកម្មផលិតទឹកសុទ្ធ ស្រា ភេសជ្ជៈមិនមានជាតិអាល់កុល",
                "អាជីវកម្មលក់ដុំ លក់រាយ ",
            ]
        ];

        $thumbnail = [
            'uploads/agricultural-input.jpg',
            'uploads/bakery.jpg',
            'uploads/grain-milling.jpg',
            'uploads/guest-house.jpg',
            'uploads/hotel.jpg',
            'uploads/meat-ball.jpg',
            'uploads/restaurant.jpg',
            'uploads/soy-chilly.jpg',
            'uploads/tour-operator.jpg',
            'uploads/water-purify-and-non-alcohol.jpg',
            'uploads/wholesale.jpg'
        ];

        $primaryLicenseTitleEn = 'Primary Licenses & Certificates';
        $primaryLicenseTitleKh = 'អាជ្ញាបណ្ណ និងវិញ្ញាបនប័ត្រសំខាន់ៗ';
        $primaryLicenseSloganEn = '- Before Operations -';
        $primaryLicenseSloganKh = '-មុនដំណើរការអាជីវកម្ម-';

        $otherLicenseTitleEn = 'Other Licenses & Certificates';
        $otherLicenseTitleKh = 'អាជ្ញាបណ្ណ និងវិញ្ញាបនប័ត្រ ផ្សេងៗ';
        $otherLicenseSloganEn = '- During Operations -';
        $otherLicenseSloganKh = '-ពេលដំណើរការអាជីវកម្ម-';

        $applicationProcessTitleEn = 'Inspections during Application Process';
        $applicationProcessTitleKh = 'ការត្រួតពិនិត្យពេលដំណើរការ ស្នើសុំ';
        $applicationProcessSloganEn = '';
        $applicationProcessSloganKh = '';

        $businessOperationTitleEn = 'Inspections during Business Operation';
        $businessOperationTitleKh = 'ការត្រួតពិនិត្យពេលដំណើរការអាជីវកម្ម';
        $businessOperationSloganEn = '';
        $businessOperationSloganKh = '';

        $primaryLicenseItems = [
            [//0
                'title' => [
                    'en' => [
                        "Ministry of Commerce",
                        "General Tax Department",
                        "Ministry of Agriculture, Forestry and Fisheries",
                        "Municipality",
                    ],
                    'kh' => [
                        "ក្រសួងពាណិជ្ជកម្ម",
                        "អគ្គនាយកដ្ឋានពន្ធដារ",
                        "ក្រសួង កសិកម្ម រុក្ខាប្រមាញ់ និងនេសាទ",
                        "សាលារាជធានីភ្នំពេញ",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Certificate of Incorporation (business registration)",
                        "Tax Registration for Patent Tax VAT Number and Registration Tax",
                        "License for Selling Agricultural Input Supplier",
                        "Location Approval License",
                    ],
                    'kh' => [
                        "វិញ្ញាបនប័ត្របញ្ជាក់ការចុះឈ្មោះក្នុងបញ្ជីពាណិជ្ជកម្ម",
                        "ការចុះបញ្ជីពន្ធដារ អាករបន្ថែម បណ្ណពន្ធប៉ាតង់ និងពន្ធប្រថាប់ត្រា",
                        "អាជ្ញាបណ្ណអាជីវកម្មលក់ដុំ លក់រាយលើការផ្គត់ផ្គង់វត្ថុធាតុដើមកសិកម្ម",
                        "លិខិតអនុញ្ញាត(បើកទីតាំងអាជីវកម្ម)",
                    ]
                ]
            ],
            [//1
                'title' => [
                    'en' => [
                        "Ministry of Commerce",
                        "General Tax Department",
                        "Ministry of Industry and Handicraft",
                        "Municipality",
                    ],
                    'kh' => [
                        "ក្រសួងពាណិជ្ជកម្ម",
                        "អគ្គនាយកដ្ឋានពន្ធដារ",
                        "ក្រសួងឧស្សាហកម្ម និងសិប្បកម្ម",
                        "សាលារាជធានីភ្នំពេញ",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Certificate of Incorporation (business registration)",
                        "Tax Registration for Patent Tax, VAT Number and Registration Tax",
                        "-Permit on Small and Medium Factory & Handicraft Establishment -Operating License on Small and Medium Factory and Handicraft",
                        "Location Approval License",
                    ],
                    'kh' => [
                        " ិញ្ញាបនប័ត្របញ្ជាក់ការចុះឈ្មោះក្នុងបញ្ជីពាណិជ្ជកម្ម",
                        "ការចុះបញ្ជីពន្ធដារ អាករបន្ថែម  បណ្ណពន្ធប៉ាតង់ និងពន្ធប្រថាប់ត្រា",
                        " - ប្រកាសបង្កើតរោងចក្រធុនតូច មធ្យម និងសិប្បកម្ម - វិញ្ញាបនប័ត្រដំណើការរោងចក្រធុនតូច មធ្យម និងសិប្បកម្ម",
                        "លិខិតអនុញ្ញាត​(បើកទីតាំងអាជីវកម្ម)",
                    ]
                ]
            ],
            [//2
                'title' => [
                    'en' => [
                        "Ministry of Commerce",
                        "General Tax Department",
                        "Ministry of Industry and Handicraft",
                        "Municipality",
                    ],
                    'kh' => [
                        "ក្រសួងពាណិជ្ជកម្ម",
                        "អគ្គនាយកដ្ឋានពន្ធដារ",
                        "ក្រសួងឧស្សាហកម្ម និងសិប្បកម្ម",
                        "សាលារាជធានីភ្នំពេញ",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Certificate of Incorporation (business registration)",
                        "Tax Registration for Patent Tax, VAT Number and Registration Tax",
                        "-Permit on Small and Medium Factory & Handicraft Establishment -Operating License on Small and Medium Factory and Handicraft",
                        "Location Approval License",
                    ],
                    'kh' => [
                        " វិញ្ញាបនប័ត្របញ្ជាក់ការចុះឈ្មោះក្នុងបញ្ជីពាណិជ្ជកម្ម",
                        "ការចុះបញ្ជីពន្ធដារ អាករបន្ថែម  បណ្ណពន្ធប៉ាតង់ និងពន្ធប្រថាប់ត្រា",
                        "- ប្រកាសបង្កើតរោងចក្រធុនតូច មធ្យម និងសិប្បកម្ម - វិញ្ញាបនប័ត្រដំណើការរោងចក្រធុនតូច មធ្យម និងសិប្បកម្ម",
                        "លិខិតអនុញ្ញាត​(បើកទីតាំងអាជីវកម្ម)",
                    ]
                ]
            ],


            [//3 => 4
                'title' => [
                    'en' => [
                        "Ministry of Commerce",
                        "General Tax Department",
                        "Ministry of Tourism",
                        "Municipality",
                    ],
                    'kh' => [
                        "ក្រសួងពាណិជ្ជកម្ម",
                        "អគ្គនាយកដ្ឋានពន្ធដារ",
                        "ក្រសួងទេសចរណ៍",
                        "សាលារាជធានីភ្នំពេញ",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Certificate of Incorporation (business registration)",
                        "Tax Registration for Patent Tax, VAT Number and Registration Tax",
                        "Tourism License for Guesthouse",
                        "Location Approval License",
                    ],
                    'kh' => [
                        " វិញ្ញាបនប័ត្របញ្ជាក់ការចុះឈ្មោះក្នុងបញ្ជីពាណិជ្ជកម្ម ",
                        "ការចុះបញ្ជីពន្ធដារ អាករបន្ថែម  បណ្ណពន្ធប៉ាតង់ និងពន្ធប្រថាប់ត្រា",
                        " អាជ្ញាបណ្ណសម្រាប់ដំណើការអាជីវកម្មផ្ទះភ្ញៀវ",
                        "លិខិតអនុញ្ញាត​(បើកទីតាំងអាជីវកម្ម)",
                    ]
                ]
            ],
            [//4
                'title' => [
                    'en' => [
                        "Ministry of Commerce",
                        "General Tax Department",
                        "Ministry of Tourism",
                        "Municipality",
                    ],
                    'kh' => [
                        "ក្រសួងពាណិជ្ជកម្ម",
                        "អគ្គនាយកដ្ឋានពន្ធដារ",
                        "ក្រសួងទេសចរណ៍",
                        "សាលារាជធានីភ្នំពេញ",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Certificate of Incorporation (business registration)",
                        "Tax Registration for Patent Tax, VAT Number and Registration Tax",
                        "Tourism License for Hotel",
                        "Location Approval License",
                    ],
                    'kh' => [
                        " វិញ្ញាបនប័ត្របញ្ជាក់ការចុះឈ្មោះក្នុងបញ្ជីពាណិជ្ជកម្ម",
                        "ការចុះបញ្ជីពន្ធដារ អាករបន្ថែម  បណ្ណពន្ធប៉ាតង់ និងពន្ធប្រថាប់ត្រា",
                        "អាជ្ញាបណ្ណសម្រាប់ដំណើការអាជីវកម្មសណ្ឋាគារ",
                        "លិខិតអនុញ្ញាត​(បើកទីតាំងអាជីវកម្ម)",
                    ]
                ]
            ],
            [//5
                'title' => [
                    'en' => [
                        "Ministry of Commerce",
                        "General Tax Department",
                        "Ministry of Industry and Handicraft",
                        "Municipality",
                    ],
                    'kh' => [
                        "ក្រសួងពាណិជ្ជកម្ម",
                        "អគ្គនាយកដ្ឋានពន្ធដារ",
                        "ក្រសួងឧស្សាហកម្ម និងសិប្បកម្ម",
                        "សាលារាជធានីភ្នំពេញ",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Certificate of Incorporation (business registration)",
                        "Tax Registration for Patent Tax, VAT Number and Registration Tax",
                        "-Permit on Small and Medium Factory & Handicraft Establishment -Operating License on Small and Medium Factory and Handicraft",
                        "Location Approval License",
                    ],
                    'kh' => [
                        " វិញ្ញាបនប័ត្របញ្ជាក់ការចុះឈ្មោះក្នុងបញ្ជីពាណិជ្ជកម្ម     ",
                        "ការចុះបញ្ជីពន្ធដារ អាករបន្ថែម  បណ្ណពន្ធប៉ាតង់ និងពន្ធប្រថាប់ត្រា",
                        "- ប្រកាសបង្កើតរោងចក្រធុនតូច មធ្យម និងសិប្បកម្ម - វិញ្ញាបនប័ត្រដំណើការរោងចក្រធុនតូច មធ្យម និងសិប្បកម្ម",
                        "លិខិតអនុញ្ញាត​(បើកទីតាំងអាជីវកម្ម)",
                    ]
                ]
            ],
            [//6
                'title' => [
                    'en' => [
                        "Ministry of Commerce",
                        "General Tax Department",
                        "Ministry of Industry and Handicraft",
                        "Municipality",
                    ],
                    'kh' => [
                        "ក្រសួងពាណិជ្ជកម្ម",
                        "អគ្គនាយកដ្ឋានពន្ធដារ",
                        "ក្រសួងទេសចរណ៍",
                        "សាលារាជធានីភ្នំពេញ",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Certificate of Incorporation (business registration)",
                        "Tax Registration for Patent Tax, VAT Number and Registration Tax",
                        "-Permit on Small and Medium Factory & Handicraft Establishment -Operating License on Small and Medium Factory and Handicraft",
                        "Location Approval License",
                    ],
                    'kh' => [
                        " វិញ្ញាបនប័ត្របញ្ជាក់ការចុះឈ្មោះក្នុងបញ្ជីពាណិជ្ជកម្ម ",
                        "ការចុះបញ្ជីពន្ធដារ អាករបន្ថែម  បណ្ណពន្ធប៉ាតង់ និងពន្ធប្រថាប់ត្រា",
                        " អាជ្ញាបណ្ណសម្រាប់ដំណើការអាជីវកម្មភោជនីយដ្ឋាន",
                        "លិខិតអនុញ្ញាត​(បើកទីតាំងអាជីវកម្ម)",
                    ]
                ]
            ],
            [//7
                'title' => [
                    'en' => [
                        "Ministry of Commerce",
                        "General Tax Department",
                        "Ministry of Tourism",
                        "Municipality",
                    ],
                    'kh' => [
                        "ក្រសួងពាណិជ្ជកម្ម",
                        "អគ្គនាយកដ្ឋានពន្ធដារ",
                        "ក្រសួងឧស្សាហកម្ម និងសិប្បកម្ម",
                        "សាលារាជធានីភ្នំពេញ",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Certificate of Incorporation (business registration)",
                        "Tax Registration for Patent Tax, VAT Number and Registration Tax",
                        "Tourism License for Restaurant",
                        "Location Approval License",
                    ],
                    'kh' => [
                        " វិញ្ញាបនប័ត្របញ្ជាក់ការចុះឈ្មោះក្នុងបញ្ជីពាណិជ្ជកម្ម     ",
                        "ការចុះបញ្ជីពន្ធដារ អាករបន្ថែម  បណ្ណពន្ធប៉ាតង់ និងពន្ធប្រថាប់ត្រា",
                        "- ប្រកាសបង្កើតរោងចក្រធុនតូច មធ្យម និងសិប្បកម្ម - វិញ្ញាបនប័ត្រដំណើការរោងចក្រធុនតូច មធ្យម និងសិប្បកម្ម",
                        "លិខិតអនុញ្ញាត​(បើកទីតាំងអាជីវកម្ម)",
                    ]
                ]
            ],
            [//8
                'title' => [
                    'en' => [
                        "Ministry of Commerce",
                        "General Tax Department",
                        "Ministry of Industry and Handicraft",
                        "Municipality",
                    ],
                    'kh' => [
                        "ក្រសួងពាណិជ្ជកម្ម",
                        "អគ្គនាយកដ្ឋានពន្ធដារ",
                        "ក្រសួងទេសចរណ៍",
                        "សាលារាជធានីភ្នំពេញ",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Certificate of Incorporation (business registration)",
                        "Tax Registration for Patent Tax, VAT Number and Registration Tax",
                        "-Permit on Small and Medium Factory & Handicraft Establishment -Operating License on Small and Medium Factory and Handicraft",
                        "Location Approval License",
                    ],
                    'kh' => [
                        " វិញ្ញាបនប័ត្របញ្ជាក់ការចុះឈ្មោះក្នុងបញ្ជីពាណិជ្ជកម្ម ",
                        "ការចុះបញ្ជីពន្ធដារ អាករបន្ថែម  បណ្ណពន្ធប៉ាតង់ និងពន្ធប្រថាប់ត្រា",
                        "អាជ្ញាបណ្ណសម្រាប់ដំណើការអាជីវកម្មការីទេសចរណ៍ថ្មី  និងភ្នាក់ងារទេសចរណ៍ថ្មី",
                        "លិខិតអនុញ្ញាត​(បើកទីតាំងអាជីវកម្ម)",
                    ]
                ]
            ],
            [//9
                'title' => [
                    'en' => [
                        "Ministry of Commerce",
                        "General Tax Department",
                        "Ministry of Tourism",
                        "Municipality",
                    ],
                    'kh' => [
                        "ក្រសួងពាណិជ្ជកម្ម",
                        "អគ្គនាយកដ្ឋានពន្ធដារ",
                        "ក្រសួងឧស្សាហកម្ម និងសិប្បកម្ម",
                        "សាលារាជធានីភ្នំពេញ",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Certificate of Incorporation (business registration)",
                        "Tax Registration for Patent Tax, VAT Number and Registration Tax",
                        "Tourism License for Tour Operator and Travel Agency",
                        "Location Approval License",
                    ],
                    'kh' => [
                        " វិញ្ញាបនប័ត្របញ្ជាក់ការចុះឈ្មោះក្នុងបញ្ជីពាណិជ្ជកម្ម     ",
                        "ការចុះបញ្ជីពន្ធដារ អាករបន្ថែម  បណ្ណពន្ធប៉ាតង់ និងពន្ធប្រថាប់ត្រា",
                        "- ប្រកាសបង្កើតរោងចក្រធុនតូច មធ្យម និងសិប្បកម្ម - វិញ្ញាបនប័ត្រដំណើការរោងចក្រធុនតូច មធ្យម និងសិប្បកម្ម",
                        "លិខិតអនុញ្ញាត​(បើកទីតាំងអាជីវកម្ម)",
                    ]
                ]
            ],
            [//10
                'title' => [
                    'en' => [
                        "Ministry of Commerce",
                        "General Tax Department",
                        "Municipality",
                    ],
                    'kh' => [
                        "ក្រសួងពាណិជ្ជកម្ម",
                        "អគ្គនាយកដ្ឋានពន្ធដារ",
                        "សាលារាជធានីភ្នំពេញ",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Certificate of Incorporation (business registration)",
                        "Tax Registration for Patent Tax, VAT Number and Registration Tax",
                        "Location Approval License",
                    ],
                    'kh' => [
                        " វិញ្ញាបនប័ត្របញ្ជាក់ការចុះឈ្មោះក្នុងបញ្ជីពាណិជ្ជកម្ម ",
                        "ការចុះបញ្ជីពន្ធដារ អាករបន្ថែម  បណ្ណពន្ធប៉ាតង់ និងពន្ធប្រថាប់ត្រា",
                        "លិខិតអនុញ្ញាត​(បើកទីតាំងអាជីវកម្ម)",
                    ]
                ]
            ],
        ];

        $otherLicenseItems = [
            [//0
                'title' => [
                    'en' => [
                        "Ministry of Labor and Vocational Training",
                        "General Department of Taxation",
                        "Phnom Penh Municipality",
                        "",
                    ],
                    'kh' => [
                        "ក្រសួងការងារ និងបណ្តុះបណ្តាលវិជ្ជាជីវៈ",
                        "អគ្គនាយកដ្ឋានពន្ធដារ",
                        "សាលារាជធានីភ្នំពេញ",
                        "",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Registration for Opening Enterprise, Establishment Ledger, Payroll & Internal Work Rule (within 30 days).",
                        "Tax on Sign Board",
                        "Permit on Sign Board",
                        "",
                    ],
                    'kh' => [
                        "ចុះបញ្ជិការប្រកាសបើកសហគ្រាស ចុះបញ្ជិកាសៀវភៅបញ្ជីប្រចាំគ្រឹះស្ថាន ចុះបញ្ជិកាសៀវភៅបើកប្រាក់ ចុះទិដ្ឋាកាបទបញ្ជាផ្ទៃក្នុង (ត្រូវជូនដំណឹងដល់ក្រសួងការងារ និងបណ្តុះបណ្តាលវិជ្ជាជីវៈស្តីពីការចាប់ដំណើរការ និងជួលបុគ្គលិក ក្នុងកំឡុងពេល៣០ថ្ងៃ)",
                        "ពន្ធលើស្លាកយីហោ",
                        "លិខិតអនុញ្ញាតស្លាកយីហោ",
                        "",
                    ]
                ]
            ],
            [//1
                'title' => [
                    'en' => [
                        "Ministry of Labor and Vocational Training",
                        "Institute of Standards of Cambodia",
                        "Ministry of Commerce",
                        "Phnom Penh Municipality",
                    ],
                    'kh' => [
                        "ក្រសួងការងារ និងបណ្តុះបណ្តាលវិជ្ជាជីវៈ",
                        "វិទ្យាស្ថានស្តង់ដារកម្ពុជា ",
                        "ក្រសួងពាណិជ្ជកម្ម",
                        "សាលារាជធានីភ្នំពេញ",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Registration for Opening Enterprise, Establishment Ledger, Payroll & Internal Work Rule (within 30 days).",
                        "The Use of Product Certification Mark ចបផ",
                        "Trade Mark Registration (Optional)",
                        "Permit on Sign Board",
                    ],
                    'kh' => [
                        "ចុះបញ្ជិការប្រកាសបើកសហគ្រាស ចុះបញ្ជិកាសៀវភៅបញ្ជីប្រចាំគ្រឹះស្ថាន ចុះបញ្ជិកាសៀវភៅបើកប្រាក់ ចុះទិដ្ឋាកាបទបញ្ជាផ្ទៃក្នុង (ត្រូវជូនដំណឹងដល់ក្រសួងការងារ និងបណ្តុះបណ្តាលវិជ្ជាជីវៈស្តីពីការចាប់ដំណើរការ និងជួលបុគ្គលិក ក្នុងកំឡុងពេល៣០ថ្ងៃ)",
                        "អាជ្ញាបណ្ណចុះបញ្ជីផលិតផល ប្រើសញ្ញាស្តង់ដារកម្ពុជា (ច ប ផ)",
                        "វិញ្ញាបនបត្រចុះបញ្ជីពាណិជ្ជសញ្ញា  (តាមការស្ម័គ្រចិត្ត)",
                        "លិខិតអនុញ្ញាតស្លាកយីហោ",
                    ]
                ]
            ],
            [//2
                'title' => [
                    'en' => [
                        "Ministry of Labor and Vocational Training",
                        "Institute of Standards of Cambodia",
                        "Ministry of Commerce",
                        "Phnom Penh Municipality",
                    ],
                    'kh' => [
                        "ក្រសួងការងារ និងបណ្តុះបណ្តាលវិជ្ជាជីវៈ",
                        "វិទ្យាស្ថានស្តង់ដារកម្ពុជា ",
                        "ក្រសួងពាណិជ្ជកម្ម",
                        "សាលារាជធានីភ្នំពេញ",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Registration for Opening Enterprise, Establishment Ledger, Payroll & Internal Work Rule (within 30 days).",
                        "The Use of Product Certification Mark ចបផ",
                        "Trade Mark Registration (Optional)",
                        "Permit on Sign Board",
                    ],
                    'kh' => [
                        "ចុះបញ្ជិការប្រកាសបើកសហគ្រាស ចុះបញ្ជិកាសៀវភៅបញ្ជីប្រចាំគ្រឹះស្ថាន ចុះបញ្ជិកាសៀវភៅបើកប្រាក់ ចុះទិដ្ឋាកាបទបញ្ជាផ្ទៃក្នុង (ត្រូវជូនដំណឹងដល់ក្រសួងការងារ និងបណ្តុះបណ្តាលវិជ្ជាជីវៈស្តីពីការចាប់ដំណើរការ និងជួលបុគ្គលិក ក្នុងកំឡុងពេល៣០ថ្ងៃ)",
                        "អាជ្ញាបណ្ណចុះបញ្ជីផលិតផល ប្រើសញ្ញាស្តង់ដារកម្ពុជា (ច ប ផ)",
                        "វិញ្ញាបនបត្រចុះបញ្ជីពាណិជ្ជសញ្ញា  (តាមការស្ម័គ្រចិត្ត)",
                        "លិខិតអនុញ្ញាតស្លាកយីហោ",
                    ]
                ]
            ],
            [//3
                'title' => [
                    'en' => [
                        "Ministry of Labor and Vocational Training",
                        "Institute of Standards of Cambodia",
                        "Phnom Penh Municipality",
                        "",
                    ],
                    'kh' => [
                        "ក្រសួងការងារ និងបណ្តុះបណ្តាលវិជ្ជាជីវៈ",
                        "អគ្គនាយកដ្ឋានពន្ធដារ",
                        "សាលារាជធានីភ្នំពេញ",
                        "",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Registration for Opening Enterprise, Establishment Ledger, Payroll & Internal Work Rule (within 30 days).",
                        "The Use of Product Certification Mark ចបផ",
                        "Permit on Sign Board",
                        "",
                    ],
                    'kh' => [
                        "ចុះបញ្ជិការប្រកាសបើកសហគ្រាស ចុះបញ្ជិកាសៀវភៅបញ្ជីប្រចាំគ្រឹះស្ថាន ចុះបញ្ជិកាសៀវភៅបើកប្រាក់ ចុះទិដ្ឋាកាបទបញ្ជាផ្ទៃក្នុង (ត្រូវជូនដំណឹងដល់ក្រសួងការងារ និងបណ្តុះបណ្តាលវិជ្ជាជីវៈស្តីពីការចាប់ដំណើរការ និងជួលបុគ្គលិក ក្នុងកំឡុងពេល៣០ថ្ងៃ)",
                        "ពន្ធលើស្លាកយីហោ",
                        "លិខិតអនុញ្ញាតស្លាកយីហោ",
                        "",
                    ]
                ]
            ],
            [//4
                'title' => [
                    'en' => [
                        "Ministry of Labor and Vocational Training",
                        "General Department of Taxation",
                        "",
                        "",
                    ],
                    'kh' => [
                        "ក្រសួងការងារ និងបណ្តុះបណ្តាលវិជ្ជាជីវៈ",
                        "អគ្គនាយកដ្ឋានពន្ធដារ",
                        "សាលារាជធានីភ្នំពេញ",
                        "",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Registration for Opening Enterprise, Establishment Ledger, Payroll & Internal Work Rule (within 30 days).",
                        "Tax on Sign Board",
                        "",
                        "",
                    ],
                    'kh' => [
                        "ចុះបញ្ជិការប្រកាសបើកសហគ្រាស ចុះបញ្ជិកាសៀវភៅបញ្ជីប្រចាំគ្រឹះស្ថាន ចុះបញ្ជិកាសៀវភៅបើកប្រាក់ ចុះទិដ្ឋាកាបទបញ្ជាផ្ទៃក្នុង (ត្រូវជូនដំណឹងដល់ក្រសួងការងារ និងបណ្តុះបណ្តាលវិជ្ជាជីវៈស្តីពីការចាប់ដំណើរការ និងជួលបុគ្គលិក ក្នុងកំឡុងពេល៣០ថ្ងៃ)",
                        "ពន្ធលើស្លាកយីហោ",
                        "លិខិតអនុញ្ញាតស្លាកយីហោ",
                        "",
                    ]
                ]
            ],
            [//5
                'title' => [
                    'en' => [
                        "Ministry of Labor and Vocational Training",
                        "Institute of Standards of Cambodia",
                        "Institute of Standards of Cambodia",
                        "Phnom Penh Municipality",
                    ],
                    'kh' => [
                        "ក្រសួងការងារ និងបណ្តុះបណ្តាលវិជ្ជាជីវៈ",
                        "វិទ្យាស្ថានស្តង់ដារកម្ពុជា ",
                        "ក្រសួងពាណិជ្ជកម្ម",
                        "សាលារាជធានីភ្នំពេញ",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Registration for Opening Enterprise, Establishment Ledger, Payroll & Internal Work Rule (within 30 days).",
                        "The Use of Product Certification Mark ចបផ",
                        "The Use of Product Certification Mark CS",
                        "Permit on Sign Board",
                    ],
                    'kh' => [
                        "ចុះបញ្ជិការប្រកាសបើកសហគ្រាស ចុះបញ្ជិកាសៀវភៅបញ្ជីប្រចាំគ្រឹះស្ថាន ចុះបញ្ជិកាសៀវភៅបើកប្រាក់ ចុះទិដ្ឋាកាបទបញ្ជាផ្ទៃក្នុង (ត្រូវជូនដំណឹងដល់ក្រសួងការងារ និងបណ្តុះបណ្តាលវិជ្ជាជីវៈស្តីពីការចាប់ដំណើរការ និងជួលបុគ្គលិក ក្នុងកំឡុងពេល៣០ថ្ងៃ)",
                        "អាជ្ញាបណ្ណចុះបញ្ជីផលិតផល ប្រើសញ្ញាស្តង់ដារកម្ពុជា (ច ប ផ)",
                        "វិញ្ញាបនបត្រចុះបញ្ជីពាណិជ្ជសញ្ញា  (តាមការស្ម័គ្រចិត្ត)",
                        "លិខិតអនុញ្ញាតស្លាកយីហោ",
                    ]
                ]
            ],
            [//6
                'title' => [
                    'en' => [
                        "Ministry of Labor and Vocational Training",
                        "Institute of Standards of Cambodia",
                        "Ministry of Commerce",
                        "Phnom Penh Municipality",
                    ],
                    'kh' => [
                        "ក្រសួងការងារ និងបណ្តុះបណ្តាលវិជ្ជាជីវៈ",
                        "អគ្គនាយកដ្ឋានពន្ធដារ",
                        "សាលារាជធានីភ្នំពេញ",
                        "",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Registration for Opening Enterprise, Establishment Ledger, Payroll & Internal Work Rule (within 30 days).",
                        "The Use of Product Certification Mark ចបផ",
                        "Trade Mark Registration (Optional)",
                        "Permit on Sign Board",
                    ],
                    'kh' => [
                        "ចុះបញ្ជិការប្រកាសបើកសហគ្រាស ចុះបញ្ជិកាសៀវភៅបញ្ជីប្រចាំគ្រឹះស្ថាន ចុះបញ្ជិកាសៀវភៅបើកប្រាក់ ចុះទិដ្ឋាកាបទបញ្ជាផ្ទៃក្នុង (ត្រូវជូនដំណឹងដល់ក្រសួងការងារ និងបណ្តុះបណ្តាលវិជ្ជាជីវៈស្តីពីការចាប់ដំណើរការ និងជួលបុគ្គលិក ក្នុងកំឡុងពេល៣០ថ្ងៃ)",
                        "ពន្ធលើស្លាកយីហោ",
                        "លិខិតអនុញ្ញាតស្លាកយីហោ",
                        "",
                    ]
                ]
            ],
            [//7
                'title' => [
                    'en' => [
                        "Ministry of Labor and Vocational Training",
                        "General Department of Taxation",
                        "Phnom Penh Municipality",
                        "",
                    ],
                    'kh' => [
                        "ក្រសួងការងារ និងបណ្តុះបណ្តាលវិជ្ជាជីវៈ",
                        "វិទ្យាស្ថានស្តង់ដារកម្ពុជា ",
                        "ក្រសួងពាណិជ្ជកម្ម",
                        "សាលារាជធានីភ្នំពេញ",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Establishment Ledger, Payroll & Internal Work Rule (within 30 days).",
                        "Tax on Sign Board",
                        "Permit on Sign Board",
                        "",
                    ],
                    'kh' => [
                        "ចុះបញ្ជិការប្រកាសបើកសហគ្រាស ចុះបញ្ជិកាសៀវភៅបញ្ជីប្រចាំគ្រឹះស្ថាន ចុះបញ្ជិកាសៀវភៅបើកប្រាក់ ចុះទិដ្ឋាកាបទបញ្ជាផ្ទៃក្នុង (ត្រូវជូនដំណឹងដល់ក្រសួងការងារ និងបណ្តុះបណ្តាលវិជ្ជាជីវៈស្តីពីការចាប់ដំណើរការ និងជួលបុគ្គលិក ក្នុងកំឡុងពេល៣០ថ្ងៃ)",
                        "អាជ្ញាបណ្ណចុះបញ្ជីផលិតផល ប្រើសញ្ញាស្តង់ដារកម្ពុជា (ច ប ផ)",
                        "វិញ្ញាបនបត្រចុះបញ្ជីពាណិជ្ជសញ្ញា  (តាមការស្ម័គ្រចិត្ត)",
                        "លិខិតអនុញ្ញាតស្លាកយីហោ",
                    ]
                ]
            ],
            [//8
                'title' => [
                    'en' => [
                        "Ministry of Labor and Vocational Training",
                        "Institute of Standards of Cambodia",
                        "Ministry of Commerce",
                        "Phnom Penh Municipality",
                    ],
                    'kh' => [
                        "ក្រសួងការងារ និងបណ្តុះបណ្តាលវិជ្ជាជីវៈ",
                        "អគ្គនាយកដ្ឋានពន្ធដារ",
                        "សាលារាជធានីភ្នំពេញ",
                        "",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Registration for Opening Enterprise, Establishment Ledger, Payroll & Internal Work Rule (within 30 days).",
                        "The Use of Product Certification Mark ចបផ",
                        "Trade Mark Registration (Optional)",
                        "Permit on Sign Board",
                    ],
                    'kh' => [
                        "ចុះបញ្ជិការប្រកាសបើកសហគ្រាស ចុះបញ្ជិកាសៀវភៅបញ្ជីប្រចាំគ្រឹះស្ថាន ចុះបញ្ជិកាសៀវភៅបើកប្រាក់ ចុះទិដ្ឋាកាបទបញ្ជាផ្ទៃក្នុង (ត្រូវជូនដំណឹងដល់ក្រសួងការងារ និងបណ្តុះបណ្តាលវិជ្ជាជីវៈស្តីពីការចាប់ដំណើរការ និងជួលបុគ្គលិក ក្នុងកំឡុងពេល៣០ថ្ងៃ)",
                        "ពន្ធលើស្លាកយីហោ",
                        "លិខិតអនុញ្ញាតស្លាកយីហោ",
                        "",
                    ]
                ]
            ],
            [//9
                'title' => [
                    'en' => [
                        "Ministry of Labor and Vocational Training",
                        "General Department of Taxation",
                        "Phnom Penh Municipality",
                        "",
                    ],
                    'kh' => [
                        "ក្រសួងការងារ និងបណ្តុះបណ្តាលវិជ្ជាជីវៈ",
                        "វិទ្យាស្ថានស្តង់ដារកម្ពុជា ",
                        "ក្រសួងពាណិជ្ជកម្ម",
                        "សាលារាជធានីភ្នំពេញ",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Registration for Opening Enterprise, Establishment Ledger, Payroll & Internal Work Rule (within 30 days).",
                        "Tax on Sign Board",
                        "Permit on Sign Board",
                        "",
                    ],
                    'kh' => [
                        "ចុះបញ្ជិការប្រកាសបើកសហគ្រាស ចុះបញ្ជិកាសៀវភៅបញ្ជីប្រចាំគ្រឹះស្ថាន ចុះបញ្ជិកាសៀវភៅបើកប្រាក់ ចុះទិដ្ឋាកាបទបញ្ជាផ្ទៃក្នុង (ត្រូវជូនដំណឹងដល់ក្រសួងការងារ និងបណ្តុះបណ្តាលវិជ្ជាជីវៈស្តីពីការចាប់ដំណើរការ និងជួលបុគ្គលិក ក្នុងកំឡុងពេល៣០ថ្ងៃ)",
                        "អាជ្ញាបណ្ណចុះបញ្ជីផលិតផល ប្រើសញ្ញាស្តង់ដារកម្ពុជា (ច ប ផ)",
                        "វិញ្ញាបនបត្រចុះបញ្ជីពាណិជ្ជសញ្ញា  (តាមការស្ម័គ្រចិត្ត)",
                        "លិខិតអនុញ្ញាតស្លាកយីហោ",
                    ]
                ]
            ],
            [//10
                'title' => [
                    'en' => [
                        "Ministry of Labor and Vocational Training",
                        "General Department of Taxation",
                        "Phnom Penh Municipality",
                    ],
                    'kh' => [

                        "ក្រសួងការងារ និងបណ្តុះបណ្តាលវិជ្ជាជីវៈ",
                        "អគ្គនាយកដ្ឋានពន្ធដារ",
                        "សាលារាជធានីភ្នំពេញ",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Registration for Opening Enterprise, Establishment Ledger, Payroll & Internal Work Rule (within 30 days).",
                        "Tax on Sign Board",
                        "Permit on Sign Board"
                    ],
                    'kh' => [
                        "ចុះបញ្ជិការប្រកាសបើកសហគ្រាស ចុះបញ្ជិកាសៀវភៅបញ្ជីប្រចាំគ្រឹះស្ថាន ចុះបញ្ជិកាសៀវភៅបើកប្រាក់ ចុះទិដ្ឋាកាបទបញ្ជាផ្ទៃក្នុង (ត្រូវជូនដំណឹងដល់ក្រសួងការងារ និងបណ្តុះបណ្តាលវិជ្ជាជីវៈស្តីពីការចាប់ដំណើរការ និងជួលបុគ្គលិក ក្នុងកំឡុងពេល៣០ថ្ងៃ)",
                        "ពន្ធលើស្លាកយីហោ",
                        "លិខិតអនុញ្ញាតស្លាកយីហោ",
                    ]
                ]
            ],
        ];

        $applicationProcessItems = [
            [//0
                'title' => [
                    'en' => [
                        "Ministry of Agriculture",
                        "Phnom Penh Municipality",
                        "",
                        "",
                    ],
                    'kh' => [
                        "ក្រសួង កសិកម្ម រុក្ខាប្រមាញ់ និងនេសាទ",
                        "សាលារាជធានីភ្នំពេញ",
                        "",
                        "",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Business approval inspection",
                        "Location Approval Inspection",
                        "",
                        "",
                    ],
                    'kh' => [
                        "ការត្រួតពិនិត្យដោយមន្រ្តីក្រសួង កសិកម្ម រុក្ខាប្រមាញ់ និងនេសាទ",
                        "ការត្រួតពិនិត្យទីតាំង",
                        "",
                        "",
                    ]
                ]
            ],
            [//1
                'title' => [
                    'en' => [
                        "Institute of Standards of Cambodia",
                        "Phnom Penh Municipality",
                        "Ministry of Health",
                        "",
                    ],
                    'kh' => [
                        "វិទ្យាស្ថានស្តង់ដារកម្ពុជា ",
                        "សាលារាជធានីភ្នំពេញ",
                        "ក្រសួងសុខាភិបាល",
                        "",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Product Quality Inspection",
                        "Business Location Inspection",
                        "Hygiene Inspection",
                        "",
                    ],
                    'kh' => [
                        "ការត្រួតពិនិត្យគុណភាពផលិតផល",
                        "ការត្រួតពិនិត្យទីតាំង",
                        "ការត្រួតពិនិត្យ អនាម័យ",
                        "",
                    ]
                ]
            ],
            [//2
                'title' => [
                    'en' => [
                        "Institute of Standards of Cambodia",
                        "Phnom Penh Municipality",
                        "Ministry of Health",
                        "",
                    ],
                    'kh' => [
                        "វិទ្យាស្ថានស្តង់ដារកម្ពុជា ",
                        "សាលារាជធានីភ្នំពេញ",
                        "ក្រសួងសុខាភិបាល",
                        "",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Product Quality Inspection",
                        "Business Location Inspection",
                        "Hygiene Inspection",
                        "",
                    ],
                    'kh' => [
                        "ការត្រួតពិនិត្យគុណភាពផលិតផល",
                        "ការត្រួតពិនិត្យទីតាំង",
                        "ការត្រួតពិនិត្យ អនាម័យ",
                        "",
                    ]
                ]
            ],
            [//3
                'title' => [
                    'en' => [
                        "Ministry of Interior",
                        "Ministry of Health",
                        "Phnom Penh Municipality",
                        "",
                    ],
                    'kh' => [
                        "ក្រសួងមហាផ្ទៃ",
                        "ក្រសួងសុខាភិបាល",
                        "សាលារាជធានីភ្នំពេញ",
                        "",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Fire Safety and Security Inspection",
                        "Hygiene Inspection",
                        "Location Approval Inspection",
                        "",
                    ],
                    'kh' => [
                        " ការត្រួតពិនិត្យសន្តិសុខ និងសុវត្ថិភាពអគ្គីភ័យ",
                        "ការត្រួតពិនិត្យ អនាម័យ",
                        "ការត្រួតពិនិត្យទីតាំង",
                        "",
                    ]
                ]
            ],
            [//4
                'title' => [
                    'en' => [
                        "Ministry of Interior",
                        "Ministry of Health",
                        "Phnom Penh Municipality",
                        "",
                    ],
                    'kh' => [
                        "ក្រសួងមហាផ្ទៃ",
                        "ក្រសួងសុខាភិបាល",
                        "សាលារាជធានីភ្នំពេញ",
                        "",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Fire Safety and Security Inspection",
                        "Hygiene Inspection",
                        "Location Approval Inspection",
                        "",
                    ],
                    'kh' => [
                        "ការត្រួតពិនិត្យសន្តិសុខ និងសុវត្ថិភាពអគ្គីភ័យ",
                        " ការត្រួតពិនិត្យ អនាម័យ",
                        "ការត្រួតពិនិត្យទីតាំង",
                        "",
                    ]
                ]
            ],
            [//5
                'title' => [
                    'en' => [
                        "Institute of Standards of Cambodia",
                        "Phnom Penh Municipality",
                        "Ministry of Health",
                        "",
                    ],
                    'kh' => [
                        "វិទ្យាស្ថានស្តង់ដារកម្ពុជា ",
                        "សាលារាជធានីភ្នំពេញ",
                        "ក្រសួងសុខាភិបាល",
                        "",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Product Quality Inspection",
                        "Business Location Inspection",
                        "Hygiene Inspection",
                        "",
                    ],
                    'kh' => [
                        "ការត្រួតពិនិត្យគុណភាពផលិតផល",
                        "ការត្រួតពិនិត្យទីតាំង",
                        "ការត្រួតពិនិត្យ អនាម័យ",
                        "",
                    ]
                ]
            ],
            [//6
                'title' => [
                    'en' => [
                        "Institute of Standards of Cambodia",
                        "Phnom Penh Municipality",
                        "Ministry of Health",
                        "",
                    ],
                    'kh' => [
                        "វិទ្យាស្ថានស្តង់ដារកម្ពុជា ",
                        "សាលារាជធានីភ្នំពេញ",
                        "ក្រសួងសុខាភិបាល",
                        "",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Product Quality Inspection",
                        "Business Location Inspection",
                        "Hygiene Inspection",
                        "",
                    ],
                    'kh' => [
                        "ការត្រួតពិនិត្យគុណភាពផលិតផល",
                        "ការត្រួតពិនិត្យទីតាំង",
                        "ការត្រួតពិនិត្យ អនាម័យ",
                        "",
                    ]
                ]
            ],[//7
                'title' => [
                    'en' => [
                        "Ministry of Interior",
                        "Ministry of Health",
                        "Phnom Penh Municipality",
                        "",
                    ],
                    'kh' => [
                        "ក្រសួងមហាផ្ទៃ",
                        "ក្រសួងសុខាភិបាល",
                        "សាលារាជធានីភ្នំពេញ",
                        "",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Fire Safety and Security Inspection",
                        "Hygiene Inspection",
                        "Location Approval Inspection",
                        "",
                    ],
                    'kh' => [
                        " ការត្រួតពិនិត្យសន្តិសុខ និងសុវត្ថិភាពអគ្គីភ័យ",
                        "ការត្រួតពិនិត្យ អនាម័យ",
                        "ការត្រួតពិនិត្យទីតាំង",
                        "",
                    ]
                ]
            ],
            [//8
                'title' => [
                    'en' => [
                        "Institute of Standards of Cambodia",
                        "Phnom Penh Municipality",
                        "Ministry of Health",
                        "",
                    ],
                    'kh' => [
                        "វិទ្យាស្ថានស្តង់ដារកម្ពុជា ",
                        "សាលារាជធានីភ្នំពេញ",
                        "ក្រសួងសុខាភិបាល",
                        "",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Product Quality Inspection",
                        "Business Location Inspection",
                        "Hygiene Inspection",
                        "",
                    ],
                    'kh' => [
                        "ការត្រួតពិនិត្យគុណភាពផលិតផល",
                        "ការត្រួតពិនិត្យទីតាំង",
                        "ការត្រួតពិនិត្យ អនាម័យ",
                        "",
                    ]
                ]
            ],[//9
                'title' => [
                    'en' => [
                        "Ministry of Tourism",
                        "Phnom Penh Municipality",
                        "",
                        "",
                    ],
                    'kh' => [
                        "ក្រសួងទេសចរណ៍",
                        "សាលារាជធានីភ្នំពេញ",
                        "",
                        "",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Business Approval Inspection",
                        "Location Approval Inspection",
                        "",
                        "",
                    ],
                    'kh' => [
                        " ការត្រួតពិនិត្យសម្រាប់ដំណើការ អាជីវកម្មការីទេសចរណ៍ថ្មី  និងភ្នាក់ងារទេសចរណ៍ថ្មី",
                        "ការត្រួតពិនិត្យទីតាំង",
                        "",
                        "",
                    ]
                ]
            ],
            [//10
                'title' => [
                    'en' => [
                        "Phnom Penh Municipality",
                        "",
                        "",
                    ],
                    'kh' => [
                        "សាលារាជធានីភ្នំពេញ",
                        "",
                        "",
                    ]
                ],
                'description' => [
                    'en' => [

                        "Location Approval Inspection",
                        "",
                        "",
                    ],
                    'kh' => [
                        "ការត្រួតពិនិត្យទីតាំង",
                        "",
                        "",
                    ]
                ]
            ],
        ];

        $businessOperationItems = [
            [//0
                'title' => [
                    'en' => [
                        "Ministry of Interior",
                        "Ministry of Labor and Vocational Training",
                        "Phnom Penh Municipality",
                        "Ministry of Agriculture",
                    ],
                    'kh' => [
                        "ក្រសួងមហាផ្ទៃ",
                        "ក្រសួងការងារ និងបណ្តុះបណ្តាលវិជ្ជាជីវៈ",
                        "សាលារាជធានីភ្នំពេញ",
                        "ក្រសួង កសិកម្ម រុក្ខាប្រមាញ់ និងនេសាទ",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Business approval inspection",
                        "Location Approval Inspection",
                        "",
                        "",
                    ],
                    'kh' => [
                        "ការត្រួតពិនិត្យដោយមន្រ្តីក្រសួង កសិកម្ម រុក្ខាប្រមាញ់ និងនេសាទ",
                        "ការត្រួតពិនិត្យទីតាំង",
                        "",
                        "",
                    ]
                ]
            ],
            [//1
                'title' => [
                    'en' => [
                        "Institute of Standards of Cambodia",
                        "Ministry of Health",
                        "Ministry of Environment",
                        "Phnom Penh Municipality",
                    ],
                    'kh' => [
                        "វិទ្យាស្ថានស្តង់ដារកម្ពុជា ",
                        "ក្រសួងមហាផ្ទៃ",
                        "ក្រសួងបរិដ្ឋាន",
                        "សាលារាជធានីភ្នំពេញ",
                    ],
                ],
                'description' => [
                    'en' => [
                        "Product Quality Inspection",
                        "Business Location Inspection",
                        "Hygiene Inspection",
                        "",
                    ],
                    'kh' => [
                        "ការត្រួតពិនិត្យគុណភាពផលិតផល",
                        "ការត្រួតពិនិត្យទីតាំង",
                        "ការត្រួតពិនិត្យ អនាម័យ",
                        "",
                    ]
                ]
            ],
            [//2
                'title' => [
                    'en' => [
                        "Institute of Standards of Cambodia",
                        "Ministry of Interior",
                        "Ministry of Labor and Vocational Training",
                        "Phnom Penh Municipality",
                    ],
                    'kh' => [
                        " វិទ្យាស្ថានស្តង់ដារកម្ពុជា ",
                        "ក្រសួងសុខាភិបាល",
                        "ក្រសួងការងារ និងបណ្តុះបណ្តាលវិជ្ជាជីវៈ",
                        "សាលារាជធានីភ្នំពេញ",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Product Quality Inspection",
                        "Business Location Inspection",
                        "Hygiene Inspection",
                        ""
                    ],
                    'kh' => [
                        "ការត្រួតពិនិត្យគុណភាពផលិតផល",
                        "ការត្រួតពិនិត្យទីតាំង",
                        "ការត្រួតពិនិត្យ អនាម័យ",
                        "",
                    ]
                ]
            ],
            [//3
                'title' => [
                    'en' => [
                        "Ministry of Health",
                        "Ministry of Interior",
                        "Ministry of Labor and Vocational Training",
                        "",
                    ],
                    'kh' => [
                        "ក្រសួងសុខាភិបាល",
                        "ក្រសួងមហាផ្ទៃ",
                        "ក្រសួងការងារ និងបណ្តុះ បណ្តាលវិជ្ជាជីវៈ",
                        "",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Fire Safety and Security Inspection",
                        "Hygiene Inspection",
                        "Location Approval Inspection",
                        "",
                    ],
                    'kh' => [
                        " ការត្រួតពិនិត្យសន្តិសុខ និងសុវត្ថិភាពអគ្គីភ័យ",
                        "ការត្រួតពិនិត្យ អនាម័យ",
                        "ការត្រួតពិនិត្យទីតាំង",
                        "",
                    ]
                ]
            ],
            [//4
                'title' => [
                    'en' => [
                        "Ministry of Health",
                        "Ministry of Interior",
                        "Ministry of Labor and Vocational Training",
                        "",
                    ],
                    'kh' => [
                        "ក្រសួងសុខាភិបាល",
                        "ក្រសួងមហាផ្ទៃ",
                        "ក្រសួងការងារ និងបណ្តុះបណ្តាលវិជ្ជាជីវៈ",
                        "",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Fire Safety and Security Inspection",
                        "Hygiene Inspection",
                        "Location Approval Inspection",
                        "",
                    ],
                    'kh' => [
                        "ការត្រួតពិនិត្យសន្តិសុខ និងសុវត្ថិភាពអគ្គីភ័យ",
                        " ការត្រួតពិនិត្យ អនាម័យ",
                        "ការត្រួតពិនិត្យទីតាំង",
                        "",
                    ]
                ]
            ],
            [//5
                'title' => [
                    'en' => [
                        "Institute of Standards of Cambodia",
                        "Ministry of Health",
                        "Ministry of Labor and Vocational Training",
                        "Phnom Penh Municipality",
                    ],
                    'kh' => [
                        "វិទ្យាស្ថានស្តង់ដារកម្ពុជា ",
                        "ក្រសួងសុខាភិបាល",
                        "ក្រសួងការងារ និងបណ្តុះបណ្តាលវិជ្ជាជីវៈ",
                        "សាលារាជធានីភ្នំពេញ",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Product Quality Inspection",
                        "Business Location Inspection",
                        "Hygiene Inspection",
                        "",
                    ],
                    'kh' => [
                        "ការត្រួតពិនិត្យគុណភាពផលិតផល",
                        "ការត្រួតពិនិត្យទីតាំង",
                        "ការត្រួតពិនិត្យ អនាម័យ",
                        "",
                    ]
                ]
            ],
            [//6
                'title' => [
                    'en' => [
                        "Institute of Standards of Cambodia",
                        "Ministry of Health",
                        "Ministry of Environment",
                        "Phnom Penh Municipality",
                    ],
                    'kh' => [
                        "វិទ្យាស្ថានស្តង់ដារកម្ពុជា ",
                        "ក្រសួងសុខាភិបាល",
                        "ក្រសួងបរិដ្ឋាន",
                        "សាលារាជធានីភ្នំពេញ",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Product Quality Inspection",
                        "Business Location Inspection",
                        "Hygiene Inspection",
                        "",
                    ],
                    'kh' => [
                        "ការត្រួតពិនិត្យគុណភាពផលិតផល",
                        "ការត្រួតពិនិត្យទីតាំង",
                        "ការត្រួតពិនិត្យ អនាម័យ",
                        "",
                    ]
                ]
            ],
            [//7
                'title' => [
                    'en' => [
                        "Ministry of Health",
                        "Ministry of Interior",
                        "Ministry of Labor and Vocational Training",
                        "",
                    ],
                    'kh' => [
                        "ក្រសួងសុខាភិបាល",
                        "ក្រសួងមហាផ្ទៃ",
                        "ក្រសួងការងារ និងបណ្តុះបណ្តាលវិជ្ជាជីវៈ",
                        "",
                    ]

                ],
                'description' => [
                    'en' => [
                        "Fire Safety and Security Inspection",
                        "Hygiene Inspection",
                        "Location Approval Inspection",
                        "",
                    ],
                    'kh' => [
                        " ការត្រួតពិនិត្យសន្តិសុខ និងសុវត្ថិភាពអគ្គីភ័យ",
                        "ការត្រួតពិនិត្យ អនាម័យ",
                        "ការត្រួតពិនិត្យទីតាំង",
                        "",
                    ]
                ]
            ],
            [//8
                'title' => [
                    'en' => [
                        "Institute of Standards of Cambodia",
                        "Ministry of Health",
                        "Ministry of Labor and Vocational Training",
                        "Phnom Penh Municipality",
                    ],
                    'kh' => [
                        "វិទ្យាស្ថានស្តង់ដារកម្ពុជា ",
                        "ក្រសួងសុខាភិបាល",
                        "ក្រសួងការងារ និងបណ្តុះបណ្តាលវិជ្ជាជីវៈ",
                        "សាលារាជធានីភ្នំពេញ",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Product Quality Inspection",
                        "Business Location Inspection",
                        "Hygiene Inspection",
                        "",
                    ],
                    'kh' => [
                        "ការត្រួតពិនិត្យគុណភាពផលិតផល",
                        "ការត្រួតពិនិត្យទីតាំង",
                        "ការត្រួតពិនិត្យ អនាម័យ",
                        "",
                    ]
                ]
            ],
            [//9
                'title' => [
                    'en' => [
                        "Phnom Penh Municipality",
                        "Ministry of Interior",
                        "Ministry of Labor and Vocational Training",
                        "",
                    ],
                    'kh' => [
                        "សាលារាជធានីភ្នំពេញ",
                        "ក្រសួងមហាផ្ទៃ",
                        "ក្រសួងការងារ និងបណ្តុះបណ្តាលវិជ្ជាជីវៈ",
                        "",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Business Approval Inspection",
                        "Location Approval Inspection",
                        "",
                        "",
                    ],
                    'kh' => [
                        " ការត្រួតពិនិត្យសម្រាប់ដំណើការ អាជីវកម្មការីទេសចរណ៍ថ្មី  និងភ្នាក់ងារទេសចរណ៍ថ្មី",
                        "ការត្រួតពិនិត្យទីតាំង",
                        "",
                        "",
                    ]
                ]
            ],
            [//10
                'title' => [
                    'en' => [
                        "Phnom Penh Municipality",
                        "Ministry of Interior",
                        "Ministry of Labor and Vocational Training",
                    ],
                    'kh' => [
                        "សាលារាជធានីភ្នំពេញ",
                        "ក្រសួងមហាផ្ទៃ",
                        "ក្រសួងការងារ និងបណ្តុះបណ្តាលវិជ្ជាជីវៈ",
                    ]
                ],
                'description' => [
                    'en' => [
                        "Location Approval Inspection",
                        "",
                        "",
                    ],
                    'kh' => [
                        "ការត្រួតពិនិត្យទីតាំង",
                        "",
                        "",
                    ]
                ]
            ],
        ];

        $faqsDescriptions = [
            'en' => [
                "Application for Setting up a New Wholesale or Retail Business Selling Agricultural Input Suppliers",
                "Application for Setting Up a New Small and Medium Factory/Handicraft Produced Bakery Products Business",
                "Application for Setting Up a New Small and Medium Factory/Handicraft Operating Grain Milling Business",
                "Application for Setting up a New - Guesthouse Business in Cambodia",
                "Application for Setting up a New Hotel Business in Cambodia",
                "Application for Setting Up a New Small and - Medium Factory/Handicraft Produced Fish Sauces, Fish Meat Processing, Meat Ball and Sausage Business",
                "Application for Setting up a New Restaurant Business in Cambodia",
                "Application for Setting up a New Small and Medium Factory/handicraft Produced Cunning and Preserving of Fruit and Vegetable Business",
                "Application for Setting up a New Tour Operator and Travel Agency Business in Cambodia",
                "Application for Setting Up a Small and Medium Factory Producing Drinking W ater , Wine and Soft Drink Business in Cambodia",
                "Application for Setting up a New Wholesale/Retail Business in Cambodia",
            ],
            'kh' => [
                "ការស្នើសុំអាជ្ញាបណ្ណ ឬវិញ្ញាបនប័ត្រសម្រាប់ អាជីវកម្មលក់ដុំ លក់រាយលើការផ្គត់ផ្គង់វត្ថុធាតុដើមកសិកម្ម",
                "ការស្នើសុំវិញ្ញាបនប័ត្រសម្រាប់ដំណើការអាជីវកម្មផលិតនំប៉័ង និងផលិតផលពាក់ព័ន្ធ",
                "ការស្នើសុំវិញ្ញាបនប័ត្រសម្រាប់ដំណើការអាជីវកម្ម កិនស្រូវ ម្សៅ ដំឡួង កាហ្វេ តែ និងមី",
                "ការស្នើសុំអាជ្ញាបណ្ណទេសចរណ៍ សម្រាប់ដំណើការអាជីវកម្មផ្ទះភ្ញៀវថ្មី ក្នុងព្រះរាជាណាចក្រកម្ពុជា",
                "ការស្នើសុំអាជ្ញាបណ្ណទេសចរណ៍ សម្រាប់ដំណើការអាជីវកម្មសណ្ឋាគារថ្មី ក្នុងព្រះរាជាណាចក្រកម្ពុជា",
                "កាការស្នើសុំវិញ្ញាបនប័ត្រសម្រាប់ដំណើការអាជីវកម្មផលិតទឹកត្រី កែច្នៃសាច់ត្រី ប្រហិត សាច់ក្រក",
                "ការស្នើសុំអាជ្ញាបណ្ណទេសចរណ៍ សម្រាប់ដំណើការអាជីវកម្មភោជនីយដ្ឋានថ្មីក្នុងព្រះរាជាណាចក្រកម្ពុជា",
                "ការស្នើសុំវិញ្ញាបនប័ត្រសម្រាប់ដំណើការអាជីវកម្ម ផលិតទឹកសណ្តែក ទឹកម្ទេស ទឹកប៉េងប៉ោះ",
                "ការស្នើសុំអាជ្ញាបណ្ណទេសចរណ៍សម្រាប់ដំណើការអាជីវកម្មការីទេសចរណ៍ថ្មី  ភ្នាក់ងារទេសចរណ៍ថ្មី ក្នុងព្រះរាជាណាចក្រកម្ពុជា",
                "ការស្នើសុំវិញ្ញាបនប័ត្រសម្រាប់ដំណើការអាជីវកម្មផលិតទឹកសុទ្ធ ស្រា ភេសជ្ជៈមិនមានជាតិអាល់កុលក្នុងព្រះរាជាណាចក្រកម្ពុជា",
                "ការស្នើសុំវិញ្ញាបនប័ត្រសម្រាប់ដំណើការអាជីវកម្មលក់ដុំ លក់រាយ ",
            ],
        ];

        for ($i = 0; $i < 11; $i++) {
            $plItems = array();
            for ($j = 0; $j < count($primaryLicenseItems[$i]['title']['en']); $j++) {
                $plItems[$j] = [
                    'title_en' => $primaryLicenseItems[$i]['title']['en'][$j],
                    'title_kh' => $primaryLicenseItems[$i]['title']['kh'][$j],
                    'description_en' => $primaryLicenseItems[$i]['description']['en'][$j],
                    'description_kh' => $primaryLicenseItems[$i]['description']['kh'][$j],
                ];

            }

            $olItems = array();
            for ($j = 0; $j < count($otherLicenseItems[$i]['title']['en']); $j++) {
                $olItems[$j] = [
                    'title_en' => $otherLicenseItems[$i]['title']['en'][$j],
                    'title_kh' => $otherLicenseItems[$i]['title']['kh'][$j],
                    'description_en' => $otherLicenseItems[$i]['description']['en'][$j],
                    'description_kh' => $otherLicenseItems[$i]['description']['kh'][$j],
                ];

            }

            $apItems = array();
            for ($j = 0; $j < count($applicationProcessItems[$i]['title']['en']); $j++) {
                $apItems[$j] = [
                    'title_en' => $applicationProcessItems[$i]['title']['en'][$j],
                    'title_kh' => $applicationProcessItems[$i]['title']['kh'][$j],
                    'description_en' => $applicationProcessItems[$i]['description']['en'][$j],
                    'description_kh' => $applicationProcessItems[$i]['description']['kh'][$j],
                ];

            }

            $boItems = array();
            for ($j = 0; $j < count($businessOperationItems[$i]['title']['en']); $j++) {
                $boItems[$j] = [
                    'title_en' => $businessOperationItems[$i]['title']['en'][$j],
                    'title_kh' => $businessOperationItems[$i]['title']['kh'][$j],
                    'description_en' => $businessOperationItems[$i]['description']['en'][$j],
                    'description_kh' => $businessOperationItems[$i]['description']['kh'][$j],
                ];

            }

            $licenseInspection = [
                'primary_license' => [
                    'title_en' => $primaryLicenseTitleEn,
                    'title_kh' => $primaryLicenseTitleKh,
                    'slogan_en' => $primaryLicenseSloganEn,
                    'slogan_kh' => $primaryLicenseSloganKh,
                    'item' => $plItems,
                ],
                'other_license' => [
                    'title_en' => $otherLicenseTitleEn,
                    'title_kh' => $otherLicenseTitleKh,
                    'slogan_en' => $otherLicenseSloganEn,
                    'slogan_kh' => $otherLicenseSloganKh,
                    'item' => $olItems,
                ],
                'application_process' => [
                    'title_en' => $applicationProcessTitleEn,
                    'title_kh' => $applicationProcessTitleKh,
                    'slogan_en' => $applicationProcessSloganEn,
                    'slogan_kh' => $applicationProcessSloganKh,
                    'item' => $apItems,
                ],
                'business_operation' => [
                    'title_en' => $businessOperationTitleEn,
                    'title_kh' => $businessOperationTitleKh,
                    'slogan_en' => $businessOperationSloganEn,
                    'slogan_kh' => $businessOperationSloganKh,
                    'item' => $boItems,
                ],
            ];


            DB::table('b121nf0_license_mappings')->insert([
                'title_en' => $titles['en'][$i],
                'title_kh' => $titles['kh'][$i],
                'thumbnail' => $thumbnail[$i],
                'banner' => 'uploads/banner-1.jpg',
                'diagram_en' => 'uploads/diagram.jpg',
                'initiative_thumb_en' => 'uploads/initiative.jpg',
                'document_en' => 'uploads/document.pdf',
                'ordering' => $i,
                'license_inspection' => json_encode($licenseInspection),
                'name' => str_replace(str_split('\\/:*?"<>|& '), '-', strtolower($titles['en'][$i])),
                'regulation_id' => 2,
                'faqs_description_en' => $faqsDescriptions['en'][$i],
                'faqs_description_kh'=> $faqsDescriptions['kh'][$i],
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]);
        }
        echo "11 License Mapping is created\n";
    }
}
