<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTable::class);
//        $this->call(RolesAndPermissionsSeeder::class);
//
//        $this->call(SettingTableSeeder::class);
//        $this->call(ContentElementTableSeeder::class);
//        $this->call(AdvertisementTableSeeder::class);
//
//
//        $this->call(CategoryTableSeeder::class);
//        $this->call(ServiceTableSeeder::class);
//        $this->call(DirectoryTableSeeder::class);
//        $this->call(ServiceDirectoryTableSeeder::class);
//        $this->call(RegulationTableSeeder::class);
//        $this->call(LicenseMappingTableSeeder::class);
//        $this->call(RalatedMinistryTableSeeder::class);
//        $this->call(FaqsTableSeeder::class);

        // old table
//        $this->call(HomeTableSeeder::class);
//        $this->call(CategoryOldTableSeeder::class);
//        $this->call(ContactUsTableSeeder::class);
//        $this->call(GalleryTableSeeder::class);
//        $this->call(MediaTableSeeder::class);
//        $this->call(OptionTableSeeder::class);
//        $this->call(PageTableSeeder::class);
//        $this->call(SocialNetworkTableSeeder::class);
    }
}
