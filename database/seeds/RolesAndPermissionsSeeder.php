<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        // create permissions
        Permission::create(['name' => 'view user']);
        Permission::create(['name' => 'create user']);
        Permission::create(['name' => 'update user']);
        Permission::create(['name' => 'delete user']);

        // create roles and assign existing permissions
        $role = Role::create(['name' => 'editor']);
        $role->givePermissionTo('view user');

        $role = Role::create(['name' => 'admin']);
        $role->givePermissionTo('view user');
        $role->givePermissionTo('create user');
        $role->givePermissionTo('update user');
        $role->givePermissionTo('delete user');

        echo "2 user created admin and editor";
    }
}
