<?php

use Illuminate\Database\Seeder;

class ServiceDirectoryTableSeeder extends Seeder
{
    /**
     * Command: php artisan db:seed --class=ServiceDirectoryTableSeeder
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $serviceId = [1, 1, 1, 1, 2, 2, 2, 3, 3, 4, 4, 5, 5, 5, 6, 6, 6, 6, 6];

        $directoryId = [9, 39, 40, 53, 11, 14, 15, 39, 40, 15, 38, 1, 25, 81, 70, 57, 74, 6, 8];

        for ($i = 0; $i < 19; $i++) {
            DB::table('b121nf0_service_directories')->insert([
                'service_id' => $serviceId[$i],
                'directory_id' => $directoryId[$i],
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]);
        }

        echo("Service and directory relationship created \n");
    }
}
