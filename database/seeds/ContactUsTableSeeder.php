<?php

use Illuminate\Database\Seeder;

class ContactUsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('b121nf0_contact_us')->insert([
            'address_en' => 'No. 113, Parkway Square, 2rd Floor, Suite 2FG, 245 Mao Tse Toung Boulevard (245), Phnom Penh',
            'address_km' => 'Suite 1.3, First Floor, Parkway Square, Moa Tse Tong Blvd, Phnom Penh',
            'email' => 'info@bizinfo.center',
            'phone' => '(855) 16 999 762',
            'latitude' => '11.544321',
            'longitude' => '104.912765',
            'zoom' => 16,
            'date' => \Carbon\Carbon::now(),
            'modify_date' => \Carbon\Carbon::now(),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
    }
}
