<?php

use Illuminate\Database\Seeder;

class HomeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $positions = [
            "Designer",
            "Software Engineering",
            "Sales Executive",
            "Mobile Application Android/iOS",
            "Web Developer",
        ];
        for ($i = 0; $i < 4; $i++) {
            DB::table('b121nf0_career')->insert([
                'position' => $positions[$i],
                'name' => str_replace(str_split('\\/:*?"<>|& '), '-', strtolower($positions[$i])),
                'date' => \Carbon\Carbon::now(),
                'modify_date' => \Carbon\Carbon::now(),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]);
        }

        $heroBannerTitles = [
            'en' => [
                'The preferred source of information & opportunities for MSMEs',
                'The preferred source of information & opportunities for MSMEs'
            ],
            'kh' => [
                'ប្រភពព័ត៌មាន និងឱកាសសំបូរបែប',
                'ប្រភពព័ត៌មាន និងឱកាសសំបូរបែប',
            ]
        ];
        $heroBannerDes = [
            'en' => [
                ' ',
                ' ',
            ],
            'kh' => [
                'សម្រាប់សហគ្រាសធុនតូច និងមធ្យម',
                'សម្រាប់សហគ្រាសធុនតូច និងមធ្យម',
            ]
        ];
        for ($i = 0; $i < 2; $i++) {
            DB::table('b121nf0_herobanner')->insert([
                'title_en' => $heroBannerTitles['en'][$i],
                'title_km' => $heroBannerTitles['kh'][$i],
                'description_en' => $heroBannerDes['en'][$i],
                'description_km' => $heroBannerDes['kh'][$i],
                'image' => $i + 11,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]);
        }
        echo "2 hero banner created \n";


        $spTitles = [
            'ADB',
            'Australian Aid',
            'MBI',
        ];
        $spUrl = [
            'https://www.adb.org/',
            '',
            'https://www.mekongbiz.org/',
        ];
        for ($i = 0; $i < 2; $i++) {
            DB::table('b121nf0_strategic_partners')->insert([
                'title' => $spTitles[$i],
                'image' => $i + 83,
                'url' => $spUrl[$i],
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]);
        }
        echo "2 Strategic partners created \n";


        $neTitles = [
            'en' => [
                "Advocate and support the implementation of Cambodia Industrial Development Policy to advance the interest of young entrepreneurs  Closely collaboration with Ministry of Economy and Finance",
                "Enable access to business and financing opportunities for young entrepreneurs within Cambodia, ASEAN and Worldwide",
                "Global Entrepreneurship Week (GEW)",
                "Global Entrepreneurship Week (GEW)",
                "CEO - Lunch (Breakfast)",
                "Business Matching and Oversea Business Trip",
                "Leverage the members' pool of expertise and experiences to support the implementation of our strategies and secure diverse funding to finance our vision",
                "YEAC's Corporate Social Responsibility & Retreat",
            ],
            'kh' => [
                "តស៊ូមតិនិងគាំទ្រការអនុវត្តគោលនយោបាយអភិវឌ្ឍន៍ឧស្សាហកម្មកម្ពុជាដើម្បីលើកកម្ពស់ចំណាប់អារម្មណ៍របស់សហគ្រិនវ័យក្មេង",
                "បើកលទ្ធភាពទទួលបានឱកាសអាជីវកម្មនិងហិរញ្ញវត្ថុសម្រាប់សហគ្រិនវ័យក្មេងនៅក្នុងប្រទេសកម្ពុជាអាស៊ាននិងនៅទូទាំងពិភពលោក  ពានរង្វាន់សហគ្រិនវ័យក្មេងកម្ពុជាឆ្នាំ 2015",
                "សប្តាហ៍ភាពជាសហគ្រិនសកល (GEW)",
                "ពានរង្វាន់ NATIONAL STARTUP",
                "នាយកប្រតិបត្តិ - អាហារថ្ងៃត្រង់ (អាហារពេលព្រឹក)",
                "ការផ្គូផ្គងអាជីវកម្មនិងដំណើរកំសាន្តក្រៅប្រទេស",
                "ប្រើប្រាស់សមាជិកនិងបទពិសោធន៍របស់សមាជិកដើម្បីគាំទ្រការអនុវត្ដយុទ្ធសាស្រ្តរបស់យើងនិងធានានូវការផ្តល់ថវិកាចម្រុះដើម្បីផ្តល់ហិរញ្ញប្បទានដល់ចក្ខុវិស័យរបស់យើង  សិក្ខាសាលាកសាងសមត្ថភាពនិងការពិគ្រោះយោបល់សម្រាប់សមាជិក",
                "ទំនួលខុសត្រូវសង្គមរបស់ក្រុមហ៊ុន YEAC និងការសម្រាក",
            ]
        ];
        $neDes = [
            'en' => [
                'Meeting with Tax Department to discuss about the challenging of entrepreneur on tax issues \n Meeting with Customs Department to study more about the update-customs policy',
                'Date: 4th Week of May \n Objectives:  To honor the creative young entrepreneurs, raise the profile of young entrepreneurs, promote and support the business creativity and new enterprise development in Cambodia.',
                'Date: 3rd Week of November \n Objectives:  To celebrate the innovators and job creators, intimate networks, connect to potential collaborators, mentors and investors. ',
                'Date: 2nd Week of September \n Objectives:  To cooperate with the STARTUP NATIONS to bring the innovative ideas that have had the positive impact on the national ecosystem and to enable the startups to scale, create new jobs and push the entrepreneurship frontier forward.',
                'Date: Monthly on Thursday of 3rd Week (11:30am -2:00pm)\n Objectives: To bring up together all members to share experiences, knowledge, and information and to identify business issues/problems, and address and/or find the solutions',
                'Date: At Least twice per year \n Objectives: \n - To synergy business opportunity locally and internationally \n - To strengthen the networking, partnership and formation of the sub committee',
                'Date: 4times per year \n Objectives: To strengthen members\' capacity in the management, government policies and regulations, and business operation \n Trainers: Experts from government, private sector, and international ',
                'To strengthen the relationship among YEAC\'s members (Week 4th, September)',
                ],
            'kh' => [
                "ជំនួបប្រជុំជាមួយ​ នាយកដ្ឋានពន្ធត្រូវបានជជែក  ជំជួបជាមួយនាយកដ្ឋានគយដើម្បីសិក្សាបន្ថែមអំពីគោលនយោបាយពត៍មាននាពេលបច្ចុប្បន្ន",
                "កាលបរិច្ឆេទ: សប្តាហ៍ទី 4 នៃខែឧសភា គោលបំណង: ដើម្បីផ្តល់កិត្តិយសដល់សហគ្រិនវ័យក្មេងដែលប្រកបដោយភាពច្នៃប្រឌិតលើកឡើងពីការជំរុញនិងគាំទ្រការច្នៃប្រឌិតអាជីវកម្មនិងការអភិវឌ្ឍសហគ្រាសថ្មីនៅក្នុងប្រទេសកម្ពុជា។",
                "កាលបរិច្ឆេទ: សប្តាហ៍ទី 3 នៃខែវិច្ឆិកា គោលបំណង: ដើម្បីអបអរសាទរអ្នកច្នៃប្រឌិត និងអ្នកបង្កើតការងារទំនាក់ទំនងជិតស្និទ្ធ ភ្ជាប់ទៅកាន់សហការីដ៏មានសក្តានុពលរួមទាំងអ្នកណែនាំនិងវិនិយោគិន។",
                "កាលបរិច្ឆេទ: សប្តាហ៍ទី 2 នៃខែកញ្ញា គោលបំណង: សហការជាមួយ STARTUP NATIONS ដើម្បីនាំយកគំនិតច្នៃប្រឌិតថ្មីដែលមានផលវិជ្ជមានទៅលើប្រព័ន្ធអេកូឡូស៊ីថ្នាក់ជាតិនិងដើម្បីបង្កើតការចាប់ផ្តើមអាជីវកម្មបង្កើតការងារថ្មីនិងជំរុញទៅមុខភាពជាសហគ្រិន។",
                "កាលបរិច្ឆេទ: រៀងរាល់ខែនៅថ្ងៃព្រហស្បតិ៍នៃសប្តាហ៍ទី 3 (11:30 ព្រឹក -2:00 ល្ងាច) គោលបំណង: ដើម្បីប្រមូលផ្តុំសមាជិកទាំងអស់ដើម្បីចែករំលែកបទពិសោធន៍ចំណេះដឹងនិងព័ត៌មាននិងដើម្បីកំណត់បញ្ហាអាជីវកម្មនិងដើម្បីស្វែងរកដំណោះស្រាយ។",
                "កាលបរិច្ឆេទ: យ៉ាងហោចណាស់ពីរដងក្នុងមួយឆ្នាំ គោលបំណង: - ដើម្បីរួមបញ្ចូលគ្នានូវឱកាសពាណិជ្ជកម្មក្នុងនិងក្រៅប្រទេស - ពង្រឹងបណ្តាញភាពជាដៃគូនិងការបង្កើតគណៈកម្មាធិការរង",
                "កាលបរិច្ឆេទ: 4 ដងក្នុងមួយឆ្នាំ គោលបំណង: ពង្រឹងសមត្ថភាពរបស់សមាជិកក្នុងការគ្រប់គ្រងគោលនយោបាយនិងបទបញ្ជារបស់រដ្ឋាភិបាលនិងប្រតិបត្តិការអាជីវកម្ម គ្រូបង្គោល: អ្នកជំនាញការមកពីរដ្ឋាភិបាលវិស័យឯកជននិងអន្តរជាតិ",
                "ទំនួលខុសត្រូវសង្គមរបស់ក្រុមហ៊ុន YEAC និងការសម្រាកដើម្បីពង្រឹងទំនាក់ទំនងរវាងសមាជិករបស់ YEAC (សប្តាហ៍ទី 4 ខែកញ្ញា)",
            ]
        ];
        $neCat = [10, 10, 11, 11, 11, 11, 9, 9];

        for ($i = 0; $i < 8; $i++) {
            DB::table('b121nf0_news_events')->insert([
                'title_en' => $neTitles['en'][$i],
                'title_km' => $neTitles['kh'][$i],
                'description_en' => $neDes['en'][$i],
                'description_km' => $neDes['kh'][$i],
                'thumbnail' => $i + 238,
                'name' => str_replace(str_split('\\/:*?"<>|& '), '-', strtolower($neTitles['en'][$i])),
                'content' => '',
                'status' => 1,
                'category' => $neCat[$i],
                'author' => 0,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'date' => \Carbon\Carbon::now(),
                'modify_date' => \Carbon\Carbon::now(),
            ]);
        }
        echo "11 News and Events created \n";



        $mediaUrls = [

        ];
        $mediaType = [

        ];
        $mediaNote = [

        ];

        for ($i = 0; $i < 2; $i++) {
            DB::table('b121nf0_media')->insert([
                'url' => $spUrl[$i],
                'type' => $spTitles[$i],
                'note' => $i + 83,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]);
        }
        echo "2 Strategic partners created \n";
    }
}
