<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use \App\Models\ContentElement;

class ContentElementTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contentPageRegulation = [
            'page_title' =>[
                'title_en' => 'Regulation',
                'title_kh' => 'kh'
            ]
        ];
        DB::table('b121nf0_content_elements')->insert([
            'type' => ContentElement::PAGE_TITLE,
            'name' => ContentElement::REGULATION,
            'content' => json_encode($contentPageRegulation),
        ]);

        $contentPageRelatedMinistry = [
            'page_title' =>[
                'title_en' => 'Related Ministry',
                'title_kh' => 'kh'
            ]
        ];
        DB::table('b121nf0_content_elements')->insert([
            'type' => ContentElement::PAGE_TITLE,
            'name' => ContentElement::RELATED_MINISTRY,
            'content' => json_encode($contentPageRelatedMinistry),
        ]);

        $contentPageSMEPolicy = [
            'page_title' =>[
                'title_en' => 'SME Policy and Framework',
                'title_kh' => 'kh'
            ]
        ];
        DB::table('b121nf0_content_elements')->insert([
            'type' => ContentElement::PAGE_TITLE,
            'name' => ContentElement::SME_POLICY,
            'content' => json_encode($contentPageSMEPolicy),
        ]);

        $contentPageLicenseMapping= [
            'page_title' =>[
                'title_en' => 'License Mapping',
                'title_kh' => 'kh'
            ]
        ];
        DB::table('b121nf0_content_elements')->insert([
            'type' => ContentElement::PAGE_TITLE,
            'name' => ContentElement::LICENSE_MAPPING,
            'content' => json_encode($contentPageLicenseMapping),
        ]);

        $contentHeroBannerPageRegulation = [
            'hero_banner' =>[
                'title_en' => 'Hero banner',
                'title_kh' => 'kh',
                'banner' => 'uploads/banner-insight.jpg'
            ]
        ];
        DB::table('b121nf0_content_elements')->insert([
            'type' => ContentElement::HERO_BANNER,
            'name' => ContentElement::REGULATION,
            'content' => json_encode($contentHeroBannerPageRegulation),
        ]);

        $contentHeroBannerPageRelatedMinistry = [
            'hero_banner' =>[
                'title_en' => 'Hero banner',
                'title_kh' => 'kh',
                'banner' => 'uploads/banner-ministry.jpg'
            ]
        ];
        DB::table('b121nf0_content_elements')->insert([
            'type' => ContentElement::HERO_BANNER,
            'name' => ContentElement::RELATED_MINISTRY,
            'content' => json_encode($contentHeroBannerPageRelatedMinistry),
        ]);

        DB::table('b121nf0_content_elements')->insert([
            'type' => ContentElement::HERO_BANNER,
            'name' => ContentElement::SME_POLICY,
            'content' => json_encode($contentHeroBannerPageRelatedMinistry),
        ]);

        $contentRegulationCategory = [
            'regulation_category' =>[
                'title_en' => 'Related Ministry',
                'title_kh' => 'kh',
                'link' => 'regulation/related-ministry',
                'image_background' => 'uploads/banner-ministry.jpg'
            ]
        ];
        DB::table('b121nf0_content_elements')->insert([
            'type' => ContentElement::REGULATION_CATEGORY,
            'content' => json_encode($contentRegulationCategory),
        ]);

        $contentRegulationCategory1 = [
            'regulation_category' =>[
                'title_en' => 'SME Licence Mapping',
                'title_kh' => 'kh',
                'link' => 'regulation/sme-license-mapping',
                'image_background' => 'uploads/banner-ministry.jpg'
            ]
        ];
        DB::table('b121nf0_content_elements')->insert([
            'type' => ContentElement::REGULATION_CATEGORY,
            'content' => json_encode($contentRegulationCategory1),
        ]);

        $contentRegulationCategory2 = [
            'regulation_category' =>[
                'title_en' => 'SME Policy & Frameworks',
                'title_kh' => 'kh',
                'link' => 'regulation/cambodia-industry-development-policy',
                'image_background' => ''
            ]
        ];
        DB::table('b121nf0_content_elements')->insert([
            'type' => ContentElement::REGULATION_CATEGORY,
            'content' => json_encode($contentRegulationCategory2),
        ]);

        $contentSearchBox = [
            'search_box' =>[
                'title_en' => 'I WANT TO START A NEW BUSINESS',
                'title_kh' => 'kh',
                'description_en' => 'If you cannot find your industry!',
                'description_kh' => 'kh',
                'placeholder_en' => 'Search....',
                'placeholder_kh' => 'kh',
            ]
        ];
        DB::table('b121nf0_content_elements')->insert([
            'type' => ContentElement::SEARCH_BOX,
            'name' => ContentElement::REGULATION,
            'content' => json_encode($contentSearchBox),
        ]);
    }
}
