<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceDirectoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b121nf0_service_directories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('service_id')->nullable();
            $table->unsignedInteger('directory_id')->nullable();
            $table->timestamps();

//            $table->foreign('service_id')
//                ->references('id')
//                ->on('b121nf0_services');

            /** todo to disable directory_id foreign because it has a bug with custom table name */
//            $table->foreign('directory_id')
//                ->references('id')
//                ->on('b121nf0_directory');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b121nf0_service_directories');
    }
}
