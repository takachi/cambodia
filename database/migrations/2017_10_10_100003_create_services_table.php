<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b121nf0_services', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_en');
            $table->string('title_kh')->nullable();
            $table->string('thumb');
            $table->string('banner')->nullable();
            $table->string('name');
            $table->integer('ordering')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b121nf0_services');
    }
}
