<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDirectoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b121nf0_directories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_en');
            $table->string('title_kh')->nullable();
            $table->string('logo')->nullable();
            $table->string('mail')->nullable();
            $table->string('phone')->nullable();
            $table->string('website')->nullable();
            $table->text('address_en')->nullable();
            $table->text('address_kh')->nullable();
            $table->text('about_en')->nullable();
            $table->text('about_kh')->nullable();
            $table->text('service_en')->nullable();
            $table->text('service_kh')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->boolean('pin')->defautl(0);
            $table->string('name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b121nf0_directories');
    }
}
