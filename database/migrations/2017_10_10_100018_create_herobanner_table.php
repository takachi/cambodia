<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHerobannerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::create('b121nf0_herobanner', function (Blueprint $table) {
//            $table->increments('id');
//            $table->string('title_en')->nullable();
//            $table->string('title_km')->nullable();
//            $table->text('description_en')->nullable();
//            $table->text('description_km')->nullable();
//            $table->string('image')->nullable();
//            $table->string('url')->nullable();
//            $table->integer('menu_order')->nullable();
//            $table->timestamps();
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b121nf0_herobanner');
    }
}
