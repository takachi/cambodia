<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b121nf0_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_en');
            $table->string('title_kh')->nullable();
            $table->string('thumb');
            $table->string('banner')->nullable();
            $table->string('status_en')->nullable();
            $table->string('status_kh')->nullable();
            $table->string('name');
            $table->integer('parent')->default(0);
            $table->integer('ordering')->nullable();
            $table->boolean('hidden')->default(0);
            $table->boolean('hidden_child')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b121nf0_categories');
    }
}
