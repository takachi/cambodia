<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarouselsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b121nf0_carousels', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('service_id')->nullable();
            $table->string('name')->nullable();
            $table->string('title_en')->nullable();
            $table->string('title_kh')->nullable();
            $table->string('description_en')->nullable();
            $table->string('description_kh')->nullable();
            $table->string('image');
            $table->timestamps();

            $table->foreign('service_id')
                ->references('id')
                ->on('b121nf0_services')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
