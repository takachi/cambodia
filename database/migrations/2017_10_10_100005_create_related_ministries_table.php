<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelatedMinistriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b121nf0_related_ministries', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('regulation_id');
            $table->string('logo')->nullable();
            $table->string('title_en');
            $table->string('title_kh')->nullable();
            $table->string('website')->nullable();
            $table->string('uri');
            $table->timestamps();

            $table->foreign('regulation_id')
                ->references('id')
                ->on('b121nf0_regulations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b121nf0_related_ministries');
    }
}
