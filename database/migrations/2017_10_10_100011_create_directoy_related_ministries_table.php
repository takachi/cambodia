<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDirectoyRelatedMinistriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b121nf0_directory_related_ministries', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('related_ministry_id');
            $table->unsignedInteger('directory_id');
            $table->timestamps();

            $table->foreign('related_ministry_id')
                ->references('id')
                ->on('b121nf0_related_ministries');

            /** todo to disable directory_id foreign because it has a bug with custom table name */
            $table->foreign('directory_id')
                ->references('id')
                ->on('b121nf0_directories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
