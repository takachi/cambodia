<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::create('b121nf0_news_events', function (Blueprint $table) {
//            $table->increments('id');
//            $table->string('thumbnail')->nullable();
//            $table->text('title_en')->nullable();
//            $table->text('title_km')->nullable();
//            $table->text('description_en')->nullable();
//            $table->text('description_km')->nullable();
//            $table->text('name');
//            $table->string('content')->nullable();
//            $table->boolean('status')->nullable();
//            $table->integer('category')->nullable();
//            $table->integer('author')->nullable();
//            $table->timestamps();
//            $table->dateTime('date');
//            $table->dateTime('modify_date');
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b121nf0_news_events');
    }
}
