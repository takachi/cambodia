<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLicenseMappingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b121nf0_license_mappings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('regulation_id');
            $table->string('title_en');
            $table->string('title_kh')->nullable();
            $table->string('thumbnail')->nullable();
            $table->string('banner')->nullable();
            $table->string('diagram_en')->nullable();
            $table->string('diagram_kh')->nullable();
            $table->string('initiative_thumb_en')->nullable();
            $table->string('initiative_thumb_kh')->nullable();
            $table->string('document_en')->nullable();
            $table->string('document_kh')->nullable();
            $table->integer('ordering')->nullable();
            $table->text('license_inspection')->nullable();
            $table->string('name')->nullable();
            $table->string('faqs_title_en')->nullable();
            $table->string('faqs_title_kh')->nullable();
            $table->string('faqs_description_en')->nullable();
            $table->string('faqs_description_kh')->nullable();
            $table->timestamps();

            $table->foreign('regulation_id')
                ->references('id')
                ->on('b121nf0_regulations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b121nf0_license_mappings');
    }
}
