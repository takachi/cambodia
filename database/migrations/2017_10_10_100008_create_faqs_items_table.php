<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaqsItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b121nf0_faqs_items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('license_mapping_id');
            $table->string('question_en');
            $table->string('question_kh')->nullable();
            $table->text('answer_en');
            $table->text('answer_kh')->nullable();
            $table->timestamps();

            $table->foreign('license_mapping_id')
                ->references('id')
                ->on('b121nf0_license_mappings')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b121nf0_faqs_items');
    }
}
