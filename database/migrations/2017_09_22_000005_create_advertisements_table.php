<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertisementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b121nf0_advertisements', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image')->nullable();
            $table->string('url')->nullable();
            $table->string('title_en')->nullable();
            $table->string('title_kh')->nullable();
            $table->string('link')->nullable();
            $table->string('page')->nullable();
            $table->timestamp('active_at');
            $table->timestamp('expired_at')->nullable();
            $table->string('status')->nullable();
            $table->integer('section')->nullable();
            $table->timestamps();
            $table->dateTime('date')->nullable();
            $table->dateTime('modify_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertisements');
    }
}
