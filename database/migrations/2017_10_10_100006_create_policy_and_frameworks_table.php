<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePolicyAndFrameworksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b121nf0_policy_and_frameworks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('regulation_id');
            $table->string('title_en');
            $table->string('title_kh')->nullable();
            $table->text('description_en')->nullable();
            $table->text('description_kh')->nullable();
            $table->string('reference_en')->nullable();
            $table->string('reference_kh')->nullable();
            $table->boolean('status');
            $table->integer('author');
            $table->timestamps();

            $table->foreign('regulation_id')
                ->references('id')
                ->on('b121nf0_regulations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b121nf0_policy_and_frameworks');
    }
}
