<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegulationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b121nf0_regulations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordering')->nullable();
            $table->string('title_en');
            $table->string('title_kh')->nullable();
            $table->string('thump')->nullable();
            $table->string('name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b121nf0_regulations');
    }
}
