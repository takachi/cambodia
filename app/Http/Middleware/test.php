<?php

namespace App\Http\Middleware;

use Closure;

class test
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth::user();
        if(auth::guest()){
            return view('login');
        }elseif($user->name != 'admin'){
            return dd('not admin');
        }
        return $next($request);
    }
}
