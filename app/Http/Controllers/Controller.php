<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\UploadedFile;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param \Illuminate\Http\UploadedFile $file
     * @param $storeLocation
     *
     * @return bool|string
     */
    public function fileUpload(UploadedFile $file = null, $storeLocation = null)
    {

        if ($file && $file->move(config('filesystems.fileUploadPath'), $file->getClientOriginalName())) {
            return 'uploads/'.$file->getClientOriginalName();
        }
        return false;
    }
}
