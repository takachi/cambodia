<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\ContentElement;
use App\Models\FaqsItem;
use App\Models\LicenseMapping;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LicenseMappingController extends Controller
{
    /**
     * LicenseMappingController constructor.
     */
    public function __construct()
    {
        view()->share('is_main_regulation_active', true);
        view()->share('is_sme_license_mapping_active', true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $licenseMappings = LicenseMapping::orderBy('ordering', 'asc')->get();
        $ceHeroBanner = ContentElement::where('type', ContentElement::HERO_BANNER)
                                      ->where('name', ContentElement::LICENSE_MAPPING)
                                      ->first();
        $cePageTitle = ContentElement::where('type', ContentElement::PAGE_TITLE)
                                     ->where('name', ContentElement::LICENSE_MAPPING)
                                     ->first();


        return view('admin.smeLicenseMapping.index', compact(
            'licenseMappings',
            'ceHeroBanner',
            'cePageTitle',
            'ceQuesAndAws'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.smeLicenseMapping.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();


        $licenseMappingId = LicenseMapping::all()->pluck('id')->last() + 1;
        $licenseMappingLocation = "licenseMapping/$licenseMappingId";

        $thumbnailUrl = $this->fileUpload($request->file('thumbnail'), $licenseMappingLocation);
        $bannerUrl = $this->fileUpload($request->file('banner'), $licenseMappingLocation);
        $diagram_enUrl = $this->fileUpload($request->file('diagram_en'), $licenseMappingLocation);
        $initiative_thumb_enUrl = $this->fileUpload($request->file('initiative_thumb_en'), $licenseMappingLocation);
        $document_enUrl = $this->fileUpload($request->file('document_en'), $licenseMappingLocation);

        $data['thumbnail'] = $thumbnailUrl;
        $data['banner'] = $bannerUrl;
        $data['diagram_en'] = $diagram_enUrl;
        $data['initiative_thumb_en'] = $initiative_thumb_enUrl;
        $data['document_en'] = $document_enUrl;
        $data['name'] = str_replace(str_split('\\/:*?"<>|& '), '-', strtolower($data['title_en']));

        $data['license_inspection'] = json_encode($data['license_inspection']);
        $data['regulation_id'] = 2;
        $data['created_at'] = Carbon::now();
        $data['updated_at'] = Carbon::now();

        if (!LicenseMapping::create($data)) {
            return redirect()->action('admin\LicenseMappingController@index')
                ->withErrors(['class' => 'alert-danger','sms' => 'License Mapping was not created.']);
        }

        return redirect()->action('admin\LicenseMappingController@index')
            ->withErrors(['class' => 'alert-success','sms' => 'License Mapping was created.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $regulationSMELicenseMapping = LicenseMapping::find($id);
        return view('admin.smeLicenseMapping.show', compact('regulationSMELicenseMapping'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $regulationSMELicenseMapping = LicenseMapping::find($id);

        return view('admin.smeLicenseMapping.edit', compact('regulationSMELicenseMapping'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        $regulationSMELicenseMapping = LicenseMapping::find($id);
        $licenseMappingLocation = "licenseMapping/$regulationSMELicenseMapping->id";
        /** todo: should remove old reference */
        if ($request->file('thumbnail')) {
            $thumbnailUrl = $this->fileUpload($request->file('thumbnail'), $licenseMappingLocation);
            $data['thumbnail'] = $thumbnailUrl;
        }
        if ($request->file('banner')) {
            $thumbnailUrl = $this->fileUpload($request->file('banner'), $licenseMappingLocation);
            $data['banner'] = $thumbnailUrl;
        }
        if ($request->file('diagram_en')) {
            $thumbnailUrl = $this->fileUpload($request->file('diagram_en'), $licenseMappingLocation);
            $data['diagram_en'] = $thumbnailUrl;
        }
        if ($request->file('initiative_thumb_en')) {
            $thumbnailUrl = $this->fileUpload($request->file('initiative_thumb_en'), $licenseMappingLocation);
            $data['initiative_thumb_en'] = $thumbnailUrl;
        }
        if ($request->file('document_en')) {
            $thumbnailUrl = $this->fileUpload($request->file('document_en'), $licenseMappingLocation);
            $data['document_en'] = $thumbnailUrl;
        }
        $data['regulation_id'] = 2;
        if (isset($data['license_inspection'])) {
            $data['license_inspection'] = json_encode($data['license_inspection']);
        }
        if (isset($data['title_en'])) {
            $data['name'] = str_replace(str_split('\\/:*?"<>|&'), '-', strtolower($data['title_en']));
        }
        $data['updated_at'] = Carbon::now();

        if ($regulationSMELicenseMapping->update($data)) {
            return redirect()->action('admin\LicenseMappingController@index')
                            ->withErrors(['class' => 'alert-success','sms' => 'License Mapping  was updated.']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $regulationSMELicenseMapping = LicenseMapping::find($id);
        $regulationSMELicenseMapping->delete();
        return redirect()->back()
            ->withErrors(['class' => 'alert-success','sms' => 'License Mapping  was deleted.']);
    }
}



