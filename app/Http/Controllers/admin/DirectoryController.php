<?php

namespace App\Http\Controllers\admin;

use App\Models\Directory;
use App\Http\Controllers\Controller;
use App\Models\Service;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Validator;
use App\Models\ContentElement as CE;

class DirectoryController extends Controller
{
    /**
     * DirectoryController constructor.
     */
    public function __construct()
    {
        view()->share('is_main_directories_active', true);
    }

    /**
     * Get a validator for an incoming directory request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        /** todo: should update */
        return Validator::make($data, [
            'title' => 'required|string|max:100',
            'phone' => 'required|string|max:11|min:7',
        ]);
    }

    // information detail
    public function information(){
        view()->share('is_information_active', true);

        $directories = Directory::orderBy('created_at', 'desc')->get();
        return view('admin.directories.information', compact('directories'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $directories = Directory::orderBy('created_at', 'desc')->get();
//        return view('admin.directories.index', compact('directories'));

        view()->share('is_directories_active', true);



        $ceHeroBanner = CE::where('type', CE::HERO_BANNER)
            ->where('name', CE::DIRECTORY)
            ->first();

        $ceSearchBox = CE::where('type', CE::SEARCH_BOX)
            ->where('name', CE::DIRECTORY)
            ->first();

        $cePageTitle = CE::where('type', CE::PAGE_TITLE)
            ->where('name', CE::DIRECTORY)
            ->first();

        return view('admin.directories.index', compact(
            'ceRegulationCategories',
            'ceHeroBanner',
            'ceSearchBox',
            'cePageTitle'
        ));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        view()->share('is_information_active', true);
        $services = Service::pluck('title_en', 'id');
        $pin = Directory::where('pin', '1')->get()->count();
        return view('admin.directories.create', compact('pin', 'services'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $request->all();
        /** todo: should validate before create new directory */
        //$validate = $this->validate($request);
        $fileUrl = $this->fileUpload($request->file('logo'));
        $data['logo'] = $fileUrl;
        $data['created_at'] = Carbon::now();
        $data['updated_at'] = Carbon::now();
        $data['name'] = str_replace(str_split('\\/:*?"<>|& '), '-', strtolower($data['title_en']));


        if (!Directory::create($data)) {
            return redirect()->action('admin\DirectoryController@index')
                ->withErrors(['class' => 'alert-danger','sms' => 'Directory was not created.']);
        }

        $directoryId = Directory::all()->pluck('id')->last();
        $directory = Directory::find($directoryId);
        $directory->services()->attach($data['service_type']);

        return redirect()->action('admin\DirectoryController@information')
            ->withErrors(['class' => 'alert-success','sms' => 'Directory was created.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Directory  $directory
     * @return \Illuminate\Http\Response
     */
    public function show(Directory $directory)
    {
        return view('admin.directories.show', compact('directory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Directory  $directory
     * @return \Illuminate\Http\Response
     */
    public function edit(Directory $directory)
    {
        view()->share('is_information_active', true);

        $services = Service::pluck('title_en', 'id');
        $pin = Directory::where('pin', '1')->get()->count();
        return view('admin.directories.edit', compact('directory', 'pin', 'services'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Directory  $directory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Directory $directory)
    {

        $data = $request->all();
        /** todo: should validate before update directory */
        //$validate = $this->validate($request);

        /** todo: should remove old image */
        if ($request->file('logo')) {
            $directoryLocation = "directory/$directory->id";
            $fileUrl = $this->fileUpload($request->file('logo'), $directoryLocation);
            $data['logo'] = $fileUrl;
        } else {
            unset($data['logo']);
        }
        $data['name'] = str_replace(str_split('\\/:*?"<>|& '), '-', strtolower($data['title_en']));
        $data['updated_at'] = Carbon::now();
        $directory->update($data);
        $directory->services()->sync($data['service_type']);

        return redirect()->action('admin\DirectoryController@information')
            ->withErrors(['class' => 'alert-success','sms' => 'Directory was updated.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Directory  $directory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Directory $directory)
    {
        $directory->delete();
        return redirect()->back()->withErrors(['class' => 'alert-success','sms' => 'Directory was deleted.']);
    }
}
