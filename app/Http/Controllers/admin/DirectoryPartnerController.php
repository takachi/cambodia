<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Directory;
use App\Models\DirectoryPartner as DirectoryPartner;
use Carbon\Carbon;
use Illuminate\Http\Request as Request;
use Illuminate\Http\Response as Response ;

class DirectoryPartnerController extends Controller
{
    /**
     * DirectoryPartnerController constructor.
     */
    public function __construct()
    {
        view()->share('is_main_directories_active', true);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $directoryId = $_GET['directory'];
        $directory = Directory::find($directoryId);
        $directoryPartner = DirectoryPartner::where('directory_id', $directoryId)->get();
        return view('admin.directoryPartner.index', compact('directory', 'directoryPartner'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $directoryId = isset($_GET['directory']) ? $_GET['directory'] : null;
        $directory = Directory::find($directoryId);
        return view('admin.directoryPartner.create', compact('directory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $fileUrl = $this->fileUpload($request->file('logo'));
        $data['logo'] = $fileUrl;
        DirectoryPartner::create($data);

        return redirect()->back()
            ->withErrors(['class' => 'alert-success','sms' => 'Directory Partner was created.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  DirectoryPartner  $directoryPartner
     * @return Response
     */
    public function show(DirectoryPartner $directoryPartner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  DirectoryPartner  $directoryPartner
     * @return Response
     */
    public function edit(DirectoryPartner $directoryPartner)
    {
        return view('admin.directoryPartner.edit', compact('directoryPartner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  DirectoryPartner  $directoryPartner
     * @return Response
     */
    public function update(Request $request, DirectoryPartner $directoryPartner)
    {
        $data = $request->all();
        if (isset($data['logo'])) {
            $fileUrl = $this->fileUpload($request->file('logo'));
            $data['logo'] = $fileUrl;
        }
        $data['updated_at'] = Carbon::now();

        $directoryPartner->update($data);

        return redirect()->back()
            ->withErrors(['class' => 'alert-success','sms' => 'Directory Partner was updated.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return Response
     */
    public function destroy($id)
    {
        $directoryPartner = DirectoryPartner::find($id);
        $directoryPartner->delete();
        return redirect()->back()
            ->withErrors(['class' => 'alert-success','sms' => 'Directory Partner was deleted.']);
    }
}
