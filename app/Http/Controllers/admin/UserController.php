<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BicUserTable;
use DB;
use Auth;
use Validator;
use Redirect;
use Datatables;
class UserController extends Controller
{
    /**
     * Constructor.
     */
    public function __construct(){
        view()->share('is_user_active', true);
    }
//For user CRUD
    public function anyData()
    {
        return Datatables::of(users::query())->make(true);
    }


    public function index()
    {
    	$users = DB::table('users')->select('*')->where('role', 'like', 'ad%')->get();

        return view('admin.administrator.user',compact('users'));
    }

    public function addUser(){
    	// show form user add
        return view('admin.administrator.user_add');
    }

    //edit user
    public function editUser($id){
        $user = BicUserTable::find($id);
        return view('admin.administrator.user_edit',compact('user'));
    }

    //show user
    public function showUser($id){

        $user = BicUserTable::find($id);
//        echo json_encode($user);exit();
        return view('admin.administrator.user_show',compact('user'));
    }

    public function createUser(Request $request){
        // insert user to database
	    $user = new BicUserTable;
        //dd($user);

	    $validator = Validator::make($request->all(), [

		        'name_en' => 'required|max:255',
		        'password_en' => 'required|min:5',
		        'confirm_password_en' => 'required_with:password|same:password_en',
                'email' => 'required|email',
                'phone_number' => 'required|min:8|numeric',

		    ]);

	    if ($validator->fails()) {
            return redirect('dashboard/user-add')
                        ->withErrors($validator)
                        ->withInput();
        	}

	    $user->name = $request->input('name_en');
	    $user->password = bcrypt($request->input('password_en'));
        $user->company_name = $request->input('company_name');
        $user->address = $request->input('address');
        $user->phone_number = $request->input('phone_number');
        $user->email = $request->input('email');
        $user->role = 'user';

	    $user->save();


	   return redirect('dashboard/user')->with('msg','Your data has been created !');
    }


    public function updateUser($id , Request $request){
		    // update user

    		$user = BicUserTable::find($id);
//    		dd($user);
            $validator = Validator::make($request->all(), [
//                'firstname_en' => 'required|max:255',
//                'lastname_en' => 'required|max:255',
                'name_en' => 'required|max:255',
                'password_en' => 'required|min:5',
                'confirm_password_en' => 'required_with:password|same:password_en',
                'email' => 'required|email',
                'phone_number' => 'required|min:8|numeric',

		    ]);

            if ($validator->fails()) {
                return redirect('dashboard/user-edit')
                    ->withErrors($validator)
                    ->withInput();
            }

	    	$user->name = $request->input('name_en');
            $user->password = bcrypt($request->input('password_en'));
	   		$user->email = $request->input('email');

	      if('' !== $request->input('password_en', '') && '' !== $request->input('confirm_password_en', '')){
	        $user->password = bcrypt($request->input('password_en'));
	      	}

	       try{
	        if($user->save()){
	          $request->session()->flash('msg', 'User successfully changed!');

	          return redirect('dashboard/user');
	        }
	      }catch(Exception $e){}

	      return back()->withInput()->with(['msg_status' => 'danger', 'msg' => 'Unable to update, please try again.']);

    }

    public function deleteUser($id , Request $request){

       	$user = BicUserTable::where('id', $id)->delete();
		//$user->update($arrayName = array('deleted' => 1));

		return redirect()->back()->with('msg','You have been deleted !');
    }


}






