<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Carousel;
use App\Models\Category;
use App\Models\ContentElement as CE;
use App\Models\Service;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    public function __construct()
    {
        view()->share('is_services_active', true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::all();
        $carousels = Carousel::where('name', Carousel::PAGES['service'])->get();
        $cePageTitle = CE::where('type', CE::PAGE_TITLE)->where('name', CE::SERVICE)->first();
        $ceSearchBox = CE::where('type', CE::SEARCH_BOX)->where('name', CE::SERVICE)->first();

        return view(
            'admin.service.index',
            compact(
                'services',
                'carousels',
                'cePageTitle',
                'ceSearchBox'
            )
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $services = Service::pluck('title_en', 'id');
        $services->prepend('---');
        return view('admin.service.create', compact('services'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $data['name'] = str_replace(str_split('\\/:*?"<>|& '), '-', strtolower($data['title_en']));

        if (!Service::create($data)) {
            return redirect()->action('admin\ServiceController@index')
                ->withErrors(['class' => 'alert-danger','sms' => 'Service was not created.']);
        }

        return redirect()->action('admin\ServiceController@index')
            ->withErrors(['class' => 'alert-success','sms' => 'Service was created.']);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        return view('admin.service.edit', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
        $data = $request->all();

        $data['updated_at'] = Carbon::now();
        $data['name'] = str_replace(str_split('\\/:*?"<>|& '), '-', strtolower($data['title_en']));

        $service->update($data);

        return redirect()->action('admin\ServiceController@index')
            ->withErrors(['class' => 'alert-success','sms' => 'Service was updated.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        if (count($service->directories)) {
            return redirect()->back()
                ->withErrors(['class' => 'alert-danger','sms' => 'Cannot delete, please remove directory first.']);
        }
        $service->delete();
        return redirect()->back()
            ->withErrors(['class' => 'alert-success','sms' => 'Service was delete.']);
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function directories($id)
    {
        $service = Service::find($id);
        $directories = $service->directories;

        return view('admin.directories.information', compact('service', 'directories'));
    }


    public function carousel($id)
    {
        $service = Service::find($id);
        return view('admin.carousel.index', ['carousels' => $service->carousels, 'service' => $service]);
    }
}
