<?php

namespace App\Http\Controllers\admin;

use App\Models\ContentElement;
use App\Models\HeroBanner;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HeroBannerController extends Controller
{
    public function __construct()
    {
        view()->share('is_herobanner_active', true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ceHeroBanners = ContentElement::where('type', ContentElement::HERO_BANNER)->orderBy('name')->get();

        return view('admin.heroBanner.index', compact('ceHeroBanners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.heroBanner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $heroBannerId = HeroBanner::all()->pluck('id')->last() + 1;
        $heroBannerLocation = "heroBanner/$heroBannerId";
        $fileUrl = $this->fileUpload($request->file('image'), $heroBannerLocation);
        $data['image'] = $fileUrl;
        $data['create_at'] = Carbon::now();
        $data['updated_at'] = Carbon::now();

//        dd($data);
        if (!HeroBanner::create($data)) {
            return redirect()->action('admin\HeroBannerController@index')
                ->withErrors(['class' => 'alert-danger','sms' => 'Hero banner was not created.']);
        }

        return redirect()->action('admin\HeroBannerController@index')
            ->withErrors(['class' => 'alert-success','sms' => 'Hero banner was created.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd('^_^ Note yet implementation');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $heroBanner = HeroBanner::find($id);
        return view('admin.heroBanner.edit', compact('heroBanner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $heroBanner = HeroBanner::find($id);

        if ($request->file('image')) {
            $heroBannerLocation = "heroBanner/$heroBanner->id";
            $fileUrl = $this->fileUpload($request->file('image'), $heroBannerLocation);
            $data['image'] = $fileUrl;
        }
        $data['updated_at'] = Carbon::now();

        if (!$heroBanner->update($data)) {
            return redirect()->action('admin\HeroBannerController@index')
                ->withErrors(['class' => 'alert-danger','sms' => 'Hero banner was not updated.']);
        }

        return redirect()->action('admin\HeroBannerController@index')
            ->withErrors(['class' => 'alert-success','sms' => 'Hero banner was created.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $heroBanner = HeroBanner::find($id);
        $heroBanner->delete();
        return redirect()->back()->withErrors(['class' => 'alert-success','sms' => 'Hero banner was deleted.']);
    }
}
