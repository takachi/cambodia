<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    protected $redirectTo = 'dashboard';

    public function index()
    {
        return view('login.login');
    }

    public function login(Request $request)
    {

        $validator = Validator::make($request->all(), [
          'username' => 'required|max:255',
          'password' => 'required|max:255',
          'remember' => 'boolean',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $username = $request->input('username');
        $password = $request->input('password');
        $remember = $request->input('remember');
//        dd($username,$password, $remember );

//        if (Auth::attempt(['name' => $username, 'password' => $password, 'status' => 1], $remember)) {
        if (Auth::attempt(['name' => $username, 'password' => $password], $remember )) {
            return redirect()->intended($this->redirectTo);
        }

        //return back()->withInput()->with('sms', 'Invalid username and password.');
        return back()->withErrors($validator)->withInput()->with('sms', 'Invalid username and password.');
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('auth.login');
    }


}





