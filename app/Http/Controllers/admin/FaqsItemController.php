<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\FaqsItem;
use App\Models\LicenseMapping;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\View\View;

class FaqsItemController extends Controller
{
    /**
     * LicenseMappingController constructor.
     */
    public function __construct()
    {
        view()->share('is_main_regulation_active', true);
        view()->share('is_sme_license_mapping_active', true);
    }

    /**
     * @param $licenseMapping_id
     *
     * @return View
     */
    public function index($licenseMapping_id)
    {
        $licenseMapping = LicenseMapping::find($licenseMapping_id);
        $faqsItems = $licenseMapping->faqsItems;
        return view('admin.faqsItem.index', compact('faqsItems', 'licenseMapping'));
    }

    /**
     * @return View
     */
    public function create($licenseMappingId)
    {
        $licenseMapping = LicenseMapping::find($licenseMappingId);
        return view('admin.faqsItem.create', compact('licenseMapping'));
    }

    /**
     * @param Request $request
     *
     * @return $this
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['created_at'] = Carbon::now();
        $data['updated_at'] = Carbon::now();
        if (!FaqsItem::create($data)) {
            return redirect()->back()->withErrors(['class' => 'alert-danger','sms' => 'FAQs was not created.']);
        }

        return redirect()->action('admin\FaqsItemController@index', $data['license_mapping_id'])
            ->withErrors(['class' => 'alert-success','sms' => 'FAQs was created.']);
    }

    public function edit($id)
    {
        $faqsItem = FaqsItem::find($id);

        return view('admin.faqsItem.edit', compact('faqsItem'));
    }

    /**
     * @param Request $request
     * @param $id
     *
     * @return $this
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $data['updated_at'] = Carbon::now();
        $faqsItem = FaqsItem::find($id);
        $faqsItem->update($data);

        return redirect()->action('admin\FaqsItemController@index', $data['license_mapping_id'])
            ->withErrors(['class' => 'alert-success','sms' => 'FAQs was updated']);
    }

    /**
     * @param FaqsItem $faqsItem
     *
     * @return $this
     */
    public function destroy(FaqsItem $faqsItem)
    {
        $faqsItem->delete();

        return redirect()->back()->withErrors(['class' => 'alert-success','sms' => 'FAQs was deleted.']);
    }

    /**
     * @param Request $request
     * @param $id
     *
     * @return $this
     */
    public function licenseMappingUpdate(Request $request, $id)
    {
        $data = $request->all();
        $data['updated_at'] = Carbon::now();
        $licenseMapping = LicenseMapping::find($id);
        $licenseMapping->update($data);

        return redirect()->action('admin\FaqsItemController@index', $licenseMapping->id)
            ->withErrors(['class' => 'alert-success','sms' => 'FAQs section title was updated']);
    }
}
