<?php

namespace App\Http\Controllers\admin;

use App\Models\ContentElement;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

class ContentElementController extends Controller
{

    /**
     * Create new regulation category content element
     * @param \Illuminate\Http\Request $request
     *
     * @return $this
     */
    public function regulationCategoryStore(Request $request)
    {
        $data = $request->all(); // Retrieving All Input Data

        $contentElementId = ContentElement::all()->pluck('id')->last() + 1;
        $contentElementLocation = "contentElement/$contentElementId";
        $image_background = $data['content']['regulation_category']['image_background'];
        $fileUrl = $this->fileUpload($image_background, $contentElementLocation);
        $data['content']['regulation_category']['image_background'] = $fileUrl;
        $data['author_id'] = Auth::id();
        $data['type'] = ContentElement::REGULATION_CATEGORY;
        $data['content'] =  json_encode($data['content']);



        if (!ContentElement::create($data)) {
            return redirect()->back()
                ->withErrors(['class' => 'alert-danger','sms' => 'Regulation category was not created.']);
        }

        return redirect()->back()
            ->withErrors(['class' => 'alert-success','sms' => 'Regulation category was created.']);
    }

    /**
     * Update regulation category content element
     * @param \Illuminate\Http\Request $request
     * @param int $id
     *
     * @return $this
     */
    public function regulationCategoryUpdate(Request $request, $id)
    {
        $data = $request->all();

        $contentElement = ContentElement::find($id);
        $regulationCategory = $data['content']['regulation_category'];

        if (isset($regulationCategory['image_background'])) {
            $contentElementLocation = "contentElement/$contentElement->id";
            $fileUrl = $this->fileUpload($regulationCategory['image_background'], $contentElementLocation);
            $data['content']['regulation_category']['image_background'] = $fileUrl;
        } else {
            $imageBackground = json_decode($contentElement->content)->regulation_category->image_background;
            $data['content']['regulation_category']['image_background'] = $imageBackground;
        }
        $data['author_id'] = Auth::id();
        $data['updated_at'] =  Carbon::now();
        $data['type'] = ContentElement::REGULATION_CATEGORY;
        $data['content'] =  json_encode($data['content']);

        if (!$contentElement->update($data)) {
            return redirect()->back()
                ->withErrors(['class' => 'alert-danger','sms' => 'Regulation category was not updated.']);
        }

        return redirect()->back()
            ->withErrors(['class' => 'alert-success','sms' => 'Regulation category was updated.']);
    }

    public function regulationCategoryDestroy($id)
    {
        $regulationCategory = ContentElement::find($id);
        $regulationCategory->delete();
        return redirect()->back()
            ->withErrors(['class' => 'alert-success','sms' => 'Regulation category was deleted.']);
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return $this
     */

    public function heroBannerStore(Request $request)
    {

        $data = $request->all();

        $data['author_id'] = Auth::id();
        $data['type'] = ContentElement::HERO_BANNER;
        $data['content'] =  json_encode($data['content']);

        if (!ContentElement::create($data)) {
            return redirect()->back()
                ->withErrors(['class' => 'alert-danger','sms' => 'Hero banner was not created.']);
        }

        return redirect()->back()
            ->withErrors(['class' => 'alert-success','sms' => 'Hero banner was created.']);
    }

    /**
     * Update regulation category content element
     * @param \Illuminate\Http\Request $request
     * @param int $id
     *
     * @return $this
     */

    public function heroBannerUpdate(Request $request, $id)
    {
        $data = $request->all();

        $contentElement = ContentElement::find($id);
        $data['author_id'] = Auth::id();
        $data['updated_at'] =  Carbon::now();
        $data['type'] = ContentElement::HERO_BANNER;
        $data['content'] =  json_encode($data['content']);

        if (!$contentElement->update($data)) {
            return Redirect::to(URL::previous() . "#hero_banner")
                ->withErrors(['class' => 'alert-danger','sms' => 'Hero banner was not updated.']);
        }
        return Redirect::to(URL::previous() . "#hero_banner")
            ->withErrors(['class' => 'alert-success','sms' => 'Hero banner was updated.']);
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function searchBoxStore(Request $request)
    {
        $data = $request->all();

        $data['author_id'] = Auth::id();
        $data['type'] = ContentElement::SEARCH_BOX;
        $data['content'] =  json_encode($data['content']);

        if (!ContentElement::create($data)) {
            return Redirect::to(URL::previous() . "#search_box")
                ->withErrors(['class' => 'alert-danger','sms' => 'Search box was not created.']);
        }

        return Redirect::to(URL::previous() . "#search_box")
            ->withErrors(['class' => 'alert-success','sms' => 'Search box was created.']);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     *
     * @return mixed
     */
    public function searchBoxUpdate(Request $request, $id)
    {
        $data = $request->all();
        $contentElement = ContentElement::find($id);

        $data['author_id'] = Auth::id();
        $data['type'] = ContentElement::SEARCH_BOX;
        $data['content'] =  json_encode($data['content']);

        if (!$contentElement->update($data)) {
            return Redirect::to(URL::previous() . "#search_box")
                ->withErrors(['class' => 'alert-danger','sms' => 'Search box was not updated.']);
        }

        return Redirect::to(URL::previous() . "#search_box")
            ->withErrors(['class' => 'alert-success','sms' => 'Search box was updated.']);
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function pageTitleStore(Request $request)
    {
        $data = $request->all();

        $data['author_id'] = Auth::id();
        $data['type'] = ContentElement::PAGE_TITLE;
        $data['content'] =  json_encode($data['content']);

        if (!ContentElement::create($data)) {
            return Redirect::to(URL::previous() . "#page_title")
                ->withErrors(['class' => 'alert-danger','sms' => 'Page title was not created.']);
        }

        return Redirect::to(URL::previous() . "#page_title")
            ->withErrors(['class' => 'alert-success','sms' => 'Page title was created.']);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     *
     * @return mixed
     */
    public function pageTitleUpdate(Request $request, $id)
    {
        $data = $request->all();
        $contentElement = ContentElement::find($id);

        $data['author_id'] = Auth::id();
        $data['type'] = ContentElement::PAGE_TITLE;
        $data['content'] =  json_encode($data['content']);

        if (!$contentElement->update($data)) {
            return Redirect::to(URL::previous() . "#page_title")
                ->withErrors(['class' => 'alert-danger','sms' => 'Page title was not updated.']);
        }

        return Redirect::to(URL::previous() . "#page_title")
            ->withErrors(['class' => 'alert-success','sms' => 'Page title was updated.']);
    }
}



