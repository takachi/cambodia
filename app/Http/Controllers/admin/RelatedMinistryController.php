<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\ContentElement;
use App\Models\Regulation;
use App\Models\RelatedMinistry;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RelatedMinistryController extends Controller
{
    /**
     * RelatedMinistryController constructor.
     */
    public function __construct()
    {
        view()->share('is_main_regulation_active', true);
        view()->share('is_related_ministry_active', true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $relatedMinistries = RelatedMinistry::orderBy('created_at', 'desc')->get();
        $ceHeroBanner = ContentElement::where('type', ContentElement::HERO_BANNER)
            ->where('name', ContentElement::RELATED_MINISTRY)
            ->first();
        $cePageTitle = ContentElement::where('type', ContentElement::PAGE_TITLE)
            ->where('name', ContentElement::RELATED_MINISTRY)
            ->first();

        return view('admin.relatedMinistry.index', compact(
            'relatedMinistries',
            'ceHeroBanner',
            'cePageTitle'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.relatedMinistry.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $fileUrl = $this->fileUpload($request->file('logo'));
        $data['logo'] = $fileUrl;
        $data['created_at'] = Carbon::now();
        $data['updated_at'] = Carbon::now();
        $data['author'] = Auth::id();
        $data['uri'] = str_replace(str_split('\\/:*?"<>|& '), '-', strtolower($data['title_en']));

        /** todo: assume that related ministry is the last */
        $data['regulation_id'] = Regulation::pluck('id')->last();

        if (!RelatedMinistry::create($data)) {
            return redirect()->action('admin\RelatedMinistryController@index')
                ->withErrors(['class' => 'alert-danger','sms' => 'Related Ministry was not created.']);
        }

        return redirect()->action('admin\RelatedMinistryController@index')
            ->withErrors(['class' => 'alert-success','sms' => 'Related Ministry was created.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RelatedMinistry  $relatedMinistry
     * @return \Illuminate\Http\Response
     */
    public function show(RelatedMinistry $relatedMinistry)
    {
        dd('^_^, Note yet implementation...');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $relatedMinistry = RelatedMinistry::find($id);
        return view('admin.relatedMinistry.edit', compact('relatedMinistry'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $relatedMinistry = RelatedMinistry::find($id);

        /** todo: should remove old logo file */
        if ($request->file('logo')) {
            $fileUrl = $this->fileUpload($request->file('logo'));
            $data['logo'] = $fileUrl;
        } else {
            unset($data['logo']);
        }
        $data['uri'] = str_replace(str_split('\\/:*?"<>|& '), '-', strtolower($data['title_en']));
        $data['updated_at'] = Carbon::now();
        $relatedMinistry->update($data);

        return redirect()->action('admin\RelatedMinistryController@index')
            ->withErrors(['class' => 'alert-success','sms' => 'Related Ministry was updated.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $relatedMinistry = RelatedMinistry::find($id);
        $relatedMinistry->delete();
        return redirect()->back()->withErrors(['class' => 'alert-success','sms' => 'Related Ministry was deleted.']);
    }
}
