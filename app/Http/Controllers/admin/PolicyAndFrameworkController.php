<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\ContentElement;
use App\Models\PolicyAndFramework;
use App\Models\Regulation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PolicyAndFrameworkController extends Controller
{
    public function __construct()
    {
        view()->share('is_main_regulation_active', true);
        view()->share('is_sme_policy_frameworks_active', true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $smePolicies = PolicyAndFramework::orderBy('created_at', 'desc')->get();
        $ceHeroBanner = ContentElement::where('type', ContentElement::HERO_BANNER)
                                        ->where('name', ContentElement::SME_POLICY)
                                        ->first();
        $cePageTitle = ContentElement::where('type', ContentElement::PAGE_TITLE)
                                        ->where('name', ContentElement::SME_POLICY)
                                        ->first();

        return view('admin.smePolicy.index', compact(
            'smePolicies',
            'ceHeroBanner',
            'cePageTitle'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $status = PolicyAndFramework::where('status', '1')->get()->count();
        return view('admin.smePolicy.create', compact('status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $smePolicyId = PolicyAndFramework::all()->pluck('id')->last() + 1;
        $smePolicyLocation = "regulation/smePolicy/$smePolicyId";
        $fileUrl = $this->fileUpload($request->file('reference_en'), $smePolicyLocation);
        $data['reference_en'] = $fileUrl;
        $data['created_at'] = Carbon::now();
        $data['updated_at'] = Carbon::now();
        $data['regulation_id'] = Regulation::pluck('id')->first();
        $data['author'] = Auth::id();

        if (!PolicyAndFramework::create($data)) {
            return redirect()->action('admin\PolicyAndFrameworkController@index')
                ->withErrors(['class' => 'alert-danger','sms' => 'SME Policy was not created.']);
        }

        return redirect()->action('admin\PolicyAndFrameworkController@index')
            ->withErrors(['class' => 'alert-success','sms' => 'SME Policy was created.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PolicyAndFramework $sMEPolicy
     *
     * @return \Illuminate\Http\Response
     */
    public function show(PolicyAndFramework $sMEPolicy)
    {
        dd('Not yet implement');
    }

    /**
     * Show the form for editing the specified resource.
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $smePolicy = PolicyAndFramework::find($id);
        $status = PolicyAndFramework::where('status', '1')->get()->count();
        return view('admin.smePolicy.edit', compact('smePolicy', 'status'));
    }

    /**
     * Update the specified resource in storage.
     * @param \Illuminate\Http\Request $request
     * @param $id
     *
     * @return $this
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $smePolicy = PolicyAndFramework::find($id);

        /** todo: should remove old reference */
        if ($request->file('reference_en')) {
            $smePolicyLocation = "regulation/smePolicy/$smePolicy->id";
            $fileUrl = $this->fileUpload($request->file('reference_en'), $smePolicyLocation);
            $data['reference_en'] = $fileUrl;
        }
        $data['updated_at'] = Carbon::now();
        $smePolicy->update($data);

        return redirect()->action('admin\PolicyAndFrameworkController@index')
            ->withErrors(['class' => 'alert-success','sms' => 'SME Policy was updated.']);
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     *
     * @return $this
     */
    public function destroy($id)
    {
        $smePolicy = PolicyAndFramework::find($id);
        /** todo: should remove reference file also */
        $smePolicy->delete();
        return redirect()->back()->withErrors(['class' => 'alert-success','sms' => 'SME Policy was deleted.']);
    }
}
