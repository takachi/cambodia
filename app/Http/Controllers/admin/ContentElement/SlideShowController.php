<?php

namespace App\Http\Controllers\admin\ContentElement;

use App\Http\Controllers\Controller;
use App\Models\ContentElement as CE;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

class SlideShowController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return $this
     */
    public function store(Request $request)
    {
        $data = $request->all();


        $image = $data['content']['slide_show'];
        $fileUrl = $this->fileUpload($image);
        $data['content']['slide_show'] = $fileUrl;
        $data['author_id'] = Auth::id();
        $data['type'] = CE::SLIDE_SHOW;
        $data['content'] =  json_encode($data['content']);

        if (!CE::create($data)) {
            return Redirect::to(URL::previous() . "#slide_show")
                ->withErrors(['class' => 'alert-danger','sms' => 'Slide show did not create.']);
        }
        return Redirect::to(URL::previous() . "#slide_show")
            ->withErrors(['class' => 'alert-success','sms' => 'Slide show created.']);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        $ce = CE::find($id);

        $data['author_id'] = Auth::id();
        $data['type'] = CE::SLIDE_SHOW;

        if (isset($data['content']['slide_show'])) {
            $fileUrl = $this->fileUpload($data['content']['slide_show']);
            $data['content']['slide_show'] = $fileUrl;
        } else {
            $data['content']['slide_show'] = json_decode($ce->content)->slide_show;
        }

        $data['content'] =  json_encode($data['content']);

        if (!$ce->update($data)) {
            return Redirect::to(URL::previous() . "#slide_show")
                ->withErrors(['class' => 'alert-danger','sms' => 'Slide show did not updated.']);
        }
        return Redirect::to(URL::previous() . "#slide_show")
            ->withErrors(['class' => 'alert-success','sms' => 'Slide show updated.']);
    }

    /**
     * Destroy Content Element Slide show
     * @param $id
     *
     * @return $this
     */
    public function destroy($id)
    {
        $slideShow = CE::find($id);
        $slideShow->delete();
        return Redirect::to(URL::previous() . "#slide_show")
            ->withErrors(['class' => 'alert-success','sms' => 'Slide show deleted']);
    }

}
