<?php

namespace App\Http\Controllers\admin;

use App\Models\ContentElement as CE;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BicRegulationTable;

class RegulationController extends Controller
{
    public function __construct(){
        view()->share('is_main_regulation_active', true);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        view()->share('is_regulation_active', true);

        $ceRegulationCategories = CE::where('type', CE::REGULATION_CATEGORY)->get();

        $ceHeroBanner = CE::where('type', CE::HERO_BANNER)
                        ->where('name', CE::REGULATION)
                        ->first();

        $ceSearchBox = CE::where('type', CE::SEARCH_BOX)
                       ->where('name', CE::REGULATION)
                       ->first();

        $cePageTitle = CE::where('type', CE::PAGE_TITLE)
                       ->where('name', CE::REGULATION)
                       ->first();

        return view('admin.regulation.index', compact(
            'ceRegulationCategories',
            'ceHeroBanner',
            'ceSearchBox',
            'cePageTitle'
        ));
    }

    //    REGULATION
    public function Regulation()
    {
        view()->share('is_regulation_active', true);
        return view('admin.regulation.regulation_ladding_page');
    }
    public function Regulation_add(){
        return view('admin.regulation.regulation_ladding_page_add');
    }
     public function Regulation_create(Request $request){


        $this->validate($request, [
            'regulation_TitleEn' => 'required|min:2',
            //'regulation_Logo_en' => 'required|image|mimes:jpeg,jpg,png,svg,gif',

        ]);

        $title = new BicRegulationTable;

         $title->title_en = $request->input('regulation_TitleEn');


                $image = $request->file('regulation_Logo_en');
                $name = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads');
                $image->move($destinationPath, $name);
                $this->save();

                return back()->with('success','Image Upload successfully');


    }



}




