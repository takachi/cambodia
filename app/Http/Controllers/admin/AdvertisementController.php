<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Advertisement;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AdvertisementController extends Controller
{
    public function __construct()
    {
        view()->share('is_advertisement_active', true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $advertisements = Advertisement::orderBy('page')->get();
        if (isset($_GET['service'])) {
            $advertisements = Advertisement::where('page', $_GET['service'])->get();
        }
        return view('admin.advertisement.index', compact('advertisements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.advertisement.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $fileUrl = $this->fileUpload($request->file('image'));
        $data['image'] = $fileUrl;
        $data['create_at'] = Carbon::now();
        $data['updated_at'] = Carbon::now();

        $dateFormat = 'd-m-Y H:i:s';
        $data['active_at'] =  Carbon::createFromFormat($dateFormat, $data['active_at'].' 00:00:00');
        $data['expired_at'] =  Carbon::createFromFormat($dateFormat, $data['expired_at'].' 23:59:59');
        if (isset($data['service'])) {
            $data['page'] = $data['service'];

        } else {
            $data['page'] = array_values(Advertisement::PAGES)[$data['page']];
        }

        if (!Advertisement::create($data)) {
            return redirect()->back()
                ->withErrors(['class' => 'alert-danger','sms' => 'Advertisement was not created.']);
        }

        return redirect()->back()
            ->withErrors(['class' => 'alert-success','sms' => 'Advertisement was created.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd('^_^ Note yet implementation');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $advertisement = Advertisement::find($id);
        return view('admin.advertisement.edit', compact('advertisement'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $advertisement = Advertisement::find($id);

        if ($request->file('image')) {
            $fileUrl = $this->fileUpload($request->file('image'));
            $data['image'] = $fileUrl;
        } else {
            unset($data['image']);
        }
        $dateFormat = 'd-m-Y H:i:s';
        $data['active_at'] =  Carbon::createFromFormat($dateFormat, $data['active_at'].' 00:00:00');
        $data['expired_at'] =  Carbon::createFromFormat($dateFormat, $data['expired_at'].' 23:59:59');
        $data['updated_at'] = Carbon::now();
        if (isset($data['service'])) {
            $data['page'] = $data['service'];

        } else {
            $data['page'] = array_values(Advertisement::PAGES)[$data['page']];
        }

        if (!$advertisement->update($data)) {
            return redirect()->action('admin\AdvertisementController@index')
                ->withErrors(['class' => 'alert-danger','sms' => 'Advertisement was not updated.']);
        }

        return redirect()->action('admin\AdvertisementController@index')
            ->withErrors(['class' => 'alert-success','sms' => 'Advertisement was updated.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $advertisement = Advertisement::find($id);
        $advertisement->delete();
        return redirect()->back()->withErrors(['class' => 'alert-success','sms' => 'Advertisement was deleted.']);
    }
}
