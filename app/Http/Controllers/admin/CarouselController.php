<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Carousel;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CarouselController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $fileUrl = $this->fileUpload($request->file('image'));
        $data['image'] = $fileUrl;
        $data['created_at'] = Carbon::now();
        $data['updated_at'] = Carbon::now();

        if (!Carousel::create($data)) {
            return redirect()->back()
                ->withErrors(['class' => 'alert-danger','sms' => 'Carousel was not created.']);
        }

        return redirect()->back()
            ->withErrors(['class' => 'alert-success','sms' => 'Carousel was created.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $carousel = Carousel::find($id);
        $data = $request->all();

//        dd($data);

        if ($request->file('image')) {
            $fileUrl = $this->fileUpload($request->file('image'));
            $data['image'] = $fileUrl;
        }
        $data['updated_at'] = Carbon::now();

        $carousel->update($data);

        return redirect()->back()
            ->withErrors(['class' => 'alert-success','sms' => 'Carousel was updated.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $carousel = Carousel::find($id);
        $carousel->delete();

        return redirect()->back()
            ->withErrors(['class' => 'alert-success','sms' => 'Carousel was delete.']);
    }
}
