<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ShopTable;

class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function test()
    {
        return view('admin.hotel.test');
    }


    public function __construct()
    {
        view()->share('is_hotel_active', true);
    }


    public function index()
    {
        view()->share('is_hotel_active', true);

        $item = ShopTable::Testing();
//        dd($item);
        return view('admin.hotel.index', compact(
            'item'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        view()->share('is_hotel_active', true);
        return view('admin.hotel.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $items = $request->all();
        dd($items);
        $items['shop_name']= $request->get('shopName_en');
        $items['shop_desen']= $request->get('des_en');
        $items['phone']= $request->get('phone');
        $items['address']= $request->get('address');
        $items['facebook']= $request->get('facebook');
        $items['google']= $request->get('google');
        $items['map']= $request->get('map');

        $items['shop_cat_id'] = '3';
     //   $items['images'] = $request->get('thumbnail');

      //  $item['name'] = str_replace(str_split('\\/:*?"<>|& '), '-', strtolower($item['title_en']));

        if (!ShopTable::create($items)) {
            return redirect()->action('admin\HotelController@index')
                ->withErrors(['class' => 'alert-danger','sms' => 'News and events was not created.']);
        }

        return redirect()->action('admin\HotelController@index')
            ->withErrors(['class' => 'alert-success','sms' => 'News and events was created.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $items = ShopTable::find($id);
        view()->share('is_hotel_active', true);
        return view('admin.hotel.edit', compact('items'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $items = ShopTable::find($id);

        $items['shop_name']= $request->get('shopName_en');
        $items['shop_desen']= $request->get('des_en');
        $items['phone']= $request->get('phone');
        $items['address']= $request->get('address');
        $items['facebook']= $request->get('facebook');
        $items['google']= $request->get('google');
        $items['map']= $request->get('map');

        if ($request->get('images') != ""){
            $items['images'] = $request->get('images');
        }else{
            $items['images'] = $items->images;
        }

        $items->save();

        return redirect()->action('admin\HotelController@index')
            ->withErrors(['class' => 'alert-success','sms' => 'News and events was updated.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $items = ShopTable::find($id);
        $items->delete();
        return redirect()->back()
            ->withErrors(['class' => 'alert-success','sms' => 'News and events was delete.']);
    }
}
