<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Service;
use App\Models\Setting;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    /**
     * FrontendController constructor.
     */
    public function __construct()
    {

        $topHeaderLabel = json_decode(Setting::where('name', Setting::$settings['topHeaderLabel'])->first()->content);
        $logo = json_decode(Setting::where('name', Setting::$settings['logo'])->first()->content);
        $bizInfo = json_decode(Setting::where('name', Setting::$settings['bizInfo'])->first()->content);
        $bizContact = json_decode(Setting::where('name', Setting::$settings['bizContact'])->first()->content);
        $categories = Category::where('parent', 0)->orderBy('ordering', 'desc')->get();
        $subCategories = Category::where('parent', '!=', 0)->get();
        $socialMedia = json_decode(Setting::where('name', Setting::$settings['socialMedia'])->first()->content);


//        dd($categories, $subCategories);

        view()->share(compact(
            'topHeaderLabel',
            'logo',
            'bizInfo',
            'bizContact',
            'socialMedia',
            'categories',
            'subCategories'
        ));
    }
}
