<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cookie;
use DB;

class HomeController extends Controller
{

    /*
     * Landing page (index page)
     */
    public function landingPage()
    {
         $hotels      = DB::table('shop')->where('shop_cat_id', 3)->limit(8)->get();
         $restaurents = Db::table('shop')->where('shop_cat_id', 2)->limit(8)->get();
         $shops       = Db::table('shop')->where('shop_cat_id', 1)->limit(8)->get();

        return   view('frontend.desktop.index',compact('hotels', 'restaurents', 'shops') );

    }


    public function hotel()
    {

    }
}




