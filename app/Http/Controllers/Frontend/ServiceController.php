<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\FrontendController;
use App\Models\Advertisement;
use App\Models\Carousel;
use App\Models\Service;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\ContentElement as CE;

class ServiceController extends FrontendController
{
    /**
     * Landing page (index page)
     * @return \Illuminate\View\View
     */
    public function landingPage()
    {
        $advertisements = Advertisement::where('page', Advertisement::PAGES['service'])
                                       ->where('active_at', '<=', Carbon::now()->startOfDay())
                                       ->where('expired_at', '>=', Carbon::now()->endOfDay())
                                       ->limit(3)->get();
        $cePageTitle = CE::where('type', CE::PAGE_TITLE)->where('name', CE::SERVICE)->first();
        $pageTitle = isset($cePageTitle) ? json_decode($cePageTitle->content)->page_title : 'Service';
        $carousels = Carousel::where('name', Carousel::PAGES['service'])->get();
        $ceSearchBox = CE::where('type', CE::SEARCH_BOX)->where('name', CE::SERVICE)->first();
        $searchBox = isset($ceSearchBox) ? json_decode($ceSearchBox->content)->search_box : null;
        $services = Service::orderBy('ordering', 'desc')->get();

        return view(
            baseview(0).'-service',
            compact(
                'searchBox',
                'carousels',
                'services',
                'pageTitle',
                'advertisements'
            )
        );
    }

    /**
     * @param $name
     * @return \Illuminate\View\View
     */
    public function serviceList($name)
    {
        $companyName = '';
        $sort = '';
        $service = Service::where('name', $name)->first();
        $advertisements = Advertisement::where('page', $service->name)
                                       ->where('active_at', '<=', Carbon::now()->startOfDay())
                                       ->where('expired_at', '>=', Carbon::now()->endOfDay())
                                       ->limit(3)->get();
        $services = Service::orderBy('ordering', 'desc')->get();
        $carousels = Carousel::where('name', $service->name)->get();

        if (isset($_REQUEST['company_name'])) {
            $companyName = $_REQUEST['company_name'];
            $sort = $_REQUEST['sort'];
            $services = Service::where('title_en', 'like', '%'.$companyName.'%')
                               ->select('*')
                               ->orderBy('title_en', $sort)
                               ->paginate(12);
        }

        return view(
            baseview(0).'-service-list',
            compact(
                'sort',
                'companyName',
                'carousels',
                'services',
                'service',
                'advertisements'
            )
        );
    }

	/*
	 * List page
	 */
    public function listPage($submenu)
	{
		//if(Cookie::get('bizinfo') == NULL){ return redirect('/'); }

//        dd($submenu);

		$companyname = '';
		$sort = '';
		$title = get_title(3,'category');
		$category = DB::select("call get_category('$submenu', '".get_current_locale()."')");
		$advertisement = DB::table('b121nf0_advertisements')->select('*')->where('section', '=', 1)->get();
		$advertisement_slide = DB::table('b121nf0_advertisements')->select('*')->where('section', '=', 2)->get();
		$sub_category = DB::table('b121nf0_category')->select('*')->where('parent', '=', 2)->get();



		if(isset($_REQUEST['companyname'])):
			$companyname = $_REQUEST['companyname'];
			$sort = $_REQUEST['sort'];

			$service = DB::table('b121nf0_service')
				->join('b121nf0_directory', 'b121nf0_service.directory_id', '=', 'b121nf0_directory.id')
				->join('b121nf0_media', 'b121nf0_directory.logo', '=', 'b121nf0_media.id')
				->where('url','<>','')
				->where('category','=',$category[0]->sub_id)
				->where('title','like','%'.$companyname.'%')
				->select('*')
				->orderBy('title', $sort)
				->paginate(12);

		else:
//			$service = DB::table('b121nf0_service')
//				->join('b121nf0_directory', 'b121nf0_service.directory_id', '=', 'b121nf0_directory.id')
//				->join('b121nf0_media', 'b121nf0_directory.logo', '=', 'b121nf0_media.id')
//				->where('url','<>','')
//				->where('category','=',$category[0]->sub_id)
//				->select('*')
//				->paginate(12);
		endif;

        $serviceListAdvertisements = Advertisement::where('page', Advertisement::PAGES['service-list'])
                                              ->where('active_at', '<=', Carbon::now()->startOfDay())
                                              ->where('expired_at', '>=', Carbon::now()->endOfDay())
                                              ->limit(3)->get();

		//dd($service); exit();

		// Point to view layout

        $service = Service::where('name', $submenu)->first();

//        dd($service);


		return view(baseview(0).'-service-list', compact('serviceListAdvertisements','title','category','advertisement','sub_category','advertisement_slide','submenu','service','companyname','sort'));
	}

	/*
	 * Detail page
	 */
    public function detailPage($submenu, $pagename)
	{

	}

}
