<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
class ShopController extends Controller
{
    public function index()
    {
        $shops = DB::table('shop')->where('shop_cat_id', 1)->paginate(5);
        return view('frontend.desktop.shop',compact('shops') );

    }
}
