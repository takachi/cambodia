<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use DB;

class SearchController extends Controller
{
	
    /* 
	 * Landing page (index page)
	 */
    public function landingPage()
	{
		$s = Input::get( 'search_key' );
		
		$category = DB::table('b121nf0_directory')->select('*')->where('name','like','%'.$s.'%')->paginate(12);  //dd($category); exit();
		
		return view(baseview(0).'-search', compact('category'));	
	}
	
}
