<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cookie;
use DB;

class NewsEventsController extends Controller
{
	
    /* 
	 * Landing page (index page)
	 */
    public function landingPage()
	{
		//if(Cookie::get('bizinfo') == NULL){ return redirect('/'); }
		
		$title = get_title(8,'category');
		$advertisement = DB::table('b121nf0_advertisement')->select('*')->where('section', '=', 1)->get();
		$news_upcoming = DB::table('b121nf0_news_events')->select('*')->where('category', '=', 10)->limit(3)->get();
		$news_bic = DB::table('b121nf0_news_events')->select('*')->where('category', '=', 11)->limit(3)->get();
		$news_industry_events = DB::table('b121nf0_news_events')->select('*')->where('category', '=', 9)->limit(3)->get();
		
		// Point to view layout
		return view(baseview(0).'-news-events', compact('title','advertisement','news_upcoming','news_bic','news_industry_events'));  
	}
	
	/* 
	 * List page
	 */
    public function listPage($submenu)
	{
		//if(Cookie::get('bizinfo') == NULL){ return redirect('/'); }
		
		$title = get_title(8,'category'); 
		$category = DB::select("call get_category('$submenu', '".get_current_locale()."')"); 
		$advertisement = DB::table('b121nf0_advertisement')->select('*')->where('section', '=', 1)->get();
		
		$news = DB::table('b121nf0_news_events')->select('*')->where('category', '=', $category[0]->sub_id)->paginate(12); 
		
		// Point to view layout
		return view(baseview(0).'-news-events-list', compact('title','category','advertisement','submenu','news')); 
	}
	
	/* 
	 * Detail page
	 */
    public function detailPage($submenu, $pagename)
	{ 
		$title = get_title(8,'category'); 
		$news = DB::table('b121nf0_news_events')->select('*')->where('name', '=', $pagename)->get(); // dd($news); exit();
		
		// Point to view layout
		return view(baseview(0).'-news-events-detail', compact('title','submenu', 'news')); 
	}
	
}