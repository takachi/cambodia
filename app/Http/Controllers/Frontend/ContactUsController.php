<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cornford\Googlmapper\Facades\MapperFacade as Mapper;
use Cookie;
use DB;

class ContactUsController extends Controller
{
	
    /* 
	 * Landing page (index page)
	 */
    public function landingPage()
	{
		//if(Cookie::get('bizinfo') == NULL){ return redirect('/'); }
		
		$title = get_title(3);
		
		$map_data 	= DB::select("SELECT * FROM b121nf0_contact_us WHERE id=1"); 		
		$latitude  	= $map_data[0]->latitude;
		$longitude	= $map_data[0]->longitude;
		$zoom 		= $map_data[0]->zoom;
		$content	= get_option('sitename', 99);
		Mapper::map($latitude, $longitude, ['marker' => false, 'zoom' => $zoom, 'scrollWheelZoom'=>false]);
		Mapper::informationWindow($latitude, $longitude, $content, ['open' => true]);

		// Point to view layout
		return view(baseview(0).'-contact-us', compact('title','map_data')); 
	}
	
}