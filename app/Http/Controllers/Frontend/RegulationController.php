<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FrontendController;
use App\Models\Advertisement;
use App\Models\ContentElement as CE;
use App\Models\LicenseMapping;
use App\Models\RelatedMinistry;
use App\Models\PolicyAndFramework;
use App\Models\CategoryTable;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class RegulationController extends FrontendController
{
    /**
     * Return to landing page
     * @return \Illuminate\View\View
     */
    public function landingPage()
    {
        $cePageTitle = CE::where('type', CE::PAGE_TITLE)->where('name', CE::REGULATION)->first();
        $pageTitle = json_decode($cePageTitle->content)->page_title;
        $ceHeroBanner = CE::where('type', CE::HERO_BANNER)->where('name', CE::REGULATION)->first();
        $heroBanner = json_decode($ceHeroBanner->content)->hero_banner;
        $ceSearchBox = CE::where('type', CE::SEARCH_BOX)->where('name', CE::REGULATION)->first();
        $searchBox = json_decode($ceSearchBox->content)->search_box;
        $regulationCategories = CE::where('type', CE::REGULATION_CATEGORY)->get();
       // $licenseMappings = LicenseMapping::all();
        //$regulationCategories = CategoryTable::all();
        //dd($regulationCategories);
        $licenseMappings = LicenseMapping::all();

        return view(
            baseview(0).'-regulation',
            compact(
                'pageTitle',
                'heroBanner',
                'searchBox',
                'regulationCategories',
                'licenseMappings'
            )
        );
    }

    /**
     * Related ministry
     * @return \Illuminate\View\View
     */
    public function relatedMinistry()
    {
        $relatedMinistries = RelatedMinistry::all();
        $cePageTitle = CE::where('type', CE::PAGE_TITLE)->where('name', CE::RELATED_MINISTRY)->first();
        $pageTitle = json_decode($cePageTitle->content)->page_title;
        $ceHeroBanner = CE::where('type', CE::HERO_BANNER)->where('name', CE::RELATED_MINISTRY)->first();
        $heroBanner = json_decode($ceHeroBanner->content)->hero_banner;

        return view(
            baseview(0).'-regulation-list-related-ministry',
            compact(
                'relatedMinistries',
                'pageTitle',
                'heroBanner'
            )
        );
    }

    /**
     * SME License and Mapping
     * @return \Illuminate\View\View
     */
    public function licenseMapping()
    {
        $licenseMappings = LicenseMapping::orderBy('ordering', 'asc')->get();
        $cePageTitle = CE::where('type', CE::PAGE_TITLE)->where('name', CE::LICENSE_MAPPING)->first();
        $pageTitle = isset($cePageTitle) ? json_decode($cePageTitle->content)->page_title : 'License Mapping';
        $ceHeroBanner = CE::where('type', CE::HERO_BANNER)->where('name', CE::LICENSE_MAPPING)->first();
        $heroBanner = isset($ceHeroBanner) ? json_decode($ceHeroBanner->content)->hero_banner : null;
        $advertisements = Advertisement::where('page', Advertisement::PAGES['licence-mapping'])
                                       ->where('active_at', '<=', Carbon::now()->startOfDay())
                                       ->where('expired_at', '>=', Carbon::now()->endOfDay())
                                       ->limit(3)->get();

        return view(
            baseview(0).'-regulation-list-sme-license-mapping',
            compact(
                'licenseMappings',
                'pageTitle',
                'heroBanner',
                'advertisements'
            )
        );
    }

    /**
     * Policy and Framework
     * @return \Illuminate\View\View
     */
    public function policyAndFramework()
    {
        $policyAndFrameworks = PolicyAndFramework::all();
        $cePageTitle = CE::where('type', CE::PAGE_TITLE)->where('name', CE::SME_POLICY)->first();
        $pageTitle = json_decode($cePageTitle->content)->page_title;
        $ceHeroBanner = CE::where('type', CE::HERO_BANNER)->where('name', CE::SME_POLICY)->first();
        $heroBanner = json_decode($ceHeroBanner->content)->hero_banner;

        return view(
            baseview(0).'-regulation-list-cambodia-industry-development-policy',
            compact(
                'policyAndFrameworks',
                'pageTitle',
                'heroBanner'
            )
        );
    }

    /**
     * @param $name
     *
     * @return \Illuminate\View\View
     */
    public function licenseMappingItem($name)
    {
        $licenseMapping = LicenseMapping::where('name', $name)->first();
        $ceHeroBanner = CE::where('type', CE::HERO_BANNER)->where('name', CE::SME_POLICY)->first();
        $heroBanner = json_decode($ceHeroBanner->content)->hero_banner;

        return view(
            baseview(0).'-regulation-detail',
            compact(
                'licenseMapping',
                'heroBanner'
            )
        );
    }



    /**
     * @param $submenu
     *
     * @return \Illuminate\View\View
     */
    public function listPage($submenu)
    {

        if ($submenu === '')

//        dd($submenu);

//		$title = get_title(1,'category');
//		$category 		= DB::select("call get_category('$submenu', '".get_current_locale()."')");
//		$data 			= DB::select("call get_regulation('".$category[0]->sub_name."')");
//        $advertisement 	= DB::table('b121nf0_advertisement')->select('*')->where('section', '=', 1)->get();

        // Condtion only this three page
//		if(in_array($submenu,array("cambodia-industry-development-policy", "sme-license-mapping", "related-ministry"))){
//			$submenu = '-'.$submenu;
//		}else{ $submenu = ''; }

        $policyAndFrameworks = PolicyAndFramework::where('status', 1)->get();
        //dd($policyAndFrameworks);
        $cePolicyAndFrameworkPageTitle = CE::where('type', CE::PAGE_TITLE)
                                           ->where('name', CE::SME_POLICY)
                                           ->first();
        $policyAndFrameworkPageTitle = json_decode($cePolicyAndFrameworkPageTitle->content)->page_title;
        $cePolicyAndFrameworkHeroBanner = CE::where('type', CE::HERO_BANNER)
                                            ->where('name', CE::SME_POLICY)
                                            ->first();
        $policyAndFrameworkHeroBanner = json_decode($cePolicyAndFrameworkHeroBanner->content)->hero_banner;



        $relatedMinistries = RelatedMinistry::all();
        $ceRelatedMinistryPageTitle = CE::where('type', CE::PAGE_TITLE)
                                      ->where('name', CE::RELATED_MINISTRY)
                                      ->first();
        $relatedMinistryPageTitle = json_decode($ceRelatedMinistryPageTitle->content)->page_title;
        $ceRelatedMinistryHeroBanner = CE::where('type', CE::HERO_BANNER)
                                       ->where('name', CE::RELATED_MINISTRY)
                                       ->first();
        $relatedMinistryHeroBanner = json_decode($ceRelatedMinistryHeroBanner->content)->hero_banner;

//        $heroBannerLicenseMapping = $relatedMinistryHeroBanner;

//        $heroBanner = CE::where('type', CE::HERO_BANNER)->where('name', CE::REGULATION)->first();


        $ceLicenseMappingPageTitle = CE::where('type', CE::PAGE_TITLE)
                                       ->where('name', CE::LICENSE_MAPPING)
                                       ->first();
        $licenseMappingPageTitle = json_decode($ceLicenseMappingPageTitle->content)->page_title;
        $ceLicenseMappingHeroBanner = CE::where('type', CE::HERO_BANNER)
                                        ->where('name', CE::RELATED_MINISTRY)
                                        ->first();
        $licenseMappingHeroBanner = json_decode($ceLicenseMappingHeroBanner->content)->hero_banner;
        $licenseMappings = LicenseMapping::orderBy('ordering', 'asc')->get();
        $licenseMappingAdvertisements = Advertisement::where('page', Advertisement::PAGES['sme-licence-mapping'])
                                                     ->where('active_at', '<=', Carbon::now()->startOfDay())
                                                     ->where('expired_at', '>=', Carbon::now()->endOfDay())
                                                     ->limit(3)->get();


        // Point to view layout
        return view(baseview(0).'-regulation-list-'.$submenu, compact(
            'title',
            'category',
            'data',
            'advertisement',
            '',
            'policyAndFrameworkPageTitle',
            'policyAndFrameworkHeroBanner',
            'policyAndFrameworks',
            '',
            'relatedMinistries',
            'relatedMinistryPageTitle',
            'relatedMinistryHeroBanner',
            '',
            'licenseMappingPageTitle',
            'licenseMappingHeroBanner',
            'licenseMappingAdvertisements',
            'licenseMappings'
        ));
    }

    /*
     * Detail page
     */
    public function detailPage($submenu,$pagename)
    {
        //if(Cookie::get('bizinfo') == NULL){ return redirect('/'); }

//        $content_one	= DB::select("
//            SELECT * FROM b121nf0_regulations_sme_license_mapping
//            INNER JOIN b121nf0_regulations_sme_license_mapping_content
//            ON b121nf0_regulations_sme_license_mapping.id = b121nf0_regulations_sme_license_mapping_content.post_id
//            where name = '$pagename' and group_id = 1
//        ");
//
//
//        $content_meta_one 	= DB::select("
//            SELECT * FROM b121nf0_regulations_sme_license_mapping
//            INNER JOIN b121nf0_regulations_sme_license_mapping_content_meta
//            ON b121nf0_regulations_sme_license_mapping.id = b121nf0_regulations_sme_license_mapping_content_meta.post_id
//            where name = '$pagename' and group_id = 1
//        ");
//
//        $content_two	= DB::select("
//            SELECT * FROM b121nf0_regulations_sme_license_mapping
//            INNER JOIN b121nf0_regulations_sme_license_mapping_content
//            ON b121nf0_regulations_sme_license_mapping.id = b121nf0_regulations_sme_license_mapping_content.post_id
//            where name = '$pagename' and group_id = 2
//        ");
//
//        $content_meta_two 	= DB::select("
//            SELECT * FROM b121nf0_regulations_sme_license_mapping
//            INNER JOIN b121nf0_regulations_sme_license_mapping_content_meta
//            ON b121nf0_regulations_sme_license_mapping.id = b121nf0_regulations_sme_license_mapping_content_meta.post_id
//            where name = '$pagename' and group_id = 2
//        ");
//
//        //		$title = get_title(1,'category');
//        //		$category 	= DB::select("call get_category('$submenu', '".get_current_locale()."')");
//        //		$document	= DB::select("SELECT document_en, document_km, faqs_description_en, faqs_description_km FROM b121nf0_regulations_sme_license_mapping where name = '$pagename'");
//        // 		$faqs 		= DB::table('b121nf0_faqs')->select('*')->where('post_id','=', $content_one[0]->post_id)->get();

        $licenseMapping = LicenseMapping::where('name', $pagename)->first();

//        dd($licenseMapping);

        // Point to view layout
        return view(baseview(0).'-regulation-detail', compact(
            'title',
            'category',
            'content_one',
            'content_meta_one',
            'content_two',
            'content_meta_two'
            ,'document',
            'faqs',
            'pagename',
            'licenseMapping'
        ));
    }

}
?>














