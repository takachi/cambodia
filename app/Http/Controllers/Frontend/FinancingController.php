<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cookie;
use DB;

class FinancingController extends Controller
{
	
    /* 
	 * Landing page (index page)
	 */
    public function landingPage()
	{
		//if(Cookie::get('bizinfo') == NULL){ return redirect('/'); }
		
		$title = get_title(5,'category');
		
		// Point to view layout
		return view(baseview(0).'-financing', compact('title')); 
	}
	
	/* 
	 * List page
	 */
    public function listPage($submenu)
	{
		//if(Cookie::get('bizinfo') == NULL){ return redirect('/'); }
		
		$title = get_title(5,'category');
		$category = DB::select("call get_category('$submenu', '".get_current_locale()."')"); 
		
		// Point to view layout
		return view(baseview(0).'-financing-list', compact('title','category','submenu')); 
	}
	
	/* 
	 * Detail page
	 */
    public function detailPage($submenu, $pagename)
	{ 
		
	}
	
}