<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cookie;
use DB;

class AboutUsController extends Controller
{
	
    /* 
	 * Landing page (index page)
	 */
    public function landingPage()
	{
		//if(Cookie::get('bizinfo') == NULL){ return redirect('/'); }
		
		$title = get_title(2);
		$data = DB::table('b121nf0_about_us')->select('*')->limit(1)->get();
		
		// Point to view layout
		return view(baseview(0).'-about-us', compact('title','data')); 
	}
	
}