<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\FrontendController;
use App\Models\Advertisement;
use App\Models\Directory;
use Carbon\Carbon;
use Cornford\Googlmapper\Facades\MapperFacade as Mapper;
use App\Models\ContentElement as CE;
use Illuminate\Support\Facades\DB;

class DirectoryController extends FrontendController
{

    public function __construct()
    {
        parent::__construct();
        $advertisements = Advertisement::where('page', Advertisement::PAGES['directory'])
                                       ->where('active_at', '<=', Carbon::now()->startOfDay())
                                       ->where('expired_at', '>=', Carbon::now()->endOfDay())
                                       ->limit(3)->get();

        view()->share('advertisements', $advertisements);
    }

    /**
     * Landing page (index page)
     * @return \Illuminate\View\View
     */
    public function landingPage()
    {
        $cePageTitle = CE::where('type', CE::PAGE_TITLE)->where('name', CE::DIRECTORY)->first();
        $pageTitle = isset($cePageTitle) ? json_decode($cePageTitle->content)->page_title : 'Directory';
        $ceHeroBanner = CE::where('type', CE::HERO_BANNER)->where('name', CE::DIRECTORY)->first();
        $heroBanner = json_decode($ceHeroBanner->content)->hero_banner;

        $ceSearchBox = CE::where('type', CE::SEARCH_BOX)->where('name', CE::DIRECTORY)->first();
        $searchBox = isset($ceSearchBox) ? json_decode($ceSearchBox->content)->search_box : null;

        $pinDirectories = Directory::where('pin', 1)->limit(4)->get();
        $directories = Directory::all();

        return view(
            baseview(0).'-directory',
            compact(
                'pageTitle',
                'heroBanner',
                'searchBox',
                'pinDirectories',
                'directories'
            )
        );
    }

    /**
     * @return \Illuminate\View\View
     */
    public function directoryList()
    {
        $companyName = '';
        $sort = '';

        $cePageTitle = CE::where('type', CE::PAGE_TITLE)->where('name', CE::DIRECTORY)->first();
        $pageTitle = isset($cePageTitle) ? json_decode($cePageTitle->content)->page_title : 'Directory';
        $ceHeroBanner = CE::where('type', CE::HERO_BANNER)->where('name', CE::DIRECTORY)->first();
        $heroBanner = json_decode($ceHeroBanner->content)->hero_banner;

        $ceSearchBox = CE::where('type', CE::SEARCH_BOX)->where('name', CE::DIRECTORY)->first();
        $searchBox = isset($ceSearchBox) ? json_decode($ceSearchBox->content)->search_box : null;
        $directories = DB::table('b121nf0_directories')->select('*')->paginate(12);

        if (isset($_REQUEST['company_name'])) {
            $companyName = $_REQUEST['company_name'];
            $sort = $_REQUEST['sort'];

            $directories = DB::table('b121nf0_directories')
                ->where('title_en', 'like', '%'.$companyName.'%')
                ->select('*')
                ->orderBy('title_en', $sort)
                ->paginate(12);
        }

        return view(
            baseview(0).'-directory-list',
            compact(
                'sort',
                'companyName',
                'pageTitle',
                'heroBanner',
                'searchBox',
                'directories'
            )
        );
    }

    /**
     * @param $name
     *
     * @return \Illuminate\View\View
     */
    public function directoryItem($name)
    {
        $directory = Directory::where('name', $name)->first();
        $ceHeroBanner = CE::where('type', CE::HERO_BANNER)->where('name', CE::DIRECTORY)->first();
        $heroBanner = isset($ceHeroBanner) ? json_decode($ceHeroBanner->content)->hero_banner : null;

        $latitude = $directory->latitude;
        $longitude = $directory->longitude;
        if (isset($latitude) && isset($longitude)) {
            $zoom = 16;
            $mapTitle = $directory->title;
            $content = $mapTitle.' - '.lang($directory, 'address');
            Mapper::map($latitude, $longitude, ['marker' => false, 'zoom' => $zoom, 'scrollWheelZoom'=>false]);
            Mapper::informationWindow($latitude, $longitude, $content, ['open' => true, 'markers' => ['title' => $mapTitle]]);
        }

        return view(
            baseview(0).'-directory-detail',
            compact(
                'directory',
                'heroBanner'
            )
        );
    }

	/*
	 * List page
	 */
    public function listPage()
	{
		//if(Cookie::get('bizinfo') == NULL){ return redirect('/'); }

		$companyname = '';
		$sort = '';
		$title = get_title(4, 'category');
		$advertisement = DB::table('b121nf0_advertisement')->select('*')->where('section', '=', 1)->get();


		if(isset($_REQUEST['companyname'])):
			$companyname = $_REQUEST['companyname'];
			$sort = $_REQUEST['sort'];

			$directory = DB::table('b121nf0_directory')
				->join('b121nf0_media', 'b121nf0_directory.logo', '=', 'b121nf0_media.id')
				->where('url','<>','')
				->where('title','like','%'.$companyname.'%')
				->select('*')
				->orderBy('title', $sort)
				->paginate(12);
		else:
			$directory = DB::table('b121nf0_directory')
				->join('b121nf0_media', 'b121nf0_directory.logo', '=', 'b121nf0_media.id')
				->where('url','<>','')
				->select('*')
				->paginate(12);
		endif;

        $directory = Directory::orderBy('title_'.get_current_locale(), $sort)->get();

//        dd($directory);

		// Point to view layout
		return view(baseview(0).'-directory-list', compact('title','advertisement','directory','companyname','sort'));
	}

	/*
	 * Detail page
	 */
    public function detailPage($pageName)
	{
		//if(Cookie::get('bizinfo') == NULL){ return redirect('/'); }

		$title = get_title(4, 'category');
        $directory = Directory::where('name', $pageName)->first();
		$advertisement = DB::table('b121nf0_advertisement')->select('*')->where('section', '=', 1)->limit(2)->get();

//        dd($directory);

		$latitude  	= $directory->latitude;
		$longitude	= $directory->longitude;
		if(isset($latitude) && isset($longitude)){
			$zoom 		= 16;
			$maptitle	= $directory->title;
			$content	= $maptitle.' - '.lang($directory, 'address');
			Mapper::map($latitude, $longitude, ['marker' => false, 'zoom' => $zoom, 'scrollWheelZoom'=>false]);
			Mapper::informationWindow($latitude, $longitude, $content, ['open' => true, 'markers' => ['title' => $maptitle]]);
		}

//		dd($pageName);

		// Point to view layout
		return view(baseview(0).'-directory-detail', compact('title','directory','advertisement'));
	}

}
