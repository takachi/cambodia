<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cookie;
use DB;

class BusinessGuidesController extends Controller
{
	
    /* 
	 * Landing page (index page)
	 */
    public function landingPage()
	{
		//if(Cookie::get('bizinfo') == NULL){ return redirect('/'); }
		
		$title = get_title(2,'category');
		
		// Point to view layout
		return view(baseview(0).'-business-guides', compact('title')); 
	}
	
	/* 
	 * List page
	 */
    public function listPage($submenu)
	{
		//if(Cookie::get('bizinfo') == NULL){ return redirect('/'); }
		
		$title = get_title(2,'category');
		$category = DB::select("call get_category('$submenu', '".get_current_locale()."')");
		
		// Point to view layout
		return view(baseview(0).'-business-guides-list', compact('title','category','submenu'));
	}
	
	/* 
	 * Detail page
	 */
    public function detailPage($submenu, $pagename)
	{
		
	}
	
}