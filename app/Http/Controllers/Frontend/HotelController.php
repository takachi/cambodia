<?php

namespace App\Http\Controllers\Frontend;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class HotelController extends Controller
{
    public function index()
    {
        $hotels_pages = DB::table('shop')->where('shop_cat_id', 3)->paginate(5);
//        dd($hotels_pages);
        return view('frontend.desktop.hotel',compact('hotels_pages') );

    }


}
