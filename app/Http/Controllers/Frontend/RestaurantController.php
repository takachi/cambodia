<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class RestaurantController extends Controller
{
      public function index()
    {
        $retaurants = DB::table('shop')->where('shop_cat_id', 2)->paginate(5);
        return view('frontend.desktop.restaurant',compact('retaurants') );
    }
}
