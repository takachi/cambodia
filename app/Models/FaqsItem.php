<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FaqsItem extends Model
{
    protected $table = 'b121nf0_faqs_items';

    protected $fillable = [
        'license_mapping_id',
        'question_en',
        'question_kh',
        'answer_en',
        'answer_kh',
        'created_at',
        'updated_at',
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function licenseMapping()
    {
        return $this->belongsTo(LicenseMapping::class);
    }
}
