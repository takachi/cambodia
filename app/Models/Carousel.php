<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Service as Service;

class Carousel extends Model
{
    const PAGES = array(
        'service' => 'service'
    );

    protected $table = 'b121nf0_carousels';

    protected $fillable = [
        'service_id',
        'title_en',
        'title_kh',
        'description_en',
        'description_kh',
        'name',
        'image',
        'created_at',
        'updated_at',
    ];

    public static $pages = [
        'service' => 'service',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function service()
    {
        return $this->belongsTo(Service::class);
    }
}
