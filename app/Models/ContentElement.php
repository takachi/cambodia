<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentElement extends Model
{
    const REGULATION_CATEGORY = 'regulation_category';

    const HERO_BANNER = 'hero_banner';

    const SEARCH_BOX = 'search_box';

    const PAGE_TITLE = 'page_title';

    const REGULATION = 'regulation';

    const SME_POLICY = 'sme_policy';

    const RELATED_MINISTRY = 'related_ministry';

    const LICENSE_MAPPING = 'license_mapping';

    const SERVICE = 'service';

    const SLIDE_SHOW = 'slide_show';

    const DIRECTORY = 'directory';

    protected $table = 'b121nf0_content_elements';

    protected $fillable = [
        'name',
        'type',
        'author_id',
        'content',
        'created_at',
        'updated_at',
    ];
}



