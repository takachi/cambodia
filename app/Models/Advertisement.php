<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model
{
    const PAGES = array(
        'directory' => 'Directory',
        'licence-mapping' => 'SME License Mapping',
        'regulation' => 'Regulation',
        'service' => 'Service',
        'service-list' => 'Service List',
    );

    protected $table = 'b121nf0_advertisements';

    protected $fillable = [
        'title_en',
        'title_kh',
        'image',
        'link',
        'page',
        'active_at',
        'expired_at',
        'created_at',
        'updated_at',
    ];
}
