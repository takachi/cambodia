<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BicRegulationTable extends Model
{
    	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'b121nf0_category';
    protected $primarykey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','title_en','title_km','name','parent','langding_page','image','menu_order'];

}





