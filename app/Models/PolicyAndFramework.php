<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PolicyAndFramework extends Model
{
    protected $table = 'b121nf0_policy_and_frameworks';

    public $timestamps = false;

    protected $fillable = [
        'title_en',
        'title_kh',
        'description_en',
        'description_kh',
        'created_at',
        'updated_at',
        'status',
        'reference_en',
        'reference_kh',
        'author',
        'regulation_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function regulation()
    {
        return $this->belongsTo(Regulation::class);
    }
}

