<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Integer;

class Category extends Model
{
    protected $table = 'b121nf0_categories';

    protected $fillable = [
        'title_en',
        'title_kh',
        'status_en',
        'status_kh',
        'thumb',
        'banner',
        'ordering',
        'parent',
        'name',
        'created_at',
        'updated_at',
    ];

    /** todo */
    public static $cat = [
        'topHeaderLabel' => 'Top header label',
        'logo' => 'Logo',
        'languageLabel' => 'Language label',
        'bizInfo' => 'Biz info',
        'bizContact' => 'Biz contact',
        'chatBox' => 'Chat box'
    ];

    /**
     * @param string $categoryName
     *
     * @return int
     */
    public function getCategoryLevel($categoryName)
    {
        $level = 0;

        $category = self::where('name', $categoryName)->first();
        if (! $category) {
            return null;
        }
        if ($category->parent) {
            $firstParent = self::find($category->parent);
            $level = 1;

            if ($firstParent->parent) {
                $level = 2;
            }
        }

        return $level;
    }

    /**
     * @param int $level
     *
     * @return \Illuminate\Database\Eloquent\Collection|null|static[]
     */
    public function getCategoryByLevel($level = 0)
    {
        if ($level === 1) {
            return self::where('parent', 0)->get();
        }

        if ($level === 2) {
            $rootLevelIds = self::where('parent', 0)->chunk('id');
            return self::whereIn('parent', $rootLevelIds)->get();
        }
        return null;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function directories()
    {
        return $this->belongsToMany(Directory::class, 'b121nf0_category_directories', 'category_id', 'directory_id');
    }

    /**
     * Children Cat
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function child()
    {
        $children = self::where('parent', $this->getAttribute('id'))->get();
        return $children;
    }
}
