<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Directory extends Model
{
    protected $table = 'b121nf0_directories';

    protected $fillable = [
        'logo',
        'title_en',
        'title_kh',
        'name',
        'phone',
        'email',
        'website',
        'address_en',
        'address_kh',
        'about_en',
        'about_kh',
        'service_en',
        'service_kh',
        'latitude',
        'longitude',
        'created_at',
        'updated_at',
        'pin'
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function services()
    {
        return $this->belongsToMany(Service::class, 'b121nf0_service_directories', 'directory_id', 'service_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function partners()
    {
        return $this->hasMany(DirectoryPartner::class);
    }
}
