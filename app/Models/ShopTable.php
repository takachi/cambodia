<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class ShopTable extends Model
{
    protected $table = 'shop';
    protected $primarykey = 'id';

    public function ScopeTesting($query){
        return $query->orderBy('date_time', 'desc')->where('shop_cat_id', '=', 3)->get();
    }

    protected $fillable = ['id','shop_cat_id','shop_name','shop_desen','images','address','phone','facebook','google','map','date_time'];
    /**
     * Disable the table timestamps
     */
    public $timestamps = false;
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
