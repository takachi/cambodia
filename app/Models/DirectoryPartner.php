<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Database\Eloquent\Relations as Relations;

class DirectoryPartner extends Model
{
    protected $table = 'b121nf0_directory_partners';

    protected $fillable = [
        'directory_id',
        'logo',
        'created_at',
        'updated_at',
    ];

    /**
     * @return Relations\BelongsTo
     */
    public function directory()
    {
        return $this->belongsTo(Directory::class);
    }
}
