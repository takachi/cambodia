<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryTable extends Model
{
   protected $table = 'b121nf0_category';
   
     protected $fillable = [
        'title_en',
        'title_km',
        'name',
        'created_at',
        'updated_at',
    ];
}

