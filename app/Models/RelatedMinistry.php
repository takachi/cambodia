<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RelatedMinistry extends Model
{
    protected $table = 'b121nf0_related_ministries';

    protected $fillable = [
        'logo',
        'title_en',
        'title_km',
        'website',
        'create_at',
        'updated_at',
        'regulation_id',
        'uri',
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function regulation()
    {
        return $this->belongsTo(Regulation::class);
    }
}
