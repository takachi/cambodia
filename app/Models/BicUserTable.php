<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class BicUserTable extends Authenticatable
{
    	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';
    protected $primarykey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable = ['id','name','username','password','email','role','status','created_by','updated_by',];
    protected $fillable = ['id','name','company_name','address','email','password','phone_number','role','date_add_data'];
    /**
     * Disable the table timestamps
     */
    public $timestamps = false;
    /**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
	    'password', 'remember_token',
	];


}




