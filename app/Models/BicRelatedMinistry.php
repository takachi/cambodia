<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BicRelatedMinistry extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'b121nf0_regulations_related_ministry';
    protected $primarykey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'logo', 'title_en', 'title_km', 'url', 'modify_date', 'langding_page', 'date'];
		    	
}





