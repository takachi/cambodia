<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = 'b121nf0_services';

    protected $fillable = [
        'title_en',
        'title_kh',
        'thumb',
        'banner',
        'ordering',
        'parent',
        'name',
        'created_at',
        'updated_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function directories()
    {
        return $this->belongsToMany(Directory::class, 'b121nf0_service_directories', 'service_id', 'directory_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function carousels()
    {
        return $this->hasMany(Carousel::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function advertisements()
    {
        return Advertisement::where('page',  $this->name)->get();
    }
}
