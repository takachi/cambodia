<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Regulation extends Model
{
    const TYPES = array(
        'cambodia-industry-development-policy' => 'Policy and Framework',
        'sme-license-mapping' => 'License Mapping',
        'related-ministry' => 'Related Ministry',
    );

    protected $table = 'b121nf0_regulations';

    protected $fillable = [
        'title_en',
        'title_kh',
        'thumb',
        'ordering',
        'uri',
        'created_at',
        'updated_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function policyAndFrameworks()
    {
        return $this->hasMany(PolicyAndFramework::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function relatedMinistries()
    {
        return $this->hasMany(RelatedMinistry::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function licenseMappings()
    {
        return $this->hasMany(LicenseMapping::class);
    }
}
