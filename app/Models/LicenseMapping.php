<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LicenseMapping extends Model
{
    protected $table = 'b121nf0_license_mappings';

    protected $fillable = [
        'regulation_id',
        'title_en',
        'title_kh',
        'thumbnail',
        'banner',
        'diagram_en',
        'diagram_kh',
        'initiative_thumb_en',
        'initiative_thumb_kh',
        'document_en',
        'document_kh',
        'ordering',
        'license_inspection',
        'created_at',
        'updated_at',
        'name',
        'faqs_title_en',
        'faqs_title_kh',
        'faqs_description_en',
        'faqs_description_kh',
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function regulation()
    {
        return $this->belongsTo(Regulation::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function faqsItems()
    {
        return $this->hasMany(FaqsItem::class, 'license_mapping_id');
    }
}
