<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'b121nf0_settings';

    protected $fillable = ['name', 'content', 'created_at', 'updated_at'];

    public static $settings = [
        'bizContact' => 'biz-contact',
        'bizInfo' => 'biz-info',
        'chatBox' => 'chat-box',
        'languageLabel' => 'language-label',
        'logo' => 'logo',
        'topHeaderLabel' => 'top-header-label',
        'socialMedia' => 'social-media',
    ];
}
