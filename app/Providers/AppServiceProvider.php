<?php

namespace App\Providers;


use App\Models\Service;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use DB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

      Schema::defaultStringLength(191);
		view()->composer('frontend.desktop.desktop', function($view) {

			$category 			= DB::table('b121nf0_category')->select('*')->where('parent', '=', '0')->where('landing_page', '<>', '-1')->orderBy('menu_order','ASC')->get();
			$sub_category 		= DB::table('b121nf0_category')->select('*')->where('parent', '<>', '0')->where('landing_page', '<>', '-1')->orderBy('menu_order','ASC')->get();
			$social				= DB::table('b121nf0_socialnetwork')->select('*')->get();
			$gallery			= DB::table('b121nf0_gallery')->select('*')->get();



			// Compact data with view
			$view->with(compact('category','sub_category','social','gallery'));
		});




		// view()->composer('frontend.desktop.desktop', function($view) {

		// 	$category 			= DB::table('b121nf0_category')->select('*')->where('parent', '=', '0')->where('landing_page', '<>', '-1')->orderBy('menu_order','ASC')->get();
		// 	$sub_category 		= DB::table('b121nf0_category')->select('*')->where('parent', '<>', '0')->where('landing_page', '<>', '-1')->orderBy('menu_order','ASC')->get();
		// 	$social				= DB::table('b121nf0_socialnetwork')->select('*')->get();
		// 	$gallery			= DB::table('b121nf0_gallery')->select('*')->get();



		// 	// Compact data with view
		// 	$view->with(compact('category','sub_category','social','gallery'));
		// });


    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

