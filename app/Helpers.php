<?php

//use Request;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Jenssegers\Agent\Agent;
use App\Http\Requests;
//use DB;

/**
 * Description: Get baseview
 * Version: 1.0.0
 * Last update: 2017/06/08 (YYYY/MM/DD)
 * Author: Sovandara <sovandara@bizsolution.biz>
 */
function baseview($detect=0)
{

	/*
	 *	Parameter
	 * ---------------------------------------------------
	 *	0 or empty : return full path ()
	 *	1 :	return specific platform (desktop, mobile)
	 */

	// User agent
	$agent = new Agent();
	if( $agent->isMobile() && !$agent->isTablet() ){ $baseview = 'desktop';  }
	$baseview = isset($baseview) ? $baseview : 'desktop';

	// Check
	if($detect == 0){ $baseview = 'frontend.'.$baseview.'.'.$baseview; }

	// Return
	return $baseview;
}


/**
 * Description: Get Current Locale
 * Version: 1.0.0
 * Last update: 2017/06/08 (YYYY/MM/DD)
 * Author: Sovandara <sovandara@bizsolution.biz>
 */
function get_current_locale()
{
	$lang  = LaravelLocalization::getCurrentLocale();
	return $lang;
}


/**
 * Description: Get Page Title
 * Version: 1.0.0
 * Last update: 2017/06/08 (YYYY/MM/DD)
 * Author: Sovandara <sovandara@bizsolution.biz>
 */
function get_title($param, $table='page', $paramname = 'id')
{
	$data = DB::table('b121nf0_'.$table)->select('title_'.get_current_locale().' as title')->where($paramname, '=', $param)->get();
	return $data[0]->title;
}

/**
 * Description: Get Basename
 * Version: 1.0.0
 * Last update: 2017/06/08 (YYYY/MM/DD)
 * Author: Sovandara <sovandara@bizsolution.biz>
 */
function get_basename()
{
	$path_parts = pathinfo($_SERVER['REQUEST_URI']);
	$path_parts = $path_parts['basename'];
	$basename	= $path_parts == get_current_locale() ? 'home' : $path_parts == '' ? 'home' : $path_parts;

	return $basename;
}

/**
 * Description: Get Option
 * Version: 1.0.0
 * Last update: 2017/06/08 (YYYY/MM/DD)
 * Author: Sovandara <sovandara@bizsolution.biz>
 */
function get_option($param, $lang='')
{
	$lang = $lang == '' ? $lang : '_'.get_current_locale();
	$data = DB::table('b121nf0_options')->select('option_value')->where('option_name', '=', $param.$lang)->get();
	return $data[0]->option_value;
}


/**
 * Description: Get Image URL
 * Version: 1.0.0
 * Last update: 2017/06/08 (YYYY/MM/DD)
 * Author: Sovandara <sovandara@bizsolution.biz>
 */
function get_media_url($param, $size='')
{
	$size 		= $size == '' ? $size : '-'.$size; // Get size
	$data 		= DB::table('b121nf0_media')->select('url')->where('id', '=', $param)->get(); // Get data from DB
	$path_parts = pathinfo(asset('uploads/'.$data[0]->url)); // Get pathinfo

	if(isset($path_parts['extension'])){
		$image 		= $path_parts['dirname'].'/'.$path_parts['filename'].$size.'.'.$path_parts['extension']; // Restring image url
	}else{
		$image = 'http://via.placeholder.com/150x50';
	}
	return $image;
}

/**
 * Description: Get Contact Us
 * Version: 1.0.0
 * Last update: 2017/06/08 (YYYY/MM/DD)
 * Author: Sovandara <sovandara@bizsolution.biz>
 */
function get_contact_info($param, $lang='', $id=1)
{
	$lang 	= $lang == '' ? $lang : '_'.get_current_locale();
	$param	= $param.$lang;
	$data 	= DB::table('b121nf0_contact_us')->select('*')->where('id', '=', $id)->get();
	return $data[0]->$param;
}


function get_category_name($id){
	$data 	= DB::table('b121nf0_category')->select('name')->where('id', '=', $id)->get();
	return $data[0]->name;
}


/**
 * Description: Hamburger Menu
 * Version: 1.0.0
 * Last update: 2017/06/08 (YYYY/MM/DD)
 * Author: Sovandara <sovandara@bizsolution.biz>
 */
function menu($category,$sub_category){

	// Declare variable
	$menu = '';
	$title = 'title_'.get_current_locale();

	foreach($category as $key=>$value):

		// Reset
		$sublist = '';
		$sublistresult = '';
		$landing = '';
		$i = 1;

		foreach($sub_category as $key=>$subvalue):
			if($value->id == $subvalue->parent):

				// Check if no landing page and redirect to first child
				if($value->landing_page == 0){
					if($i==1){ $landing = '/'.$subvalue->name; $i++; }
				}

				$sublist .= '<li><a href="'.asset($value->name).'/'.$subvalue->name.'">'.$subvalue->$title.'</a></li>';

			endif;
		endforeach;

		// Submenu
		if($sublist):
			$sublistresult = '
				<span class="arrow"></span>
				<div class="submenu">
					<ul>
						<li class="name back"><a href="#"><span></span>'.__('Menu').'</a></li>
						<li class="name"><a href="#">'.$value->$title.'</a></li>
						'.$sublist.'
					</ul>
				</div>
			';
		endif;

		$menu .= '<li><a href="'.asset($value->name).$landing.'">'.$value->$title.'</a>'.$sublistresult.'</li>';

	endforeach;

	echo  '<ul id="sidemenu">'.$menu.'<ul>';

}

/**
 * @param $date
 *
 * @return string
 */
function dateFormat($date)
{
    return  Carbon::parse($date)->format('d-m-Y');
}

/**
 * @param object $entity
 * @param string $attribute
 *
 * @return null
 */
function lang($entity = null, $attribute = null)
{
    if (!is_object($entity) || !$attribute) {
        return null;
    }

    $currentLocale = '_en';

    /** todo: should refactor this condition after make db col _kh and _km to consistent  */
    if (get_current_locale() != 'en') {
        $currentLocale = '_kh';
    }

    $attribute .= $currentLocale;
    if (isset($entity->$attribute)) {
        return $entity->$attribute;
    }

    return null;
}
?>
